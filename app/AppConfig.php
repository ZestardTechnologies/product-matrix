<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
    protected $table = 'appconfig';
	  protected $primaryKey = 'id';
    protected $fillable = ['id','store_id','app_status','show_stock','show_price','left_header_color','top_header_color','border_color','quantity_color',
        'price_color','left_text_color','top_text_color' ,'price_label' ,'price_font_size' , 'quantity_label' , 'quantity_font_size' , 
        'additional_css' , 'allow_out_of_stock' ,
        'quantity_position','price_position','show_total','row_total_label','row_font_size','column_total_label','column_font_size','min_qty_error_msg',
        'price_relative_to_quantity'];
    
    public $timestamps = false; 
}
