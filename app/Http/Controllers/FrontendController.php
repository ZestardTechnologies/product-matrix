<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;
use App\ProductSettings;
use App\AppConfig;

class FrontendController extends Controller {

    public function frontend(request $request) {
        $dataId = $request->news_id;
        $shop = $request->domain_name;

        $id = DB::table('usersettings')->where('store_name', $shop)->value('id');
        //echo '<pre>'; print_r($id); die;      
        $newsFormData = AddNewsForm::select()->where(['encrypt_id' => $dataId, 'store_id' => $id, 'status' => 1])->first();
        //echo '<pre>'; print_r($newsFormData); die;      
        if ($newsFormData == "") {
            return ("");
        } else {
            $unserializeNewsDetails = unserialize(base64_decode($newsFormData->news_details));
            //echo '<pre>'; print_r($unserializeNewsDetails); die;
            return view('frontend', ['newsData' => $newsFormData, 'newsDetails' => $unserializeNewsDetails]);
        }
    }

    public function get_product_settings(Request $request) {
        $shop_name = $request->input('shop_name');
        $count = $request->input('count');
        $store = DB::table('usersettings')->where('store_name', $shop_name)->first();
        $product_settings = ProductSettings::where(['store_id' => $store->store_encrypt])->get()->toArray();
        $response = array();
        foreach ($product_settings as $product) {
            if ($product['min_qty_required'] == 1) {
                $response[$product['product_id']] = $product['min_qty_value'];
            }
        }
        echo json_encode($response);
    }

    public function get_settings(Request $request) {
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $store_id = $select_store->id;
        $encrypted_store_id = $select_store->store_encrypt;
        $store_settings = AppConfig::where('store_id', '=', $store_id)->first()->toArray();
        echo json_encode($store_settings);
    }

    public function check_app(Request $request) {
        $shop = $request->input('domain_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $store_id = $select_store->id;
        $settings = DB::table('appconfig')->where('store_id', $store_id)->first();
        
        $response = array('app_status' => $settings->app_status,'config_confirmation' => $select_store->config_confirmation);
        
        echo json_encode($response);
        /*  if (count($settings) > 0) {
          if ($settings->app_status == 1) {
          echo 1;
          } else {
          echo 0;
          }
          } else {
          echo 1;
          } */
    }

    /*
      displays product matrix on front page
     */
    public function frontend_view(Request $request) {
		/* Check App Function */
		
		$shop = $request->input('domain_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $store_id = $select_store->id;
        $settings = DB::table('appconfig')->where('store_id', $store_id)->first();
        
        $response = array('app_status' => $settings->app_status,'config_confirmation' => $select_store->config_confirmation);
        
        $config_settings = json_encode($response);
		
		/* Check App Function */
		
        $product_id = $request->product_id;
        $domain_name = $request->domain_name;
        /* $shop = session('shop'); */

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $domain_name)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        
        
        $url = 'https://' . $shop . '/admin/products/' . $product_id . '.json';

        $store_id = $select_store[0]->id;

        $product_info = $sh->call([
            'URL' => $url,
            'METHOD' => 'GET'
                ], false);

        /* Load Configuration form product settings table */
        $product_settings = ProductSettings::where('product_id', '=', $product_id)->first();

        /* Load Configuration from general settings - appconfig table */
        $general_settings = AppConfig::where('store_id', '=', $select_store[0]->id)->first();

        if (count($general_settings) > 0) {
            $general_settings_array = AppConfig::where('store_id', '=', $select_store[0]->id)->first()->toArray();
            $general_settings_json = json_encode($general_settings_array);
        }

        // options and variants handling
        $options = $product_info->product->options;

        // Quantity error message
        $min_qty_error_msg = "Please enter minimum quantity";

        if (count($general_settings) > 0) {
            $app_status = $general_settings->app_status;

            //quantity
            $show_stock = $general_settings->show_stock;
            $quantity_label = $general_settings->quantity_label;
            $allow_out_of_stock = $general_settings->allow_out_of_stock;

            //price
            $show_price = $general_settings->show_price;
            $price_label = $general_settings->price_label;

            //total
            $show_total = $general_settings->show_total;
            $row_total_label = $general_settings->row_total_label;
            $column_total_label = $general_settings->column_total_label;

            $border_color = $general_settings->border_color;
            $border_status = $general_settings->border_status;

            $quantity_position = $general_settings->quantity_position;
            $price_position = $general_settings->price_position;
            $price_color = $general_settings->price_color;
            $quantity_color = $general_settings->quantity_color;

            $price_font_size = $general_settings->price_font_size;
            $quantity_font_size = $general_settings->quantity_font_size;

            // Quantity error message
            $min_qty_error_msg = $general_settings->min_qty_error_msg;

            if (trim($min_qty_error_msg) == "") {
                $min_qty_error_msg = "Please enter minimum quantity";
            }
        } else {
            $app_status = 1;
            // quantity
            $show_stock = null;
            $quantity_label = null;
            $allow_out_of_stock = null;

            //price
            $show_price = null;
            $price_label = null;

            //total
            $show_total = null;
            $row_total_label = null;
            $column_total_label = null;
        }
        /*
          if ($options[0]->name == "Title") {
          $app_status = 0;
          } */


        if ($app_status != 1) {
            // app is disabled
            return "app_disabled";
        }

        $settings_array = array();

        $settings_array['show_stock'] = $show_stock;
        $settings_array['quantity_label'] = $quantity_label;
        $settings_array['allow_out_of_stock'] = $allow_out_of_stock;

        $settings_array['show_price'] = $show_price;
        $settings_array['price_label'] = $price_label;

        $settings_array['show_total'] = $show_total;
        $settings_array['row_total_label'] = $row_total_label;
        $settings_array['column_total_label'] = $column_total_label;

        $settings_array['border_color'] = $border_color;
        $settings_array['border_status'] = $border_status;

        $settings_array['price_position'] = $price_position;
        $settings_array['quantity_position'] = $quantity_position;

        $settings_array['price_color'] = $price_color;
        $settings_array['quantity_color'] = $quantity_color;

        $settings_array['price_font_size'] = $price_font_size;
        $settings_array['quantity_font_size'] = $quantity_font_size;

        $options_array = array();

        $options_array['row_option'] = array();

        $options_array['row_option']['id'] = "";
        $options_array['row_option']['product_id'] = "";
        $options_array['row_option']['name'] = "";
        $options_array['row_option']['position'] = "";
        $options_array['row_option']['values'] = array();

        $options_array['column_option'] = array();

        $options_array['column_option']['id'] = "";
        $options_array['column_option']['product_id'] = "";
        $options_array['column_option']['name'] = "";
        $options_array['column_option']['position'] = "";
        $options_array['column_option']['values'] = array();

        $options_array['dropdown_option'] = array();

        $options_array['dropdown_option']['id'] = "";
        $options_array['dropdown_option']['product_id'] = "";
        $options_array['dropdown_option']['name'] = "";
        $options_array['dropdown_option']['position'] = "";
        $options_array['dropdown_option']['values'] = array();
        
        if (count($options) > 0) {
            foreach ($options as $option) {
                if (count($product_settings) > 0) {
                    if ($product_settings->row_option != null) {
                        if ($option->id == $product_settings->row_option) {
                            $options_array['row_option'] = (array) $option;
                        } else if ($option->id == $product_settings->column_option) {
                            $options_array['column_option'] = (array) $option;
                        } else {
                            $options_array['dropdown_option'] = (array) $option;
                        }
                    } else {
                        if ($option->position == 1) {
                            $options_array['row_option'] = (array) $option;
                        } else if ($option->position == 2) {
                            $options_array['column_option'] = (array) $option;
                        } else {
                            $options_array['dropdown_option'] = (array) $option;
                        }
                    }
                } else {
                    if ($option->position == 1) {
                        $options_array['row_option'] = (array) $option;
                    } else if ($option->position == 2) {
                        $options_array['column_option'] = (array) $option;
                    } else {
                        $options_array['dropdown_option'] = (array) $option;
                    }
                }
            }
        }
        
        $variant_url = 'https://' . $shop . '/admin/products/' . $product_id . '/variants.json?limit=250';

        $variants = $sh->call([
            'URL'       => $variant_url,
            'METHOD'    => 'GET',                                
        ], false);        
        $variants_array_final = array();

        $variants_array = (array) $variants->variants;
        
        if (count($options_array['dropdown_option']['values']) > 0) {
            $dimension = '3';
            foreach ($options_array['dropdown_option']['values'] as $dropdown_option) {
                foreach ($options_array['column_option']['values'] as $column_option) {
                    // The first foreach loops through the row options
                    foreach ($options_array['row_option']['values'] as $row_option) {
                        // The second foreach loops through the column options
                        $variants_array_final[$dropdown_option][$column_option][$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                        // echo $this->getCellValue($row_option , $column_option , $variants);
                        // $variants = $this->getCellValue($row_option , $column_option , $variants);  
                    }
                }
            }
        } else if (count($options_array['column_option']['values']) > 0 && count($options_array['row_option']['values']) > 0) {
            $dimension = '2';
            //dd(count($options_array['column_option']['values']));
            //dd(count($options_array['row_option']['values']));
            foreach ($options_array['column_option']['values'] as $column_option) {
                // The first foreach loops through the row options
                foreach ($options_array['row_option']['values'] as $row_option) {                    
                    $dropdown_option = "";
                    // The second foreach loops through the column options
                    $variants_array_final[$column_option][$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                    // echo $this->getCellValue($row_option , $column_option , $variants);
                    // $variants = $this->getCellValue($row_option , $column_option , $variants);  
                }
            }
        } else if (count($options_array['row_option']['values']) > 0) {
            $dimension = '1';
            // The first foreach loops through the row options
            foreach ($options_array['row_option']['values'] as $row_option) {
                $dropdown_option = "";
                $column_option = "";
                // The second foreach loops through the column options
                $variants_array_final[$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                // echo $this->getCellValue($row_option , $column_option , $variants);
                // $variants = $this->getCellValue($row_option , $column_option , $variants);  
            }
        } else {
            // no options are there for these product
            $variants_array_final = array("no_options" => "no_options");
        }

        $total_width = (100 / (count($options_array['row_option']['values']) + 2)) + 5;
        $total_width_options = 100 / (count($options_array['row_option']['values']));

        $variants_array_json = json_encode($variants_array_final);

        $min_qty = null;
        $disable_app_for_product = null;

        if (count($product_settings) > 0) {
            if ($product_settings->min_qty_required == '1') {
                $min_qty = $product_settings->min_qty_value;
            }
            if ($product_settings->disable_app_for_product == '1') {
                $disable_app_for_product = $product_settings->disable_app_for_product;
            }
        }
        
         /* 
		 $shop_api_url = 'https://' . $shop . '/admin/shop.json';
        
         $shop_api_response = $sh->call([
            'URL' => $shop_api_url,
            'METHOD' => 'GET'
                ], false);
         
         $money_format = $shop_api_response->shop->money_format;         
		*/		
        /* return view('products.productmatrix_frontview', compact('variants_array_final', 'options_array', 'product_id', 'domain_name', 'variants_array_json', 'total_width', 'dimension', 'min_qty', 'min_qty_error_msg', 'general_settings_json', 'column_total_label', 'row_total_label', 'variants_array_final', 'disable_app_for_product','money_format')); */
        $money_format = $settings->money_format;
       
        if($shop == "action-plastics-sales.myshopify.com"){
            /* $money_format = "Rs. {{amount}}"; */
		    $view = (string) View('products.action_plastics_frontview', compact('variants_array_final', 'options_array', 'product_id', 'domain_name', 'variants_array_json', 'total_width', 'dimension', 'min_qty', 'min_qty_error_msg', 'general_settings_json', 'column_total_label', 'row_total_label', 'variants_array_final', 'disable_app_for_product','money_format','total_width_options'))->render();
        }
        else{
            /* $money_format = "Rs. {{amount}}"; */
		    $view = (string) View('products.productmatrix_frontview', compact('variants_array_final', 'options_array', 'product_id', 'domain_name', 'variants_array_json', 'total_width', 'dimension', 'min_qty', 'min_qty_error_msg', 'general_settings_json', 'column_total_label', 'row_total_label', 'variants_array_final', 'disable_app_for_product','money_format','total_width_options'))->render();
        }
        		
		$response = array();
		$response['config_settings'] = $config_settings;		
		$response['view'] = $view;		
		return json_encode($response);
	}

    /*
     * Creates associative array from variants array
     */

    public function getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings) {
        $price = '';
        $variant_id = '';
        $inventory_quantity = '';
        $quantity_position = '';
        $quantity_label = '';
        $show_stock_qty = '';

        foreach ($variants_array as $variant) {
            if (($row_option == $variant->option1 || $row_option == $variant->option2 || $row_option == $variant->option3) && ($column_option == $variant->option1 || $column_option == $variant->option2 || $column_option == $variant->option3) && ($dropdown_option == $variant->option1 || $dropdown_option == $variant->option2 || $dropdown_option == $variant->option3)) {
                $price = $variant->price;
                $variant_id = $variant->id;

                if ($variant->inventory_management != null) {
                    if (count($general_settings) > 0) {
                        if ($general_settings->show_stock == 1) {
                            $inventory_quantity = $variant->inventory_quantity;
                            
                            if($inventory_quantity < 0 )
                            {
                                // if quantity is set in minus, than show  0
                                $inventory_quantity = 0;
                            }
                            
                            if ($general_settings->allow_out_of_stock != 1) {
                                if ($inventory_quantity == 0) {
                                    return "out of stock";
                                    continue;
                                }
                            }
                            
                            

                            $quantity_position = $general_settings->quantity_position;
                            $quantity_label = $general_settings->quantity_label;
                        }
                    }
                } 
                
                return array('price' => $price, 'variant_id' => $variant_id, 'inventory_quantity' => $inventory_quantity, 'quantity_position' => $quantity_position, 'quantity_label' => $quantity_label);
            } else {
                continue;
            }
        }
    }

}
