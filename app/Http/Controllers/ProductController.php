<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use App\ProductSettings;
use App\AppConfig;
use App\ShopModel;

class ProductController extends Controller {

    public function __construct() {   
    }

    /*
     * Shows the list of all products in backend
     */

    public function index() {
//        $shop = session('shop');
//        $app_settings = DB::table('appsettings')->where('id', 1)->first();
//        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
//        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
//
//        $url = 'https://' . $shop . '/admin/products.json';
//
//        $products = $sh->call([
//            'URL' => $url,
//            'METHOD' => 'GET'
//                ], false);

        return view('products.index');
    }

    /*
     * datatables ajax call data loading method
     */

    public function get_products(Request $request) {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $count = $sh->call(['URL' => '/admin/products/count.json', 'METHOD' => 'GET']);
        $products_count = (array) $count;
        $product_count = $products_count['count'];
        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];
        $current_page = ceil($start / $limit) + 1;
        $search = $request['search']['value'];
        $val = $start + 1;
        $total_products = array('draw' => $draw, 'recordsTotal' => $product_count, 'recordsFiltered' => $product_count);

        if ($search) {
            $default_image = url('/image/default.png');
            $pages = ceil($product_count / 250);
            $limit = 250;
            for ($i = 0; $i < $pages; $i++) {
                $current_page = $i + 1;
                $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                foreach ($product_list->products as $product) {

                    if (stristr($product->title, $search)) {
                        if (isset($product->images[0])) {
                            $image = $product->images[0]->src;
                        } else {
                            $image = $default_image;
                        }

                        $link = "<a href = 'https://" . $shop . "/admin/products/" . $product->id . "' target = '_blank'>" . $product->title . "</a>";
                        $edit_link = "<a href =" . url('edit/' . $product->id) . "><button class='buttonstyle'><span class='glyphicon glyphicon-edit'></span></button></a>";
                        $total_products['data'][] = array($image, $link, $product->product_type, $product->vendor, $edit_link);
                    }
                    $val++;
                }
            }
        } else {
            $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);

            $default_image = url('/image/default.png');

            foreach ($product_list->products as $product) {
                if (isset($product->images[0])) {
                    $image = $product->images[0]->src;
                } else {
                    $image = $default_image;
                }

                $link = "<a href = 'https://" . $shop . "/admin/products/" . $product->id . "' target = '_blank'>" . $product->title . "</a>";
                $edit_link = "<a href =" . url('edit/' . $product->id) . "><button class='buttonstyle'><span class='glyphicon glyphicon-edit'></span></button></a>";
                $total_products['data'][] = array($image, $link, $product->product_type, $product->vendor, $edit_link);
                $val++;
            }
        }

        return json_encode($total_products);
    }

    /*
     * Edit a product from list shown in backend
     */
    public function edit($id) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $url = 'https://' . $shop . '/admin/products/' . $id . '.json';

        $store_id = $select_store[0]->store_encrypt;

        $product_info = $sh->call([
            'URL' => $url,
            'METHOD' => 'GET'
                ], false);

        $options = $product_info->product->options;

        $options_array = array();

        foreach ($options as $option) {
            $options_array[$option->id]['name'] = $option->name;
        }

        $options_array_count = count($options_array);
        if (count($options_array) < 2) {
            $variants_message = "Row option and column option will be shown only when there are at least two options.currently there is/are " . $options_array_count . " option/s.";
        } else {
            $variants_message = "";
        }

        $product_settings = ProductSettings::where('product_id', '=', $id)->where('store_id', '=', $store_id)->first();

        $options_array_count = count($options_array);

        return view('products.edit', compact('id', 'product_info', 'options_array', 'shop', 'store_id', 'product_settings', 'options_array_count', 'variants_message'));
    }

    /*
     * Update a product/ edit page posts here
     */

    public function update(Request $request) {
        if ($request->update_product) {
            $id = $request->update_product;
            $product_settings = ProductSettings::where('id', '=', $id)->first();
        } else {
            $product_settings = new ProductSettings;
        }

//        if (!isset($request->row_option) && !isset($request->column_option)) {
//            return redirect()->back()->withInput($request->input())->with(array('notification' => array('alert-type' => 'error',
//                            'message' => 'Please add both column option and row option.')));
//        }
//
//        if ($request->row_option == $request->column_option) {
//            return redirect()->back()->withInput($request->input())->with(array('notification' => array('alert-type' => 'error',
//                            'message' => 'Row option and column option cannot be same.')));
//        }		
        if ($request->min_qty_required) {
            $product_settings->row_option = $request->row_option;
            $product_settings->column_option = $request->column_option;
            $product_settings->min_qty_required = '1';
            $product_settings->min_qty_value = $request->min_qty_value;
            $product_settings->min_qty_error_msg = $request->min_qty_error_msg;
            $product_settings->product_id = $request->product_id;
            $product_settings->store_id = $request->store_id;

            if ($request->disable_app_for_product) {
                $product_settings->disable_app_for_product = '1';
            } else {
                $product_settings->disable_app_for_product = '0';
            }

            $product_settings->save();
        } else {
            $product_settings->row_option = $request->row_option;
            $product_settings->column_option = $request->column_option;
            $product_settings->min_qty_required = '0';
            $product_settings->min_qty_value = "";
            $product_settings->min_qty_error_msg = "";
            $product_settings->product_id = $request->product_id;
            $product_settings->store_id = $request->store_id;
            if ($request->disable_app_for_product) {
                $product_settings->disable_app_for_product = '1';
            } else {
                $product_settings->disable_app_for_product = '0';
            }

            $product_settings->save();
        }

        return redirect()->back()->with(array('notification' => array('alert-type' => 'success',
                        'message' => 'Product updated.')));
    }

    /*
     * Shows dashboard/general settings
    */

    public function dashboard(Request $request) {
        $shop = session('shop');
		if(empty($shop))
		{
			$shop = $_GET['shop'];
			//$shop = $request->input('shop');
			session(['shop' => $shop]);			
		}		
        $select_store = ShopModel::where('store_name' , $shop)->first();
        $store_id = $select_store->id;
        $new_install = $select_store->new_install;
		$install_date = $select_store->payment_created_at;
		$date_time = explode("T", $install_date);		
		$date = strtotime($date_time[0]);
		$date = strtotime("+7 day", $date);
		$modal_date  = date('Y-m-d', $date);
		$todays_date = date('Y-m-d');					
		/* if(strtotime($todays_date) > strtotime($modal_date))
		{
			$new_install = 'N';
			$select_store->new_install = 'N';
			$select_store->save();
		}	 */	
        $encrypted_store_id = $select_store->store_encrypt;
        $store_record = AppConfig::where('store_id', '=', $store_id)->first();

        return view('dashboard_new', compact('store_record', 'encrypted_store_id','new_install'));
    }

    /*
     * Shows dashboard/general settings
    */

    public function help() 
	{
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $store_id = $select_store->id;
        $encrypted_store_id = $select_store->store_encrypt;

        return view('help', compact('encrypted_store_id'));
    }

    /*
     * dashboard/general settings page posts here
     */

    public function updateGeneralSettings(Request $request) {
        $shop = $request->session()->get('shop');
        $shopid = DB::table('usersettings')->where('store_name', $shop)->value('id');
        $appconfig = new AppConfig;
        $app_setting = AppConfig::where('store_id', $shopid)->first();
		
		$shop_find = DB::table('usersettings')->where('store_name', $shop)->first();
		$app_settings = DB::table('appsettings')->where('id', 1)->first();		
		$shop_api_url = 'https://' . $shop . '/admin/shop.json';		
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);					
        $shop_api_response = $sh->call([
            'URL' => $shop_api_url,
            'METHOD' => 'GET'
                ], false);
         
		$money_format = $shop_api_response->shop->money_format;         		
        if(count($app_setting) > 0) {
            if ($request['app_status']) 
			{
                $app_setting->app_status = $request['app_status'];
            } 
			else 
			{
                $app_setting->app_status = 0;
            }
            $app_setting->show_stock = $request['show_stock'];
            $app_setting->show_price = $request['show_price'];
            $app_setting->left_header_color = $request['left-header-color'];
            $app_setting->top_header_color = $request['top-header-color'];
            $app_setting->left_text_color = $request['left-text-color'];
            $app_setting->top_text_color = $request['top-text-color'];
            $app_setting->border_color = $request['border-color'];
            if ($request['border_status']) {
                $app_setting->border_status = $request['border_status'];
            } else {
                $app_setting->border_status = 0;
            }
            if ($request['cell_border_status']) {
                $app_setting->cell_border_status = $request['cell_border_status'];
            } else {
                $app_setting->cell_border_status = 0;
            }
            if ($request['show_default_price']) {
                $app_setting->show_default_price = $request['show_default_price'];
            } else {
                $app_setting->show_default_price = 0;
            }
            if ($request['quantity-color']) {
                $app_setting->quantity_color = $request['quantity-color'];
            }
            if ($request['price-color']) {
                $app_setting->price_color = $request['price-color'];
            }
            if ($request['cell_color']) {
                $app_setting->cell_color = $request['cell_color'];
            }
            $app_setting->allow_out_of_stock = $request['allow_out_of_stock'];
            $app_setting->store_id = $shopid;


            $app_setting->price_label = $request['price_label'];
            $app_setting->quantity_label = $request['quantity_label'];
            $app_setting->additional_css = $request['additional_css'];
            if ($request['price_font_size']) {
                $app_setting->price_font_size = $request['price_font_size'];
            }
            if ($request['quantity_message_color']) {
                $app_setting->quantity_message_color = $request['quantity_message_color'];
            }
            if ($request['min_quantity_font_size']) {
                $app_setting->min_quantity_font_size = $request['min_quantity_font_size'];
            }
            $app_setting->quantity_label = $request['quantity_label'];
            if ($request['quantity_font_size']) {
                $app_setting->quantity_font_size = $request['quantity_font_size'];
            }

            // columns added on 26-03-2018
            $app_setting->quantity_position = $request['quantity_position'];
            $app_setting->price_position = $request['price_position'];
            $app_setting->show_total = $request['show_total'];
            $app_setting->row_total_label = $request['row_total_label'];
            $app_setting->row_font_size = $request['row_font_size'];
            $app_setting->column_total_label = $request['column_total_label'];
            $app_setting->column_font_size = $request['column_font_size'];
            $app_setting->min_qty_error_msg = $request['min_qty_error_msg'];
            $app_setting->min_qty_error_msg_on_cart = $request['min_qty_error_msg_on_cart'];
            $app_setting->min_qty_msg_on_product_page = $request['min_qty_msg_on_product_page'];
            $app_setting->add_to_cart_text = $request['add_to_cart_text'];
			$app_setting->money_format = $money_format;
            $app_setting->save();
            $notification = array(
                'message' => 'Updated Successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('dashboard')->with('notification', $notification);
        } 
		else 
		{
            if ($request['app_status']) {
                $appconfig->app_status = $request['app_status'];
            } else {
                $appconfig->app_status = 0;
            }
            $appconfig->show_stock = $request['show_stock'];
            $appconfig->show_price = $request['show_price'];
            $appconfig->left_header_color = $request['left-header-color'];
            $appconfig->top_header_color = $request['top-header-color'];
            $appconfig->left_text_color = $request['left-text-color'];
            $appconfig->top_text_color = $request['top-text-color'];
            $appconfig->border_color = $request['border-color'];
            $appconfig->cell_color = $request['cell-color'];
            if ($request['border_status']) {
                $appconfig->border_status = $request['border_status'];
            } else {
                $appconfig->border_status = 0;
            }
            if ($request['cell_border_status']) {
                $appconfig->cell_border_status = $request['cell_border_status'];
            } else {
                $appconfig->cell_border_status = 0;
            }
            if ($request['show_default_price']) {
                $appconfig->show_default_price = $request['show_default_price'];
            } else {
                $appconfig->show_default_price = 0;
            }
            if ($request['quantity-color']) {
                $appconfig->quantity_color = $request['quantity-color'];
            }
            if ($request['price-color']) {
                $appconfig->price_color = $request['price-color'];
            }
            $appconfig->store_id = $shopid;

            $appconfig->price_label = $request['price_label'];
            if ($request['price_font_size']) {
                $appconfig->price_font_size = $request['price_font_size'];
            }
            $appconfig->quantity_label = $request['quantity_label'];
            if ($request['quantity_font_size']) {
                $appconfig->quantity_font_size = $request['quantity_font_size'];
            }
            if ($request['quantity_message_color']) {
                $appconfig->quantity_message_color = $request['quantity_message_color'];
            }
            if ($request['min_quantity_font_size']) {
                $appconfig->min_quantity_font_size = $request['min_quantity_font_size'];
            }
            $appconfig->additional_css = $request['additional_css'];

            $appconfig->allow_out_of_stock = $request['allow_out_of_stock'];

            // columns added on 26-03-2018
            $appconfig->quantity_position = $request['quantity_position'];
            $appconfig->price_position = $request['price_position'];
            $appconfig->show_total = $request['show_total'];
            $appconfig->row_total_label = $request['row_total_label'];
            $appconfig->row_font_size = $request['row_font_size'];
            $appconfig->column_total_label = $request['column_total_label'];
            $appconfig->column_font_size = $request['column_font_size'];

            $appconfig->min_qty_error_msg = $request['min_qty_error_msg'];
            $appconfig->min_qty_error_msg_on_cart = $request['min_qty_error_msg_on_cart'];
            $appconfig->min_qty_msg_on_product_page = $request['min_qty_msg_on_product_page'];
            $appconfig->add_to_cart_text = $request['add_to_cart_text'];
            $appconfig->money_format = $money_format;
            $appconfig->save();
            $notification = array(
                'message' => 'Updated Successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('dashboard')->with('notification', $notification);
        }
    }

    /*
     * displays product matrix on front page
     */

    public function frontend(Request $request) {
        $product_id = $request->product_id;
        $domain_name = $request->domain_name;
        $shop = session('shop');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $url = 'https://' . $shop . '/admin/products/' . $product_id . '.json';

        $store_id = $select_store[0]->id;

        $product_info = $sh->call([
            'URL' => $url,
            'METHOD' => 'GET'
                ], false);

        /* Load Configuration form product settings table */
        $product_settings = ProductSettings::where('product_id', '=', $product_id)->first();

        /* Load Configuration from general settings - appconfig table */
        $general_settings = AppConfig::where('store_id', '=', $select_store[0]->id)->first();

        // options and variants handling
        $options = $product_info->product->options;

        // Quantity error message
        $min_qty_error_msg = "Please enter minimum quantity";

        if (count($general_settings) > 0) {
            $app_status = $general_settings->app_status;

            //quantity
            $show_stock = $general_settings->show_stock;
            $quantity_label = $general_settings->quantity_label;
            $allow_out_of_stock = $general_settings->allow_out_of_stock;

            //price
            $show_price = $general_settings->show_price;
            $price_label = $general_settings->price_label;

            //total
            $show_total = $general_settings->show_total;
            $row_total_label = $general_settings->row_total_label;
            $column_total_label = $general_settings->column_total_label;

            // Quantity error message
            $min_qty_error_msg = $general_settings->min_qty_error_msg;

            if (trim($min_qty_error_msg) == "") {
                $min_qty_error_msg = "Please enter minimum quantity";
            }
        } else {
            $app_status = 1;
            // quantity
            $show_stock = null;
            $quantity_label = null;
            $allow_out_of_stock = null;

            //price
            $show_price = null;
            $price_label = null;

            //total
            $show_total = null;
            $row_total_label = null;
            $column_total_label = null;
        }

        if ($options[0]->name == "Title") {
            $app_status = 0;
        }


        if ($app_status != 1) {
            // app is disabled
            return "app_disabled";
        }

        $settings_array = array();

        $settings_array['show_stock'] = $show_stock;
        $settings_array['quantity_label'] = $quantity_label;
        $settings_array['allow_out_of_stock'] = $allow_out_of_stock;

        $settings_array['show_price'] = $show_price;
        $settings_array['price_label'] = $price_label;

        $settings_array['show_total'] = $show_total;
        $settings_array['row_total_label'] = $row_total_label;
        $settings_array['column_total_label'] = $column_total_label;


        $options_array = array();

        $options_array['row_option'] = array();

        $options_array['row_option']['id'] = "";
        $options_array['row_option']['product_id'] = "";
        $options_array['row_option']['name'] = "";
        $options_array['row_option']['position'] = "";
        $options_array['row_option']['values'] = array();

        $options_array['column_option'] = array();

        $options_array['column_option']['id'] = "";
        $options_array['column_option']['product_id'] = "";
        $options_array['column_option']['name'] = "";
        $options_array['column_option']['position'] = "";
        $options_array['column_option']['values'] = array();

        $options_array['dropdown_option'] = array();

        $options_array['dropdown_option']['id'] = "";
        $options_array['dropdown_option']['product_id'] = "";
        $options_array['dropdown_option']['name'] = "";
        $options_array['dropdown_option']['position'] = "";
        $options_array['dropdown_option']['values'] = array();

        if (count($options) > 0) {
            foreach ($options as $option) {
                if (count($product_settings) > 0) {
                    if ($product_settings->row_option != null) {
                        if ($option->id == $product_settings->row_option) {
                            $options_array['row_option'] = (array) $option;
                        } else if ($option->id == $product_settings->column_option) {
                            $options_array['column_option'] = (array) $option;
                        } else {
                            $options_array['dropdown_option'] = (array) $option;
                        }
                    } else {
                        if ($option->position == 1) {
                            $options_array['row_option'] = (array) $option;
                        } else if ($option->position == 2) {
                            $options_array['column_option'] = (array) $option;
                        } else {
                            $options_array['dropdown_option'] = (array) $option;
                        }
                    }
                } else {
                    if ($option->position == 1) {
                        $options_array['row_option'] = (array) $option;
                    } else if ($option->position == 2) {
                        $options_array['column_option'] = (array) $option;
                    } else {
                        $options_array['dropdown_option'] = (array) $option;
                    }
                }
            }
        }

        $variant_url = 'https://' . $shop . '/admin/products/' . $product_id . '/variants.json';

        $variants = $sh->call([
            'URL' => $variant_url,
            'METHOD' => 'GET'
                ], false);


        $variants_array_final = array();

        $variants_array = (array) $variants->variants;

        if (count($options_array['dropdown_option']['values']) > 0) {
            $dimension = '3';
            foreach ($options_array['dropdown_option']['values'] as $dropdown_option) {
                foreach ($options_array['column_option']['values'] as $column_option) {
                    // The first foreach loops through the row options
                    foreach ($options_array['row_option']['values'] as $row_option) {
                        // The second foreach loops through the column options
                        $variants_array_final[$dropdown_option][$column_option][$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                        // echo $this->getCellValue($row_option , $column_option , $variants);
                        // $variants = $this->getCellValue($row_option , $column_option , $variants);  
                    }
                }
            }
        } else if (count($options_array['column_option']['values']) > 0 && count($options_array['row_option']['values']) > 0) {
            $dimension = '2';
            foreach ($options_array['column_option']['values'] as $column_option) {
                // The first foreach loops through the row options
                foreach ($options_array['row_option']['values'] as $row_option) {
                    $dropdown_option = "";
                    // The second foreach loops through the column options
                    $variants_array_final[$column_option][$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                    // echo $this->getCellValue($row_option , $column_option , $variants);
                    // $variants = $this->getCellValue($row_option , $column_option , $variants);  
                }
            }
        } else if (count($options_array['row_option']['values']) > 0) {
            $dimension = '1';
            // The first foreach loops through the row options
            foreach ($options_array['row_option']['values'] as $row_option) {
                $dropdown_option = "";
                $column_option = "";
                // The second foreach loops through the column options
                $variants_array_final[$row_option] = $this->getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings);
                // echo $this->getCellValue($row_option , $column_option , $variants);
                // $variants = $this->getCellValue($row_option , $column_option , $variants);  
            }
        } else {
            // no options are there for these product
            $variants_array_final = array("no_options" => "no_options");
        }

        $total_width = 100 / (count($options_array['row_option']['values']) + 2);

        $variants_array_json = json_encode($variants_array_final);

        $min_qty = null;
        $disable_app_for_product = null;


        if (count($product_settings) > 0) {
            if ($product_settings->min_qty_required == '1') {
                $min_qty = $product_settings->min_qty_value;
            }

            if ($product_settings->disable_app_for_product == '1') {
                $disable_app_for_product = $product_settings->disable_app_for_product;
            }
        }

        $general_settings_json = json_encode($settings_array);

//        return view('products.productmatrix_frontview', compact('variants_array_final', 'options_array', 'product_id', 'domain_name', 'variants_array_json', 'total_width', 'general_settings_json', 'dimension', 'min_qty', 'min_qty_error_msg', 'show_total', 'row_total_label', 'column_total_label', 'show_price'));
        
        return view('products.productmatrix_frontview', compact('variants_array_final', 'options_array', 'product_id', 'domain_name', 'variants_array_json', 'total_width', 'dimension', 'min_qty', 'min_qty_error_msg', 'general_settings_json', 'column_total_label', 'row_total_label', 'variants_array_final', 'disable_app_for_product'));
    }

    /*
     * Creates associative array from variants array
     */

    public function getCellValue($dropdown_option, $column_option, $row_option, $variants_array, $general_settings) {
        $price = '';
        $variant_id = '';
        $inventory_quantity = '';
        $quantity_position = '';
        $quantity_label = '';
        $show_stock_qty = '';

        foreach ($variants_array as $variant) {
            if (($row_option == $variant->option1 || $row_option == $variant->option2 || $row_option == $variant->option3) && ($column_option == $variant->option1 || $column_option == $variant->option2 || $column_option == $variant->option3) && ($dropdown_option == $variant->option1 || $dropdown_option == $variant->option2 || $dropdown_option == $variant->option3)) {
                $price = $variant->price;
                $variant_id = $variant->id;


                if ($variant->inventory_management != null) {
                    if (count($general_settings) > 0) {
                        if ($general_settings->show_stock == 1) {
                            $inventory_quantity = $variant->inventory_quantity;
                            if ($general_settings->allow_out_of_stock != 1) {
                                if ($inventory_quantity == 0) {
                                    return "out of stock";
                                    continue;
                                }
                            }

                            $quantity_position = $general_settings->quantity_position;
                            $quantity_label = $general_settings->quantity_label;
                        }
                    }
                }

                return array('price' => $price, 'variant_id' => $variant_id, 'inventory_quantity' => $inventory_quantity, 'quantity_position' => $quantity_position, 'quantity_label' => $quantity_label);
            } else {
                //return array();
                continue;
            }
        }
    }

//    /*
//     * dropdown option ajax call
//     */
//
//    public function getMatrix(Request $request) {
//        $product_id = $request->product_id;
//        $domain_name = $request->domain_name;
//    }

    /*
     * productmatrix add items to cart
     */
    public function productMatrixAddCart() {
        dd('product matrix add cart');
    }

    public function get_product_settings(Request $request) {
        $product_id = $request->input('product_id');
        $shop_name = $request->input('shop_name');
        $count = $request->input('count');
        $store = DB::table('usersettings')->where('store_name', $shop_name)->first();
        $product_settings = ProductSettings::where(['store_id' => $store->id, 'product_id' => $product_id])->first();
        $response = array();
        if (count($product_settings) > 0) {
            if ($product_settings->min_qty_required == 1) {
                $min_qty = $product_settings->min_qty_value;
                if ($min_qty > $count) {
                    $response['result'] = 0;
                    $response['min_quantity'] = $min_qty;
                    echo json_encode($response);
                } else {
                    $response['result'] = 0;
                    echo json_encode($response);
                }
            } else {
                $response['result'] = 0;
                echo json_encode($response);
            }
        } else {
            $response['result'] = 0;
            echo json_encode($response);
        }
    }
    
     public function shortcodes() {
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $store_id = $select_store->id;
        $encrypted_store_id = $select_store->store_encrypt;
        $store_record = AppConfig::where('store_id', '=', $store_id)->first();
        
        $config_confirmation = $select_store->config_confirmation;
        return view('shortcode_settings', compact('store_record', 'encrypted_store_id', 'config_confirmation'));
    }
    
//      public function shortcodes_modified() {
//        $shop = session('shop');
//        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
//        $store_id = $select_store->id;
//        $encrypted_store_id = $select_store->store_encrypt;
//        $store_record = AppConfig::where('store_id', '=', $store_id)->first();
//
//        return view('shortcode_settings', compact('store_record', 'encrypted_store_id'));
//    }
	
	public function update_modal_status(Request $request)
	{
		$shop = $request->input('shop_name');
		$shop_find = ShopModel::where('store_name' , $shop)->first();		
		$shop_find->new_install = 'N';
		$shop_find->save();
	}
        
        public function configConfirmation(Request $request)
        {
            $shop = $request->input('shop_name');
		$shop_find = ShopModel::where('store_name' , $shop)->first();		
		$shop_find->config_confirmation = 'Y';
		$shop_find->save();
        }
}
