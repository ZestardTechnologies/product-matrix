<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\ShopModel;

class CheckConfig {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
//        if (session('shop')) {
//            $shop = session('shop');
//        } else {
//            if ($request['shop']) {
//                session(['shop' => $request['shop']]);
//                $shop = session('shop');
//            } else {
//                if (isset($_SERVER['HTTP_REFERER'])) {
//                    $url = $_SERVER['HTTP_REFERER'];
//                    $pieces = parse_url($url);
//                    session(['shop' => $pieces['host']]);
//
//                    $shop = session('shop');
//                    //echo '<script>window.top.location.href="admin/apps"</script>';	
//                }
//            }
//        }

        $shop = session('shop');
        
        $shop_find = ShopModel::where('store_name', $shop)->first();
        
        
        if (!count($shop_find) > 0 || $shop_find->config_confirmation == "N") {
           return redirect("/shortcodes");
        }

        return $next($request);
    }
}
