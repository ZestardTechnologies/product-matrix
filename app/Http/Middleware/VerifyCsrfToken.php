<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'search','product/get/matrix', 'check-quantity', 'product-config', 'get-settings', 'check-quantity', 'product-config', 'check-app', 'frontend', 'update-modal-status','update/general/settings'
    ];
}
