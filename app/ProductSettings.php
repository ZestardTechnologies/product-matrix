<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSettings extends Model
{

    protected $table = 'product_settings';
        protected $primaryKey = 'id';
    
    protected $fillable = ['id' , 'row_option' , 'column_option' , 'min_qty_required' , 'min_qty_value' ,'min_qty_error_msg' ,'product_id' ,'store_id',
        'disable_app_for_product'];
    
    public $timestamps = false;
}
