<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<div class=" row" style="padding:0 40px 40px 40px;height:100%;margin:0;background:#f4f6f8;margin:50px">
	<div class="help_page">
		<h1 style="padding-bottom:40px;">Charge was declined </h1>			
			<b>How to Uninstall the app?</b>
			<div class="panel-body">
				<ul class="ul-help">
					<li>To uninstall the App, go to <a href="https://<?php echo $store_name;?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
					<li>Click on delete icon of Zestard Product Matrix App.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/uninstall.png') }}" data-toggle="modal" data-target="#help_modal"><b>See Example</b></a></li>						
				</ul>									
			</div>			
		<div class="modal fade" id="help_modal" role="dialog">
			<div class="modal-dialog">      
				<div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Help</h4>
					</div>
					<img src="" / style="width:100%;">			
				</div>      
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function(){
		$(".screenshot").click(function(){
			$(".modal-content img").attr("src", $(this).attr("data-src"));
		});
    });
</script>

