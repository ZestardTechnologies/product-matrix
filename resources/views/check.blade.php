<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">  
<link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ url('css/style.css') }}" />     
<link rel="stylesheet" href="{{ url('css/jquery-ui.css') }}" />     
<script src="{{ url('js/zestard_jquery_3.3.1.js') }}"></script>
<script src="{{ url('js/jquery-ui.js') }}"></script>
<script>
    $(document).ready(function () {
        $(".slide_down").click(function () {
            var display = $("#shortcode_info").css("display");
            if (display == "none")
            {
                $(".slide_down i").removeClass("fa fa-chevron-up");
                $(".slide_down i").addClass("fa fa-chevron-down");
            } else
            {
                $(".slide_down i").removeClass("fa fa-chevron-down");
                $(".slide_down i").addClass("fa fa-chevron-up");
            }
            $("#shortcode_info").slideToggle();
        });
    });
</script>
<div>
    <div class="container formcolor">  
        <ul class="nav nav-tabs dashboard_tabs">
            <li class="active"><a href="dashboard">General Settings</a></li>
            <li><a href="cut-off">Cut Off Settings</a></li>
            <li><a href="delivery-time">Delivery Time Settings</a></li>
        </ul>


        <br>
        <h2 class="sub-heading">General Settings</h2>
        <div class="row formcolor_row">
            <div class="col-sm-4 padding_left">
                <strong>App Active?</strong>
                <span class="onoff"><input type="checkbox" value="1" id="checkboxID0"><label for="checkboxID0"></label></span>
            </div>                
            <div class="col-sm-4 padding_middle"><strong>Show Notes on Website?</strong>
                <span class="onoff"><input type="checkbox" value="1" id="checkboxID3"><label for="checkboxID3"></label></span></div>
            <div class="col-sm-4 padding_right" style="border: 0;"><strong>Allowed Pre-order Time</strong></div>                
        </div>
        
        
        
        <div class="row formcolor_row">
            <div class="col-sm-4 padding_left">
                <div class="dateformat_option">
                    <div><strong>Select Language</strong></div>
                    <div>                            
                        <select name="language" class="form-control">       
                            <option value="sq">Albanian (Gjuha shqipe)</option>
                            <option value="ar">Arabic (&#8235;(&#1604;&#1593;&#1585;&#1576;&#1610;</option>
                            <option value="hy">Armenian (&#1344;&#1377;&#1397;&#1381;&#1408;&#1381;&#1398;)</option>
                            <option value="bg">Bulgarian (&#1073;&#1098;&#1083;&#1075;&#1072;&#1088;&#1089;&#1082;&#1080; &#1077;&#1079;&#1080;&#1082;)</option>
                            <option value="ca">Catalan (Catal&agrave;)</option>
                            <option value="zh-CN">Chinese Simplified (&#31616;&#20307;&#20013;&#25991;)</option>
                            <option value="zh-TW">Chinese Traditional (&#32321;&#39636;&#20013;&#25991;)</option>
                            <option value="hr">Croatian (Hrvatski jezik)</option>
                            <option value="cs">Czech (Ce&ouml;tina)</option>
                            <option value="da">Danish (Dansk)</option>
                            <option value="nl">Dutch (Nederlands)</option>
                            <option value="eo">Esperanto</option>
                            <option value="en">English</option>
                            <option value="fa">Farsi/Persian (&#8235;(&#1601;&#1575;&#1585;&#1587;&#1740;</option>
                            <option value="fi">Finnish (suomi)</option>
                            <option value="fr">French (Fran&ccedil;ais)</option>
                            <option value="de">German (Deutsch)</option>
                            <option value="el">Greek (&#917;&#955;&#955;&#951;&#957;&#953;&#954;&#940;)</option>
                            <option value="he">Hebrew (&#8235;(&#1506;&#1489;&#1512;&#1497;&#1514;</option>
                            <option value="hu">Hungarian (Magyar)</option>
                            <option value="is">Icelandic (&Otilde;slenska)</option>
                            <option value="id">Indonesian (Bahasa Indonesia)</option>
                            <option value="it">Italian (Italiano)</option>
                            <option value="ja">Japanese (&#26085;&#26412;&#35486;)</option>
                            <option value="ko">Korean (&#54620;&#44397;&#50612;)</option>
                            <option value="lv">Latvian (Latvie&ouml;u Valoda)</option>
                            <option value="lt">Lithuanian (lietuviu kalba)</option>
                            <option value="ms">Malaysian (Bahasa Malaysia)</option>
                            <option value="no">Norwegian (Norsk)</option>
                            <option value="pl">Polish (Polski)</option>
                            <option value="pt-BR">Portuguese/Brazilian (Portugu&ecirc;s)</option>
                            <option value="ro">Romanian (Rom&acirc;n&#259;)</option>
                            <option value="ru">Russian (&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;)</option>
                            <option value="sr">Serbian (&#1089;&#1088;&#1087;&#1089;&#1082;&#1080; &#1112;&#1077;&#1079;&#1080;&#1082;)</option>
                            <option value="sr-SR">Serbian (srpski jezik)</option>
                            <option value="sk">Slovak (Slovencina)</option>
                            <option value="sl">Slovenian (Slovenski Jezik)</option>
                            <option value="es">Spanish (Espa&ntilde;ol)</option>
                            <option value="sv">Swedish (Svenska)</option>
                            <option value="th">Thai (&#3616;&#3634;&#3625;&#3634;&#3652;&#3607;&#3618;)</option>
                            <option value="tr">Turkish (T&uuml;rk&ccedil;e)</option>
                            <option value="uk">Ukranian (&#1059;&#1082;&#1088;&#1072;&#1111;&#1085;&#1089;&#1100;&#1082;&#1072;)</option>
                        </select>		
                        </select> 
                    </div>
                </div>
                <div class="datepicker_label">
                    <div><strong>Datepicker label</strong></div>
                    <div><input type="text" name="datepicker_label" value="" class="form-control"></div>
                </div>
                <div class="datepicker_validate">
                    <div>
                        <strong>Delivery Date is Required Field?</strong>
                        <span class="onoff"><input type="checkbox" value="1" id="checkboxID1"><label for="checkboxID1"></label></span>
                    </div>
                </div>  
                <div class="datepicker_label">
                    <div><strong>Validation Message When Delivery Date is Required</strong></div>
                    <div><input type="text" name="date_error_message" value="" class="form-control"></div>
                </div>
                <div class="datepicker_label">
                    <strong>Show Datepicker label on Website?</strong>
                    <span class="onoff"><input type="checkbox" value="1" id="checkboxID6"><label for="checkboxID6"></label></span>
                </div>
                <div class="datepicker_validate">
                    <div>
                        <strong>Show Date Format on Website?</strong>
                        <span class="onoff"><input type="checkbox" value="1" id="checkboxID2"><label for="checkboxID2"></label></span>
                    </div>                        
                </div>
                <div class="dateformat_option">
                    <div><strong>Date Format</strong></div>
                    <div>
                        <select name="date_format" class="form-control">
                            <option value="mm/dd/yy" >mm/dd/yyyy</option>
                            <option value="yy/mm/dd" >yyyy/mm/dd</option>
                            <option value="dd/mm/yy" >dd/mm/yyyy</option>
                        </select> 
                    </div>
                </div>
                <div class="datepicker_validate">
                    <div>
                        <strong>Show Time on Front?</strong>
                        <span class="onoff"><input type="checkbox" value="1" id="checkboxID5"><label for="checkboxID5"></label></span>
                    </div>                       
                </div>
                <div class="datepicker_validate">
                    <div><strong>Time is Required Field?</strong>
                        <span class="onoff"><input type="checkbox" value="1" id="checkboxID4"><label for="checkboxID4"></label></span>
                    </div>
                    <!--<div>
                       <select name="time_require_option" class="form-control">
                           <option value="0" >No</option>
                           <option value="1" >Yes</option>
                       </select> 
                    </div>-->
                </div>
                <div class="datepicker_label">
                    <div><strong>Delivery Time Title</strong></div>
                    <div><input type="text" name="time_label" value="" class="form-control"></div>
                </div>
                <div class="datepicker_label">
                    <div><strong>Time Default option label</strong></div>
                    <div><input type="text" name="time_default_option_label" value="" class="form-control"></div>
                </div>
                <div class="order_note">
                    <div><span class="note"><strong>Delivery Time</strong> (Add <b>","</b>(comma) separated values)</span></div>
                    <div>
                        <textarea id="delivery_time" name="delivery_time" class="form-control" rows="5" ></textarea>
                    </div>
                </div>


            </div>
            <div class="col-sm-4 padding_middle">					
                <div class="order_note">
                    <div><strong>Notes</strong></div> 
                    <div>
                        <textarea id="admin_order_note" name="admin_order_note" class="form-control" rows="5" ></textarea>                            
                    </div>
                </div>
                <br>
                <div>   
                    <span class="checkboxFive">
                        <input type="checkbox" value="1" id="checkbox1" name="exclude_block_date_status">
                        <label for="checkbox1"></label>
                    </span> 
                    <strong>Add Block Days Interval</strong>  
                </div>   
                <div class="note" style="margin-top: 0;margin-bottom: 10px;">
                    <strong>Note:</strong> Use "Add Block Days", if you wish to add extra days for interval due to Block Days.
                </div>
                <br>
                <br>
                <strong>Block Day(s)</strong>
                <div class="form-group block_days">
                    <select name="days[]" size="8" multiple class="form-control">
                        <option value="all_allow" selected >All Allows</option>
                        <option value="Sunday" >Sunday</option>
                        <option value="Monday" >Monday</option>
                        <option value="Tuesday" >Tuesday</option>
                        <option value="Wednesday" >Wednesday</option>
                        <option value="Thursday" >Thursday</option>
                        <option value="Friday" >Friday</option>
                        <option value="Saturday" >Saturday</option>
                    </select>
                </div>
                <div class="note">
                    <strong>Note:</strong> Selected day will be blocked and user won't be able to select it as a delivery date.
                </div>
                <div class="main-date" id="main-date">
                    <div><strong>Blocked Date(s)</strong></div>

                    <div class="form-group date-box">
                        <input type="text" name="saveblockdate[]" class="saveblockdate form-control">
                    </div>                      

                </div>
                <div class="form-group">
                    <a class="addrow"><i class="fa fa-plus"></i>Add Date(s)</a>
                </div>
                <div class="note">
                    <strong>Note:</strong>List of dates that will be blocked for delivery so user won't be able to select it for the order delivery.
                </div>
            </div>
            <div class="col-sm-4 padding_right" style="border: 0;">
                <div class="form-group margin_bottom_0" >
                    <input class="form-control" type="number" name="allowed_month" min="0" id="example-number-input" style="width:70px;" >
                </div>
                <div class="note">
                    <strong>Note:</strong> This option will allow user to book order before "X" number of Months on the Website.
                </div>                    
                <br />
                <div><strong>Minimum Date Interval</strong></div>
                <div class="form-group margin_bottom_0">
                    <input class="form-control" type="number" min="0" name="intervel" id="example-number-input" style="width:70px;">
                </div>
                <div class="note">
                    <strong>Note:</strong> For "Today Delivery" it should be "0" otherwise it should be the interval of days from the current day to allow user to select delivery date.
                </div>
                <br/>                    
                <div class="datepicker_validate">
                    <div>
                        <span class="checkboxFive">
                            <input type="checkbox" value="1" id="checkbox2" name="datepicker_display_on">
                            <label for="checkbox2"></label>
                        </span>
                        <strong>Show Calendar visible by default</strong>
                    </div>
                </div>
                <div class="main-date" id="main-date">
                    <div>
                        <span class="checkboxFive">
                            <input type="checkbox" value="1" id="checkbox3" name="default_date">
                            <label for="checkbox3"></label>
                        </span>
                        <strong>Next available date set as a selected</strong>
                    </div>
                </div>
                <br>
                <div class="">
                    <div>
                        <span class="checkboxFive">
                            <input type="checkbox" value="1" id="checkbox4" name="add_delivery_information">
                            <label for="checkbox4"></label>
                        </span>
                        <strong>Add Delivery Information under Order Instructions</strong>
                    </div>
                </div>
                <div>
                    <br/>
                    <div><strong>Cut Off Time&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; Active </strong><span class="checkboxFive" >
                            <input type="checkbox" value="cuttoff_on" id="checkbox5" name="cuttoff_time">
                            <label for="checkbox5"></label>
                        </span>
                    </div>  
                    <div class="cuttofftime_box">
                        <div class="display_block">
                            <label for="hours">Hour</label>
                            <select name="hours" class="form-control">                                    
                            </select>
                        </div>
                        <div class="display_block">&nbsp;&nbsp;<strong>:</strong>&nbsp;&nbsp;</div>
                        <div class="display_block">
                            <label for="minute">Minute</label>
                            <select name="minute" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="note">
                        <strong>Note:</strong> For "Next Day Delivery" if the cut off time is enable & set then it will allow user to select day after the "Interval Date" as first available delivery date.
                    </div>
                </div>
                <div class="order_note">
                    <div><strong>Additional CSS</strong></div>
                    <div>
                        <textarea name="additional_css" class="form-control" rows="4" ></textarea>
                    </div> 
                    <div class="note">
                        <strong>Note:</strong> You can add CSS For Example: .body {margin:0px;}
                    </div> 
                </div>
            </div>  
        </div>  
        <div class="col-md-12 formcolor sticky_formcolor">
            <?php
            if (Session::has('shop')) {
                $url = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
            } else {
                $url = "#";
            }
            ?>
            <?php
            if (Session::has('shop')) {
                $url1 = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
            } else {
                $url1 = "#";
            }
            ?>
            <div class="shortcode_heading col-sm-6">
                <h2 class="sub-heading subleft col-md-3">Shortcode</h2>
                <div class="col-sm-9">
                    <div class="copystyle_wrapper">
                        <textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{% include 'delivery-date' %}" ?></textarea>
                        <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_script()"><i class="fa fa-check"></i> Copy</btn>
                    </div>
                </div>
            </div>
            <div class="shortcode_heading col-sm-6">
                <h2 class="slide_down sub-heading subleft col-md-12">Where to paste Shortcode?<i class="fa fa-chevron-up"></i></h2>
                <div class="col-sm-12" id="shortcode_info">          
                    <ul class="shortcode-note">
                        <li>Copy and paste the shortcode in your <a href="<?php echo $url; ?>" target="_blank"><b>cart page</b></a> see <a class="info_css" href="{{ asset('image/Delivery date short code.png') }}"><b>Example</b></a>
                        </li>
                        <li>If you dont find form tag in your cart page so you have to paste the short code in the 
                            <a href="<?php echo $url1; ?>" target="_blank"><b>Cart Template</b></a> see <a class="info_css" href="{{ asset('image/info_delivery date.png') }}"><b>Example</b></a>
                        </li>
                        <li>
                            If your cart page is not opening as url https://your-store-name/cart, Then Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right
                        </li>
                    </ul>           
                </div>
            </div>            
        </div>      
        </form>					
        </form>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $(".blockeddate").datepicker({
            minDate: +0,
            dateFormat: 'dd/mm/yy'
        });
        $(".saveblockdate").datepicker({
            minDate: +0,
            dateFormat: 'dd/mm/yy'
        });
    });
</script>