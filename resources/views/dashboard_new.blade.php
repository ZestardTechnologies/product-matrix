@extends('header')
@section('content')
<?php
if(Session::has('shop')) 
{
    $url = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} 
else 
{
    $url = "#";
    $url_cart_template = "#";
    $url_product_page = "#";
    $url_cart_page = "#";
}
?>
<?php
if (Session::has('shop')) {
    $url1 = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
} else {
    $url1 = "#";
}
?>

<style>
    .nav-tabs>li:last-child {
        //float: right;
    }
    .onoff
    {
        margin-top : 3px;
        display: inline-block;
        vertical-align: middle;
        zoom: 1;
        position: relative;
        cursor: pointer;
        width: 45px;
        height: 20px;
        line-height: 24px;
        font-size: 12px;
        font-family: inherit;
        float: right;
    }
</style>

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'General Settings',
            buttons: {
                primary: {
                    label: 'Save',
                    message: 'form_submit'
                },
                secondary:
                        [
                            {
                                label: 'Product Settings',
                                href: '{{ url("/products") }}',
                                loading: true
                            },
                            {
                                label: 'Shortcodes',
                                href: 'shortcodes',
                                loading: true
                            },
                            {
                                label: 'Help',
                                href: '{{ url("/help") }}',
                                loading: true
                            }
                        ]
            }
        });
    });
    
    $(document).ready(function () {
        $(".slide_down").click(function () {
            var display = $("#shortcode_info").css("display");
            $("#shortcode_info").slideToggle();
            if (display == "none")
            {
                $(".slide_down i").removeClass("fa fa-chevron-up");
                $(".slide_down i").addClass("fa fa-chevron-down");
            } else
            {
                $(".slide_down i").removeClass("fa fa-chevron-down");
                $(".slide_down i").addClass("fa fa-chevron-up");
            }
        });
        $(".close_box").click(function () {
            var display = $("#shortcode_info").css("display");
            $("#shortcode_info").slideToggle();
            if (display == "none")
            {
                $(".slide_down i").removeClass("fa fa-chevron-up");
                $(".slide_down i").addClass("fa fa-chevron-down");
            } else
            {
                $(".slide_down i").removeClass("fa fa-chevron-down");
                $(".slide_down i").addClass("fa fa-chevron-up");
            }
        });
    });
</script>

<form id="productmatrix" method="post" data-shopify-app-submit="form_submit" data-toggle="validator" action="{{ action('ProductController@updateGeneralSettings') }}">
    <marquee class="notification_marquee">Our application works based on javascript only, there may be a conflict between application code and your theme (other installed applications or conflict with other scripts).Please contact on support e-mail (<a href="mailto:support@zestard.com">support@zestard.com</a>) before you try for multiple time <strong>install-uninstall</strong> process.</marquee>
	{{ csrf_field() }}
    <div class="formcolor">
        <ul class="nav nav-tabs dashboard_tabs">
            <li>
                <a href="shortcodes">Configuration</a>
            </li>
            <li class="active">
                <a href="dashboard">General Settings</a>
            </li>
            <li>
                <a href="products">Product List</a>
            </li>

            <li class="app_tab">
                <div class ="col-sm-6">
                    <strong>App Active?</strong>
                </div>
                <div class ="col-sm-6">
                    <span class="onoff">
                        <input type="checkbox" value="1" id="app_status" name = "app_status" @if(count($store_record) > 0) @if($store_record->app_status == 1)  {{ "checked" }} @endif @endif>
                               <label for="app_status"></label>
                    </span>
                </div>
            </li>
        </ul>

        <div class ="row formcolor-row">

            <div class="col-sm-4 padding_left">
                <!-- Price Font Size -->
                <h2 class="sub-heading">Design Settings</h2>
                <label class="header-color" for="price_font_size">Price Font size</label>
                <select class="form-control" id="price_font_size" name="price_font_size">
                    <option value="" selected disabled>Please select</option>
                    @for($i=5 ; $i<=18 ; $i++)
                    <option @if(count($store_record) > 0) <?php echo ($store_record->price_font_size == $i ? ' selected' : '') ?> @endif>
                             {{ $i }}px
                </option>
                @endfor
            </select>            
        </div>

        <!-- Show Stock Qty? -->
        <div class ="col-sm-4 padding_middle">
            <h2 class="sub-heading">Product Configuration Settings</h2>    
            <strong>Show Stock?</strong>
            <span class="onoff"><input type="checkbox" value="1" id="show_stock" name = "show_stock" @if(count($store_record) > 0) @if($store_record->show_stock == 1)  {{ "checked" }} @endif @endif><label for="show_stock"></label></span>
            <div class="note">			
                <strong>Note:</strong>It won't display stock value if you have selected "Don't track inventory" of inventory policy on individual product variant settings page
                <!--strong>Note:Quantity Won't be displayed if Shopify doesn't track this product's inventory. Only if "Show Stock Quantity" is enabled, quantity position dropdown will work.</strong-->
            </div>
        </div>

        <!-- Row Total Label -->
        <div class ="col-sm-4 padding_right">
            <h2 class="sub-heading">Language Settings</h2>
            <label class="header-color" for="row_total_label">Row Total Label</label>
            <input type="text" name="row_total_label" id ="row_total_label"  @if(count($store_record) > 0) value="{{$store_record->row_total_label}}" @else value="" @endif class = "form-control">
        </div>
    </div>

    <div class ="row formcolor-row">
        <div class="col-sm-4 padding_left">                        
            <label class="header-color" for="quantity_font_size">Quantity Font size</label>
            <select class="form-control" id="quantity_font_size" name="quantity_font_size">
                <option value="" selected disabled>Please select</option>
                @for($i=5 ; $i<=18 ; $i++)
                <option @if(count($store_record) > 0) <?php echo ($store_record->quantity_font_size == $i ? ' selected' : '') ?> @endif>
                         {{ $i }}px
            </option>
            @endfor
        </select>
    </div> 	  

    <div class="col-sm-4 padding_middle">
        <label class="header-color" for="quantity_position">Stock Position</label>
        <select class="form-control" id="quantity_position" name="quantity_position">
            <option value="" selected disabled>Please select</option>
            <option value ="show_above_input_box" @if(count($store_record) > 0) <?php echo ($store_record->quantity_position == "show_above_input_box" ? ' selected' : '') ?> @endif>
                    Show above input box
        </option>
        <option value ="show_below_input_box" @if(count($store_record) > 0) <?php echo ($store_record->quantity_position == "show_below_input_box" ? ' selected' : '') ?> @endif>
                Show below input box
    </option>
</select>
</div> 


<div class="col-sm-4 padding_right">
    <label class="header-color" for="column_total_label">Column Total Label</label>
    <input type="text" name="column_total_label" id ="column_total_label" @if(count($store_record) > 0) value="{{$store_record->column_total_label}}" @else value="" @endif class = "form-control">
</div>	
</div>



<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">
        <label class="header-color" for="row_font_size">Row Font Size</label>
        <select class="form-control" id="row_font_size" name="row_font_size">
            <option value="" selected disabled>Please select</option>
            @for($i=5 ; $i<=18 ; $i++)
            <option @if(count($store_record) > 0) <?php echo ($store_record->row_font_size == $i ? ' selected' : '') ?> @endif>
                     {{ $i }}px
        </option>
        @endfor
    </select>
</div>


<div class="col-sm-4 padding_middle">		
    <strong>Show Price?</strong>
    <span class="onoff"><input type="checkbox" value="1" id="show_price" name  = "show_price" @if(count($store_record) > 0) @if($store_record->show_price == 1)  {{ "checked" }} @endif @endif><label for="show_price"></label></span>

    <div class="note">
        <strong>Note:</strong>Only if "Show Price" is enabled, Price Position dropdown will work.
    </div>
</div>



<div class="col-sm-4 padding_right">
    <label class="header-color" for="quantity_label">Quantity Label</label>
    <input type="text" name="quantity_label" @if(count($store_record) > 0) value="{{$store_record->quantity_label}}" @else value="" @endif class = "form-control">
</div>	

</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">
        <label class="header-color" for="column_font_size">Column Font Size</label>
        <select class="form-control" id="column_font_size" name="column_font_size">
            <option value="" selected disabled>Please select</option>
            <option value="" selected disabled>Please select</option>
            @for($i=5 ; $i<=18 ; $i++)
            <option @if(count($store_record) > 0) <?php echo ($store_record->column_font_size == $i ? ' selected' : '') ?> @endif>
                     {{ $i }}px
        </option>
        @endfor
    </select>
</div>	

<div class ="col-sm-4 padding_middle">
    <label class="header-color" for="price_position">Price Position</label>
    <select class="form-control" id="price_position" name="price_position">
        <option value="" selected disabled>Please select</option>
        <option value = "show_above_input_box_before_quantity" @if(count($store_record) > 0) <?php echo ($store_record->price_position == "show_above_input_box_before_quantity" ? ' selected' : '') ?> @endif>
                Show above input box before quantity
    </option>
    <option value = "show_above_input_box_after_quantity" @if(count($store_record) > 0) <?php echo ($store_record->price_position == "show_above_input_box_after_quantity" ? ' selected' : '') ?> @endif>
            Show above input box after quantity
</option>
<option value ="show_below_input_box_before_quantity" @if(count($store_record) > 0) <?php echo ($store_record->price_position == "show_below_input_box_before_quantity" ? ' selected' : '') ?> @endif>
        Show below input box before quantity
</option>

<option value ="show_below_input_box_after_quantity" @if(count($store_record) > 0) <?php echo ($store_record->price_position == "show_below_input_box_after_quantity" ? ' selected' : '') ?> @endif>
        Show below input box after quantity
</option>
</select>
</div>

<div class="col-sm-4 padding_right">
    <label class="header-color" for="price_label">Price Label</label>
    <input type="text" name="price_label" @if(count($store_record) > 0) value="{{$store_record->price_label}}" @else 
           value="" @endif class = "form-control">
</div>

</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">                        
        <label class="header-color" for="min_quantity_font_size	">Minimum Quantity Font size</label>
        <select class="form-control" id="min_quantity_font_size	" name="min_quantity_font_size">
            <option value="" selected disabled>Please select</option>
            @for($i=5 ; $i<=18 ; $i++)
            <option @if(count($store_record) > 0) <?php echo ($store_record->min_quantity_font_size == $i ? ' selected' : '') ?> @endif>
                     {{ $i }}px
        </option>
        @endfor
    </select>
</div>  

<div class ="col-sm-4 padding_middle">
    <label class="header-color" for="show_total">Show Total</label>
    <select class="form-control" id="show_total" name="show_total">
        <option value="" selected disabled>Please select</option>
        <option value ="no" @if(count($store_record) > 0) <?php echo ($store_record->show_total == "no" ? ' selected' : '') ?> @endif>
                No
    </option>
    <option value ="row_total" @if(count($store_record) > 0) <?php echo ($store_record->show_total == "row_total" ? ' selected' : '') ?> @endif>
            Row Total
</option>
<option value ="column_total" @if(count($store_record) > 0) <?php echo ($store_record->show_total == "column_total" ? ' selected' : '') ?> @endif>
        Column Total
</option>
<option value ="both" @if(count($store_record) > 0) <?php echo ($store_record->show_total == "both" ? ' selected' : '') ?> @endif>
        Both (Row total and Column total)
</option>
</select>
</div>
<div class="col-sm-4 padding_right">
    <label class="header-color" for="quantity_label">Add to Cart Button Text</label>
    <input type="text" name="add_to_cart_text" @if(count($store_record) > 0) value="{{$store_record->add_to_cart_text}}" @else value="" @endif class = "form-control">						
</div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">			
        <strong>Show Product Matrix Box Border?</strong>
        <span class="onoff"><input type="checkbox" value="1" id="border_status" name = "border_status" @if(count($store_record) > 0) @if($store_record->border_status == 1)  {{ "checked" }} @endif @endif><label for="border_status"></label></span>
        <div class="note">
            <strong>Note:</strong>In order for "Product Matrix Box Border Color" setting to work, keep this option enabled
            <br/>
            <br/>
        </div>
    </div>

    <div class="col-sm-4 padding_middle">
        <label for="allow_out_of_stock">Allow out of stock products to Order</label>				
        <span class="onoff"><input type="checkbox" value="1" id="allow_out_of_stock" name = "allow_out_of_stock" @if(count($store_record) > 0) @if($store_record->allow_out_of_stock == 1)  {{ "checked" }} @endif @endif><label for="allow_out_of_stock"></label></span>                
        <div class="note">
            <!--strong>Note:In order to "Allow out of stock" feature to work keep this checkbox checked for each variant of the product as shown in this</strong-->
            <strong>Note:</strong>"Allow out of stock" feature will work if we keep checkbox checked as shown in the below screenshot on individual product variant settings
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/out_of_stock.png') }}" href="javascript:;"><b>Example</b></a> else this feature won't work and won't display any error
        </div>
    </div>	
    <div class="col-sm-4 padding_right">        
		<!--label class="header-color" for="min_qty_required">Minimum Quantity Required</label>
        <span class="onoff"><input type="checkbox" value="1" id="min_qty_required" name = "min_qty_required" @if(count($store_record) > 0) @if($store_record->allow_out_of_stock == 1)  {{ "checked" }} @endif @endif><label for="min_qty_required"></label></span>                
		<label class="header-color" for="min_qty_required">Enter Minimum Quantity</label>
		<input type="text" name="min_qty_value" @if(count($store_record) > 0) value="{{$store_record->min_qty_value}}" @else value="" @endif class = "form-control"-->								
		<label class="header-color" for="quantity_label">Minimum Quantity Message</label>
        <input type="text" name="min_qty_msg_on_product_page" @if(count($store_record) > 0) value="{{$store_record->min_qty_msg_on_product_page}}" @else value="" @endif class = "form-control">						
    </div>
</div>
<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">			
        <strong>Show Cell Border?</strong>
        <span class="onoff"><input type="checkbox" value="1" id="cell_border_status" name = "cell_border_status" @if(count($store_record) > 0) @if($store_record->cell_border_status == 1)  {{ "checked" }} @endif @endif><label for="cell_border_status"></label></span>
        <div class="note">
            <strong>Note:</strong>In order for "Product Matrix Cell Color" setting to work, keep this option enabled
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
    </div>	

    <div class="col-sm-4 hide_default_price_div padding_middle">			
        <strong>Show Default Price</strong>
        <span class="onoff"><input type="checkbox" value="1" id="default_price_status" name = "show_default_price" @if(count($store_record) > 0) @if($store_record->show_default_price == 1)  {{ "checked" }} @endif @endif><label for="default_price_status"></label></span>                				
        <strong><label>Note:If you want to hide default price add following class in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/copy_price_class.png') }}" href="javascript:;"><b>Example</b></a></label>				
        </strong> 				
        <div class="copystyle_wrapper">
            <br>	
            <textarea id="hide_default_price" rows="1" class="form-control short-code"  readonly=""><?php echo "hide-default-price"; ?>
            </textarea>
            <btn id="copy_cart_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="hide_default_price()"><i class="fa fa-check"></i> Copy</btn>
            <label class="copy_message">
            </label>
        </div>		
        <div class="alert alert-success alert-dismissable hide_default_price"><a href="javascript:;" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>
    </div>

    <div class ="col-sm-4 padding_right">
        <?php $quantity_message = ""; ?>
        @if(count($store_record) >0) 
        @if(isset($store_record->min_qty_error_msg))
        <?php $quantity_message = $store_record->min_qty_error_msg ?>
        @endif 
        @endif
        <label for="min_qty_value">Minimum Quantity Error Message</label> 
        <div>
            <input name="min_qty_error_msg" class="form-control" value= " {{trim($quantity_message)}}"></input>
        </div>
        <div class="note">
            <strong>Note:</strong>This message will be displayed when customer clicks on add to cart & total quantity of product 
            entered by customer is less than the required quantity of product.
        </div> 	 	
    </div>

</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">
        <h2 class="sub-heading">Color Settings</h2>	
        <p class="colorlabel"><label class="header-color" for="broder-color">Product Matrix Box Border Color</label></p>
        <input type="text" name="border-color" class="showInputBorder" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->border_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>

    <div class="col-sm-4 padding_middle">
        <input type ="hidden"/>
    </div>


    <div class ="col-sm-4 padding_right">
        <label for="min_qty_value">Minimum Quantity Error Message on Cart Page</label> 
        <div>
            <?php $cart_message = ""; ?>
            @if(count($store_record) >0) 
            @if(isset($store_record->min_qty_error_msg_on_cart))
            <?php $cart_message = $store_record->min_qty_error_msg_on_cart ?>
            @endif 
            @endif
            <input name="min_qty_error_msg_on_cart" class="form-control" value="{{trim($cart_message)}}" ></input>
        </div>
        <div class="note">
            <strong>Note:</strong> 	This message will be displayed on cart page when customer updates quantity of product variant on cart page & 
            remaining quantity is less than the required quantity of product.
        </div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="cell-color">Product Matrix Cell Color</label></p>
        <input type="text" name="cell_color" class="showInputBorder" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->cell_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="top-text-color">Row Option Font Color</label></p>
        <input type="text" name="top-text-color" class="showInputTopText" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->top_text_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="left-text-color">Column Option Font Color</label></p>
        <input type="text" name="left-text-color" class="showInputLeftText" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->left_text_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>


<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="top-header-color">Row Option Background Color</label></p>
        <input type="text" name="top-header-color" class="showInputTopHeader" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->top_header_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel">
            <label class="header-color" for="left-header-color">Column Option Background Color</label>
        </p>
        <input type="text" name="left-header-color" class="showInputLeftHeader" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->left_header_color}}" @else value="" @endif>
               <div class ="border-full-first"></div>
    </div>
</div>


<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="price-color">Price Color</label></p>
        <input type="text" name="price-color" class="showInputPrice" style="display: none;" @if(count($store_record) > 0) 
               value="{{$store_record->price_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="quantity-color">Quantity Color</label></p>
        <input type="text" name="quantity-color" class="showInputQuantity" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->quantity_color}}" @else value="" @endif>
         <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <p class="colorlabel"><label class="header-color" for="quantity-message-color">Quantity Message Font Color</label></p>
        <input type="text" name="quantity-message-color" class="showInputQuantity" style="display: none;" @if(count($store_record) > 0) value="{{$store_record->quantity_message_color}}" @else value="" @endif>
               <div class ="border-full"></div>
    </div>
</div>

<div class ="row formcolor-row">
</div>
<div class ="row formcolor-row">
    <div class="col-sm-4 padding_left">		
        <div class="order_note">
            <div><strong>Additional CSS</strong></div>
            <div>
                <?php $additional_css = ""; ?>
                @if(count($store_record) >0) 
                    @if(isset($store_record->additional_css))
                     <?php $additional_css = $store_record->additional_css ?>
                    @endif 
                @endif
                <textarea name="additional_css" class="form-control" rows="4">{{trim($additional_css)}}</textarea>
            </div> 
            <div class="note">
                <strong>Note:</strong> You can add CSS For Example: .body {margin:0px;}
            </div> 
        </div>
    </div>
</div>
</div>
</form>
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>
<div class="modal fade" id="new_note">
    <div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
			<p>
				Welcome to the Product Matrix Shopify App...!
			<br/><br/>Product Matrix app consists of several different features and requires code to be inserted in few files for which it is mandatory to follow <strong style="background-color:yellow;">configuration steps</strong>,else application will not work correctly.
			<br/><br/>Since configuration steps are more and due to many features it became bit complex, please contact support on "<a href="mailto:support@zestard.com">support@zestard.com</a>" or you can talk on live chat (bottom right - Available during IST office hours), if you need any assistance for <strong style="background-color:yellow;">initial setup</strong>.
			<br/><br/>Our application works based on javascript only, there may be a conflict between application code and your theme (other installed applications or conflict with other scripts).Please contact on support e-mail (<a href="mailto:support@zestard.com">support@zestard.com</a>) before you try for multiple time <strong style="background-color:yellow;">install-uninstall</strong> process. 
			<br/>
			</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong style="margin-right:30px">Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#dont_show_again").change(function(){
		var checked = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";
		if (!checked)
		{
			$.ajax({
				url: 'update-modal-status',
				data: {shop_name: shop_name},
				async: false,				
				success: function (result)
				{

				}
			});
		$('#new_note').modal('toggle');
		}
	});
});
var new_install = "{{ $new_install }}";	
if(new_install == "Y")
{
	$('#new_note').modal('show');
}
</script>
@endsection

@section('scripts')
<script>
    $(".screenshot").click(function () {
        $(".modal-content img").attr("src", $(this).attr("data-src"));
    });

    if ($("#show_stock").prop('checked') == true)
    {
        $('#quantity_position').prop('disabled', false);
    } else
    {
        $('#quantity_position').prop('disabled', 'disabled');
    }


    if ($("#show_price").prop('checked') == true)
    {
        $('#price_position').prop('disabled', false);
    } else
    {
        $('#price_position').prop('disabled', 'disabled');
    }

    $('#show_stock').click(function () {
        if ($(this).is(':checked')) {
            $('#quantity_position').prop('disabled', false);
        } else {
            $('#quantity_position').prop('disabled', 'disabled');
        }
    });

    $('#show_price').click(function () {
        if($(this).is(':checked')) {
            $('#price_position').prop('disabled', false);
        } else {
            $('#price_position').prop('disabled', 'disabled');
        }
    });
</script>
@endsection