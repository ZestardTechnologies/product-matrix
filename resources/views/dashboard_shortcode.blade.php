<div class="col-md-12 formcolor sticky_formcolor">   
    <div class="shortcode_heading col-sm-12 ">
        <h2 class="slide_down sub-heading subleft col-md-12"><i class="fa fa-chevron-up"></i> Shortcode & Where to paste Shortcode?</h2>
        <div class="col-sm-12" id="shortcode_info">    
            <img class="close_box" src="{{ asset('image/cancel.png') }}" />            
            <ul class="shortcode-note">
                <li>
                    <div>Copy and paste below shortcodes in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code.png') }}" href="javascript:;"><b>Example</b></a>
					</div>
                </li>
                <li>If your theme is section theme, or product template file is not available in your theme, then paste it in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_001.png') }}" href="javascript:;"><b>Example</b></a>
                </li>	                
                <li>
                    <div class="copystyle_wrapper">
                        <textarea id="script_code" rows="1" class="form-control short-code"  readonly=""><?php echo "<div class='zestard-productmatrix' store_id= '" . $encrypted_store_id . "' id='{{product.id}}'></div>"; ?>
                        </textarea>
                        <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_shortcode()"><i class="fa fa-check"></i> Copy</btn>
                        <label class="copy_message">
                        </label>
                    </div>
                    <div class="alert alert-success alert-dismissable show_copy_message"><a href="#" class="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>		
                </li>
                <li>
                    <div class="copystyle_wrapper">
                        <textarea id="product_page_code" rows="1" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix' %}"; ?></textarea>
                        <btn id="copy_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_product_shortcode()"><i class="fa fa-check"></i> Copy</btn>
                        <label class="copy_message">
                        </label>
                    </div>
                    <div class="alert alert-success alert-dismissable show_product_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>							
				</li>                                
				<li>
                    Then in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a>, add class "zestard-productmatrix" in the form, see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/shortcode_product_2.png') }}" href="javascript:;"><b>Example</b></a>. If your form tag is having format like "{% form 'product', product, class:form_classes %}", Then add class as shown in this <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/form_class_product_matrix.png') }}" href="javascript:;"><b>Example</b></a>.
                </li>                
                <li>If your theme is section theme, or product template file is not available in your theme, then paste it in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_002.png') }}" href="javascript:;"><b>Example</b></a>
                </li>                                				
                <li>				
                    <div class="copystyle_wrapper">
					
                        <textarea id="productmatrix_custom_class" rows="1" class="form-control short-code"  readonly=""><?php echo "zestard-product-matrix"; ?></textarea>
                        <btn id="copy_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="add_productmatrix_custom_class()"><i class="fa fa-check"></i> Copy</btn>
                        <label class="copy_message">
                        </label>
                    </div>
                    <div class="alert alert-success alert-dismissable show_productmatrix_custom_class_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>	
                </li>
                
                <li>
                    Copy and paste below shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code_cart.png') }}" href="javascript:;"><b>Example</b></a>
                </li>
                
                <li>
                 If your theme does not have cart-template.liquid file then paste the short-code in <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_shortcode_001.png') }}" href="javascript:;"><b>Example</b></a>
                </li>
                
                
                <li>
                    <div class="copystyle_wrapper">
                        <textarea id="cart_page_code" rows="1" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix-cart' %}"; ?>
                        </textarea>
                        <btn id="copy_cart_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_cart_shortcode()"><i class="fa fa-check"></i> Copy</btn>
                        <label class="copy_message">
                        </label>
                    </div>
			
                    <div class="alert alert-success alert-dismissable show_cart_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>							
                </li>
                
                <li>Copy and paste below shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page </b></a> inside <?php echo "&lt;div class='cart__qty'&gt; &lt;/div&gt;." ?> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/error_code.png') }}" href="javascript:;"><b>Example</b></a>
                </li>
            
               
                 <li>
                 If your theme does not have cart-template.liquid file then paste the short-code in <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_shortcode_002.png') }}" href="javascript:;"><b>Example</b></a>
                </li>
                
              
                <li>
                    <div class="copystyle_wrapper">
                        <textarea id="error_code" rows="1" class="form-control short-code"  readonly=""><?php echo '<p style="color:red" class="{{ item.product.id }}"></p>'; ?>
                        </textarea>
                        <btn id="copy_error_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_error_shortcode()"><i class="fa fa-check"></i> Copy</btn>
                        <label class="copy_message">
                        </label>
                    </div>
                    <div class="alert alert-success alert-dismissable show_error_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>		
                </li>
            </ul>
            <ul class="shortcode-note">
                <li>
                    If your cart page is not opening as url https://your-store-name/cart, Then Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right
                </li>
                
            </ul>
            
            
        </div>
    </div>            
</div>