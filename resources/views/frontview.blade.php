<style>
    ul li{
        display: block;
        float: left;
        width: 33.33%;
        margin-bottom: 10px;
        height : 80px;
    }
</style>

<div id="productmatrix-matrix">
    <ul>
        <li>Size</li>
        <li>Add Quantity</li>
        <li>Total prize</li>
    </ul>
    
    <ul>
        <li>O/S</li>
        <li><input type="number" min="0" value="0" name="quantity" class="quantity" data-price="118.00" data-variant_id="8153514541099" data-column="O/S">
            118.00<div class="inventory_quantity"> 1</div></li>
        <input type="hidden" class="set_price" value="0">
        <li data-column="total_price" class="total_intermediate_price">0</li>
    </ul>
    
    <ul>
        <li>O/SS</li>
        <li><input type="number" min="0" value="0" name="quantity" class="quantity" data-price="118.00" data-variant_id="11390599266347" 
                   data-column="O/SS">118.00<div class="inventory_quantity"> 1</div></li>
        <input type="hidden" class="set_price" value="0">
        <li data-column="total_price" class="total_intermediate_price">0</li>
    </ul>
</div>