@yield('header')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Product Matrix</title>
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> 
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('css/jquery-ui.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
        <script src="{{ url('js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ url('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <!-- jquery datatable CSS -->
        <link rel="stylesheet" href="{{ asset('css/datatable/dataTables.bootstrap.min.css') }}"> 
        <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>

        <!-- toastr-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <!-- spectrun datepicker -->
        <link rel="stylesheet" href="{{ asset('css/spectrum.css') }}">
        <script src="{{ asset('js/spectrum.js') }}"></script>

        <script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>        
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>

        <!-- shopify Script for fast load -->
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>

        <script>
			ShopifyApp.init({
				apiKey: '7b3c0295189e36cceea3e5e47b5aa29b',
				shopOrigin: '<?php echo "https://" . session('shop') ?>'
			});

			ShopifyApp.ready(function () {
				ShopifyApp.Bar.initialize({
					icon: "",
					title: '',
					buttons: {}
				});
			});
        </script>

        <style>
            .overlay {
                position: fixed;
                display: none;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(0, 0, 0, 0.5);
                z-index: 2;
            }
        </style>
    </head>
    <body>
        <div class="overlay"></div>
        @yield('content')
        <div class="modal fade" id="new_note">
            <div class="modal-dialog">          
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><b>Note</b></h4>
                    </div>
                    <div class="modal-body">
                        <p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store, before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
                    </div>        
                    <div class="modal-footer">			
                        <div class="datepicker_validate" id="modal_div">
                            <div>
                                <strong>Show me this again</strong>
                                <span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
                                    <label for="dont_show_again"></label></span>
                            </div>      
                        </div>      
                    </div>      
                </div>
            </div>
        </div>
        @yield('scripts')
        <script type="text/javascript">
            @if (isset($new_install))
            {
                var new_install = "{{ $new_install }}";
                if (new_install == "Y")
                {
                    $('#new_note').modal('show');
                }
            }
            @endif           
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
            }

                function copy_shortcode()
                {
                    var copyText = document.getElementById("script_code");
                    /* Select the text field */
                    copyText.select();
                    /* Copy the text inside the text field */
                    document.execCommand("Copy");
                    toastr.success("Shortcode copied!");
                }
                function copy_product_shortcode()
                {
                    var copyText = document.getElementById("product_page_code");
                    copyText.select();
                    document.execCommand("Copy");
                    //jQuery(".show_product_copy_message").show();
                    toastr.success("Shortcode copied!");
                }
                function copy_cart_shortcode()
                {
                    var copyText = document.getElementById("cart_page_code");
                    copyText.select();
                    document.execCommand("Copy");
                    //jQuery(".show_cart_copy_message").show();
                    toastr.success("Shortcode copied!");
                }
                function copy_error_shortcode()
                {
                    var copyText = document.getElementById("error_code");
                    copyText.select();
                    document.execCommand("Copy");
                    //jQuery(".show_error_message").show();
                    toastr.success("Shortcode copied!");
                }

                function add_productmatrix_custom_class()
                {
                    var copyText = document.getElementById("productmatrix_custom_class");
                    copyText.select();
                    document.execCommand("Copy");
                    //jQuery(".show_productmatrix_custom_class_copy_message").show();
                    toastr.success("Shortcode copied!");
                }
                function copyToClipboard()
                {
                    var copyText = document.getElementById("jquery_code");
                    copyText.select();
                    document.execCommand("Copy");
                }
                function hide_default_price()
                {
                    var copyText = document.getElementById("hide_default_price");
                    copyText.select();
                    document.execCommand("Copy");
                    jQuery(".hide_default_price").show();
                }

                function copy_script_js()
                {
                    var copyText = document.getElementById("script_code_js");
                    copyText.select();
                    document.execCommand("Copy");
                    //jQuery(".show_error_message").show();
                    toastr.success("Shortcode copied!");
                }
        </script>
        <script>
                $(document).ready(function () {
                    $(".shortcode-note .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".hide_default_price_div .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".show_copy_message .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".show_product_copy_message .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".show_productmatrix_custom_class_copy_message .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".show_cart_copy_message .close").click(function () {
                        $(this).parent().hide();
                    });

                    $(".show_error_message .close").click(function () {
                        $(this).parent().hide();
                    });
                });
        </script>

        <script type="text/javascript">
                jQuery(function () {
                    jQuery('.screenshot').on('click', function () {
                        jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
                        jQuery('#imagemodal').modal('show');
                    });

                });
        </script>

        <script type="text/javascript">
                $(document).ready(function () {
                    $(document).ajaxStart(function () {
                        startloader(1);
                    });
                    $(document).ajaxComplete(function () {
                        startloader(0);
                    });


                    function startloader(process) {
                        if (process == 1) {
                            $(".overlay").css({
                                'display': 'block',
                                'background-image': 'url({{ asset("image/loader.gif") }})',
                                'background-repeat': 'no-repeat',
                                'background-attachment': 'fixed',
                                'background-position': 'center'
                            });
                        } else {
                            $(".overlay").css({
                                'display': 'none',
                                'background-image': 'none',
                            });
                        }
                    }
                });
        </script>

        <script>
                @if (Session::has('notification'))

                var type = "{{ Session::get('notification.alert-type', 'info') }}";
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                switch (type) {
                    case 'info':
                        toastr.info("{{ Session::get('notification.message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('notification.message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('notification.message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('notification.message') }}");
                        break;
                    case 'options':
                        toastr.warning("{{ Session::get('notification.message') }}");
                        break;

                }
                @endif
        </script>

        <script type="text/javascript">
                        jQuery(".showInputLeftHeader").spectrum({
                preferredFormat: "hex",
                        showInput: true,
                showAlpha: true,
                        palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputTopHeader").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputLeftText").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputTopText").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputBorder").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputQuantity").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });

            jQuery(".showInputPrice").spectrum({
                preferredFormat: "hex",
                showInput: true,
                showAlpha: true,
                palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
            });
        </script>

        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>

        <!--End of Tawk.to Script-->
        <script type = "text/javascript">
            jQuery(function () {
                $('#price_position').change(function () {
                    if ($('#price_position').val() == $('#quantity_position').val())
                    {
                        $('#price_relative_to_quantity').html('<select class="form-control" name ="price_relative_to_quantity"><option value="before_quantity">Before Quantity</option><option value="after_quantity">After Quantity</option></select>');
                    } else
                    {
                        $('#price_relative_to_quantity').html("");
                    }
                });
                $('#quantity_position').change(function () {
                    if ($('#price_position').val() == $('#quantity_position').val())
                    {
                        $('#price_relative_to_quantity').html('<select class="form-control" name ="price_relative_to_quantity"><option value="before_quantity">Before Quantity</option><option value="after_quantity">After Quantity</option></select>');
                    } else
                    {
                        $('#price_relative_to_quantity').html("");
                    }
                });
            });
        </script>
    </body>
</html>
