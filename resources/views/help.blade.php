@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
                secondary: [
                    {
                        label: 'General Settings',
                        href: '{{ url("/dashboard") }}',
                        loading: true
                    },
                    {
                        label: 'Product Settings',
                        href: '{{ url('products') }}',
                        loading: true
                    },
                     {
                        label: 'Shortcodes',
                        href: '{{ url('shortcodes') }}',
                        loading: true
                    }
                ]
            }
        });
    });
</script>

<?php
$store_name = session('shop');
?>

<?php
if (Session::has('shop')) {
    $url_product_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} else {
    $url_product_template = "#";
    $url_product_page = "#";
    $url_cart_template = "#";
    $url_cart_page = "#";
}
?>

<div class="container formcolor formcolor_help">
    <div class=" row">
        <div class="help_page">
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class ="row">
                <div class =col-md-12 col-sm-12 col-xs-12">

                     <div class ="container">
                        <h2 class="dd-help">Please refer the below screencast if you need help configuring the app.</h2>
                        <center>
                            <video  controls style = "height="100%"; width : 100%;" >
                                    <source src="{{ asset('screencast/product_matrix_screencast.mp4') }}" type="video/mp4">
                                Your browser does not support the HTML 5 video tag. Please try opening in another Browser.
                            </video>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        @include('help.how_to_setup',array('url_product_template'=>$url_product_template,'url_product_page'=>$url_product_page,'url_cart_template'=>$url_cart_template,'url_cart_page'=>$url_cart_page))
        @include('help.how_to_configure')        
        @include('help.faq')
        @include('help.uninstall')
    </div>

    <div class="version_update_section">

        <div class="col-md-6" style="padding-right: 0;">

            <div class="feature_box">

                <h3 class="dd-help">Version Updates <span class="verison_no">1.0</span></h3>

                <div class="version_block">
                    <!--div class="col-md-12">

                        <div class="col-md-3 version_date">

                            <p><i class="glyphicon glyphicon-heart"></i></p>

                            <strong>22 Jan, 2018</strong>

                            <a href="#"><b>Update</b></a>

                        </div>

                        <div class="col-md-8 version_details">

                            <strong>Version 2.0</strong>

                            <ul>

                                <li>Add Delivery Date & Time information under customer order Email</li>

                                <li>Auto Select for Next Available Delivery Date</li>

                                <li>Auto Tag Delivery Details to all the Orders</li>

                                <li>Manage Cut Off Time for Each Individual Weekday</li>

                                <li>Limit Number of Order Delivery during the given time slot for any day </li>

                            </ul>

                        </div>

                    </div-->
                    <div class="col-md-12">

                        <div class="col-md-3 version_date">
                            <p><i class="glyphicon glyphicon-globe"></i></p>
                            <strong>10 April, 2018</strong>
                            <a href="#"><b>Release</b></a>
                        </div>

                        <div class="col-md-8 version_details version_details_2">

                            <strong>Version 1.0</strong>

                            <ul>

                                <li>Add multiple product variants to cart at once with multiple quantities</li>

                                <li>Configure option showed in row and column of product matrix</li>

                                <li>stock quantity management</li>

                                <li>Currently available stock can be displayed on Frontend</li>

                                <li>Allow out of stock products to order</li>

                                <li>Minimum quantity required for product to order can be set for each product</li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>
        </div>

        <div class="col-md-6">

            <div class="feature_box">

                <h3 class="dd-help">Upcoming Features</h3>

                <div class="feature_block">
                    <div>   

                        <span class="checkboxFive">

                            <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">

                            <label for="checkbox5"></label>

                        </span> 

                        <strong>Upsell - Allows to add discount when bulk quantities are added</strong>  

                    </div>


                </div>

                <div>

                    <p class="feature_text">

                        New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 

                    </p>

                </div>

            </div>

        </div>
    </div>
</div>

<!--Version updates-->
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>

<script>
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        // Toggle plus minus icon on show hide of collapse element

        $(".collapse").on('show.bs.collapse', function () {

            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {

            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
@endsection

@section('scripts')
<script>
    $(".screenshot").click(function () {
        $(".modal-content img").attr("src", $(this).attr("data-src"));
        $(".modal-title").html($(this).data("title"));
    });
</script>
@endsection