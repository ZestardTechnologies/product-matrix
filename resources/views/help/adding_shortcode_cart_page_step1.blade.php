<div class ="row">
    <div class ="col-sm-12">
        <div class="copystyle_wrapper">
            <textarea id="cart_page_code" rows="1" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix-cart' %}"; ?></textarea>
            <btn id="copy_cart_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_cart_shortcode()"><i class="fa fa-check"></i> Copy</btn>
            <label class="copy_message">
            </label>
        </div> 	
    </div>
</div>

<div class = "product_matrix_screenshot_box">

<div class ="row">
      <div class ="col-sm-6">
            Copy and paste above shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page.</b></a> 
      </div>
    <div class ="col-sm-6">
        If your theme does not have cart-template.liquid file, then paste the short-code in <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page.</b></a>
    </div>
</div>


<div class ="row">
    <div class ="col-sm-6"> 
        <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code_cart.png') }}" data-title="Add shortcode in Cart Template Page" href="javascript:;">
            <img class ="img-responsive" src ="{{ asset('image/short_code_cart.png') }}"/>                                
        </a>
    </div>

    <div class ="col-sm-6">
        <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot"  data-src="{{ asset('image/cart_template_shortcode_001.png') }}" data-title="Add shortcode in Cart Page" href="javascript:;">
            <img class ="img-responsive" src ="{{ asset('image/cart_template_shortcode_001.png') }}"/>                                
        </a>
    </div>
</div>
</div>