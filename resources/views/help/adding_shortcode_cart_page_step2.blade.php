<div class ="row">
    <div class ="col-sm-12">
        <div class="copystyle_wrapper">
            <textarea id="error_code" rows="1" class="form-control short-code"  readonly=""><?php echo '<p style="color:red" class="{{ item.product.id }}"></p>'; ?>
            </textarea>
            <btn id="copy_error_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_error_shortcode()"><i class="fa fa-check"></i> Copy</btn>
            <label class="copy_message">
            </label>
        </div>

    </div>
</div>

<div class ="product_matrix_screenshot_box">

    <div class ="row">
        <div class ="col-sm-6">Copy and paste above shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page.</b></a> inside <?php echo "&lt;div class='cart__qty'&gt; &lt;/div&gt;." ?></div>
        <div class ="col-sm-6">If you can't find cart template page, Then paste the short code in  
            <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page.</b></a></div>
    </div>


    <div class ="row">
        <div class ="col-sm-6">
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/error_code.png') }}" data-title="Add shortcode in Cart Template Page"  href="javascript:;">
                <img class ="img-responsive" src ="{{ asset('image/error_code.png') }}"/>                                
            </a>
        </div>
        <div class ="col-sm-6">
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_shortcode_002.png') }}" data-title="Add shortcode in Cart Page" href="javascript:;">
                <img class ="img-responsive" src ="{{ asset('image/cart_template_shortcode_002.png') }}"/>                                
            </a>
        </div>
    </div>
</div>



<div class ="row">
    <div class ="col-sm-12">
        <ul class="ul-help">
            <li>
                If your cart page is not opening as url https://your-store-name/cart, Then Please contact support team (<a href="mailto:support@zestard.com" target ="_blank">support@zestard.com</a>) or live chat at bottom right.
            </li>
        </ul>
    </div>
</div>