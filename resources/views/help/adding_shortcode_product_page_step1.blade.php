<div class = "row">
    <div class = "col-sm-12">
        <div class="copystyle_wrapper">
            <textarea id="script_code" rows="2" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix' %}"; ?><?php //echo "&#13;&#10;<div class='zestard-productmatrix' store_id= " . $encrypted_store_id . " id='{{product.id}}'></div>"; ?></textarea>
            <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".short_code" style="display: block;" onclick="copy_shortcode()"><i class="fa fa-check"></i> Copy</btn>

            <label class="copy_message">
            </label>
        </div>
    </div>
</div>
<div class ="product_matrix_screenshot_box">
	<div class ="row">
		<div class = "col-sm-6">Copy and paste above shortcodes in <a href="<?php echo $url_product_template; ?>" target="_blank"><b>product template page.</b></a></div>
		<div class = "col-sm-6">If your theme is section theme, or product template file is not available in your theme, then paste above shortcodes in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page.</b></a></div>
	</div>
	<div class ="row">
		<div class = "col-sm-6">
			<a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code.png') }}" 
			   data-title="Paste the shortcode in Product Template Page" href="javascript:;">
				<img class ="img-responsive" src ="{{ asset('image/short_code.png') }}"/>
			</a>
		</div>
		<div class = "col-sm-6">
			<a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_001.png') }}" data-title="Paste the shortcode in Product Page" href="javascript:;" >
				<img class ="img-responsive" src ="{{ asset('image/product_template_shortcode_001.png') }}"/>
			</a>
		</div>
	</div>
</div>