<div class = "row">
    <div class = "col-sm-12">

        <div class="copystyle_wrapper">
            <textarea id="productmatrix_custom_class" rows="1" class="form-control short-code"  readonly=""><?php echo "zestard-product-matrix"; ?></textarea>
            <btn id="copy_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="add_productmatrix_custom_class()"><i class="fa fa-check"></i> Copy</btn>
            <label class="copy_message">
            </label>
        </div>	
    </div>
</div>


<div class ="product_matrix_screenshot_box">
    <div class ="row">
        <div class = "col-sm-4">
            In <a href="<?php echo $url_product_template; ?>" target="_blank"><b>product template page</b></a>, add class "zestard-product-matrix" in the form.
        </div>
        <div class = "col-sm-4">
            If your theme is section theme, or product template file is not available in your theme, then paste the class in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page.</b></a>
        </div>
        <div class = "col-sm-4">
            If your form tag is having format like "{% form 'product', product, class:form_classes %}", Then add class as shown in the screenshot below in <a href="<?php echo $url_product_template; ?>" target="_blank"><b>product template page.</b></a>.
        </div>
    </div>


    <div class ="row">
        <div class = "col-sm-4">
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/shortcode_product_2.png') }}" data-title="Add class in Product Template Page" href="javascript:;">
                <img class ="img-responsive" src ="{{ asset('image/shortcode_product_2.png') }}"/>
            </a>
        </div>
        <div class = "col-sm-4">
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_002.png') }}" data-title="Add class in Product Template Page" href="javascript:;">
                <img class ="img-responsive" src ="{{ asset('image/product_template_shortcode_002.png') }}"/>
            </a>	
        </div>
        <div class = "col-sm-4">
            <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/form_class_product_matrix.png') }}" data-title="Add class in Product Template Page for updated shopify themes" href="javascript:;">
                <img class ="img-responsive" src ="{{ asset('image/form_class_product_matrix.png') }}"/>
            </a>
        </div>
    </div>
</div>