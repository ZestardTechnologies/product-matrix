<div class="col-md-12 col-sm-12 col-xs-12">
    <h2 class="dd-help">FAQ</h2> 
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion3" href="#collapse8"> 
                            <strong><span class="">Product matrix is not displayed in store on product page, what should I do?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>
                            Make sure app is enabled from general settings. If the productmatrix is still not showing, then make sure at least one option is added for that product.
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion3" href="#collapse9"> 
                            <strong><span class="">I have enabled "Allow out of stock" from general settings, still the cart is empty when I try to add products more than stock quantity, why is that so?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>
                            You need to enable "Allow customers to purchase this product when it's out of stock" option from shopify backend. See <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/out_of_stock.png') }}" href="javascript:;"><b>Example.</b></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion3" href="#collapse10"> 
                            <strong><span class="">How do I change the default designing of productmatrix to match with my theme?</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>
                            You can make use of existing design settings in dashboard/general settings, or you can also add "additional css" from  general settings.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>