<div class="col-md-12 col-sm-12 col-xs-12">
    <h1 class="dd-help">
        How to Configure?
    </h1>
    <div class="success-copied"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion2">
  
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion2" href="#collapse5"> 
                            <strong><span class="">Product Configuration Options</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>			
                        </p>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('help.product_configuration')
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion2" href="#collapse6"> 
                            <strong><span class="">Language Settings</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  										
                        </p>
                    </h4>
                </div>

                <div id="collapse6" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('help.language_settings')
                    </div>
                </div>
            </div>
            
            
            
                  <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion2" href="#collapse7"> 
                            <strong><span class="">Design Settings</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>
                        </p>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('help.minimum_quantity_validation')
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>