<div class="col-md-12 col-sm-12 col-xs-12">
    <h1 class="dd-help">
        How to Setup?
    </h1>
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                            <strong><span class="">#1 - For displaying product matrix on product page</span>
                                <span class="fa fa-chevron-down pull-right"></span>
                            </strong>
                        </p>
                    </h4>
                </div>

                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <!-- adding shortcode on product page -->
                        @include('help.adding_shortcode_product_page_step1')
                        <!-- stop adding shortcode on product page -->
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                            <strong>
                                <span class="">#2- For hiding default options and variants dropdown box</span>
                                <span class="fa fa-chevron-down pull-right"></span>
                            </strong>		
                        </p>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <!-- adding shortcode on cart page -->         
                        @include('help.adding_shortcode_cart_page_step2')
                        <!-- stop adding shortcode on cart page-->
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                            <strong>
                                <span class="">#3 - For checking minimum quantity on cart page(Optional but recommended when minimum quantity validation is set)</span>
                                <span class="fa fa-chevron-down pull-right"></span>
                            </strong>		
                        </p>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <!-- adding shortcode on cart page -->         
                        @include('help.adding_shortcode_cart_page_step1')
                        <!-- stop adding shortcode on cart page-->
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                            <strong>
                                <span class="">#4 - For displaying error message if minimum quantity criteria is not satisfied(Optional but recommended when minimum quantity validation is set)</span>
                                <span class="fa fa-chevron-down pull-right"></span>
                            </strong>		
                        </p>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <!-- adding shortcode on cart page -->         
                        @include('help.adding_shortcode_cart_page_step2')
                        <!-- stop adding shortcode on cart page-->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>