                    <div class ="row">
                            <div class ="col-sm-8">
                                <div class ="settings-heading">
                                    Language options for the current version are as follows :
                                </div>
                                <ol>
                                    <li>
                                        <b>Row Total Label</b>
                                        <br/>
                                        Label for showing Row Total values.
                                    </li>
                                    <li>
                                        <b>Column Total Label</b>
                                        <br/>
                                        Label for showing Column Total values.
                                    </li>
                                    <li>
                                        <b>Quantity Label</b>
                                        <br/>
                                        Label to show before value for each variant stock quantity is displayed.
                                    </li>
                                    <li>
                                        <b>Price Label</b>
                                        <br/>
                                        Label to show before value for each variant price is displayed.
                                    </li>
                                    <li>
                                        <b>Add to Cart Button Text</b>
                                        <br/>
                                        Text value for add to cart button.
                                    </li>
                                                                        <li>
                                        <b>Minimum Quantity Error Message</b>
                                        <br/>
                                        This message will be displayed when customer clicks on add to cart & total quantity of product entered by customer is less than the required quantity of product.
                                    </li>
                                    <li>
                                        <b>Minimum Quantity Error Message on Cart Page</b>
                                        <br/>
                                        This message will be displayed when the quantity in the cart is less than the minimum quantity required.
                                    </li>
                                    <li>
                                        <b>Minimum Quantity Message</b>
                                        <br/>
                                        Message to be displayed in bottom of product matrix for minimum quantity to be required to go to cart page.
                                    </li>
                                </ol>
                            </div>
                            <div class ="col-sm-4">
                                <div class ="row">
                                    <div class ="container">
                                        <a class="info_css screenshot" href="{{ asset('image/frontend_screenshots_final/frontend_screenshot_003.png') }}" target="_blank">
                                            <img class ="img-responsive" src ="{{ asset('image/frontend_screenshots_final/frontend_screenshot_003.png') }}"/>
                                        </a>
                                        <br/>

                                        <a class="info_css screenshot" href="{{ asset('image/dashboard_screenshots_final/dashboard_003.png') }}" target="_blank">
                                            <img class ="img-responsive" src ="{{ asset('image/dashboard_screenshots_final/dashboard_003.png') }}"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>