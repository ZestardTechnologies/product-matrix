<div class ="row">
    <div class ="col-sm-8">
        <div class ="settings-heading">
            Design options for the current version are as follows :
        </div>
        <b><h4>Design Options</h4></b>
        <ol>
            <li>
                <b>Price Font size</b>
                <br/>
                Font size for price displayed in product matrix in the store can be changed using this option, if "Show Price?" option is enabled.
            </li>

            <li>
                <b>Quantity Font size</b>
                <br/>
                Font size of quantity displayed in product matrix in the store can be changed using this option, if "Show Stock Qty?" option is enabled.
            </li>

            <li>
                <b>Row Font Size</b>
                <br/>
                Font size of row option can be changed using this option.
            </li>

            <li>
                <b>Column Font Size</b>
                <br/>
                Font size of column option can be changed using this option.
            </li>

            <li>
                <b>Minimum Quantity Font size</b>
                <br/>
                Font size for minimum quantity message can be changed using this option.
            </li>

            <li>
                <b>Show Product Matrix Box Border?</b>
                <br/>
                Whether to show Border around product matrix or not, displayed in store.
            </li>
            <li>
                <b>Show Cell Border?</b>
                <br/>
                Whether to show Border around each cell of product matrix, displayed in store.
            </li>
            <li>
                <b>Product Matrix Box Border Color</b>
                <br/>
                Main Border Color around Product Matrix can be changed using this option.
            </li>
            <li>
                <b>Product Matrix Cell Color</b>
                <br/>
                Border Color around each cell pf product matrix displayed in frontend can be changed using this option.
            </li>
            <li>
                <b>Row Option Font Color</b>
                <br/>
                Font color for row options can be changed using this option.
            </li>
            <li>
                <b>Column Option Font Color</b>
                <br/>
                Font color for column options can be changed using this option.
            </li>
            <li>
                <b>Row Option Background Color</b>
                <br/>
                Background color for row options can be changed using this option.
            </li>
            <li>
                <b>Column Option Background Color</b>
                <br/>
                Background color for column options can be changed using this option.
            </li>
            <li>
                <b>Price Color</b> 
                <br/>
                Font color for price displayed in product matrix, in the store can be changed using this option, if "Show Price?" option is enabled.
            </li>
            <li>
                <b>Quantity Color</b>
                <br/>
                Font color for quantity displayed in product matrix, in the store can be changed using this option, if "Show Stock Quantity?" option is enabled.
            </li>
            <li>
                <b>Quantity Message Font Color</b>
                <br/>
                Font Color of quantity message displayed at the bottom of productmatrix can be changed using this option.
            </li>
            <li>
                <b>Additional CSS</b>
                <br/>
                If you want to add any other designing apart from above mentioned options, you can directly add CSS for that using this option.
            </li>
        </ol>
    </div>
    <div class ="col-sm-4">
        <div class ="row">
            <div class ="container">
                <a class="info_css screenshot" href="{{ asset('image/frontend_screenshots_final/frontend_screenshot_001.png') }}" target ="_blank">
                    <img class ="img-responsive" src ="{{ asset('image/frontend_screenshots_final/frontend_screenshot_001.png') }}"/>
                </a>
                <br/>

                <a class="info_css screenshot" href="{{ asset('image/dashboard_screenshots_final/dashboard_001.png') }}" target ="_blank">
                    <img class ="img-responsive" src ="{{ asset('image/dashboard_screenshots_final/dashboard_001.png') }}"/>
                </a>
            </div>
        </div>
    </div>
</div>