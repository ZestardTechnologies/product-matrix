<div class ="row">
    <div class ="col-sm-8">
        <div class ="settings-heading">
            Product Configuration options for the current version are as follows:
        </div>
        <b><h4>General Settings</h4></b>
        <ol>
            <li>
                <b>App Active?</b>
                <br/>
                Enable/Disable the app, once this option is disabled, app will not be displayed in the store on product page.
            </li>
            <li>
                <b>Show Stock Qty?</b>
                <br/>
                Display stock quantity or not for product matrix displayed in the store.
            </li>
            <li>
                <b>Quantity Position</b>
                <br/>
                Where to display stock quantity for product matrix displayed in the store.
            </li>
            <li>
                <b>Show Price?</b>
                <br/>
                Display price or not for product matrix displayed in the store.
            </li>
            <li>
                <b>Price Position</b>
                <br/>
                Where to display price for product matrix displayed in the store.
            </li>
            <li>
                <b>Show Total</b>
                <br/>
                Show Total or not, if total is shown, whether to show it column wise or row wise for product matrix displayed in store.
            </li>

            <li>
                <b>Allow out of stock products to Order?</b>
                <br/>
                If "Allow customers to purchase this product when it's out of stock" setting is enabled from backend then this setting will allow out of stock products to order using the app.
            </li>
            <li>
                <b>Show Default Price</b>
                <br/>
                Whether to show default price of product or not, if this option is disabled, then show price option should be enabled to show price in product matrix in store.
            </li>
        </ol>

        <b><h4>Product Settings</h4></b>


        <ol>
            <li>
                <b>Row Option</b>
                <br/>
                Only shown if 2 or 3 options are added for the product, which determines what option to show in row of productmatrix displayed in store.
            </li>
            <li>
                <b>Show Stock Qty?</b>
                <br/>
                Only shown if 2 or 3 options are added for the product, which determines what option to show in column of productmatrix displayed in store.
            </li>
            <li>
                <b>Minimum Quantity</b>
                <br/>
                Minimum Quantity of the Product required for order, this option must be set product wise from Product Settings tab.
            </li>
            <li>
                <b>Disable Zestard ProductMatrix App for this product</b>
                <br/>
                Disable the app for a specific product. Default shopify store layout will be shown instead of Product matrix in store on product page when this option is enabled.
            </li>
        </ol>

    </div>
    <div class ="col-sm-4">
        <div class ="row">
            <div class ="container">
                <a class="info_css screenshot" href="{{ asset('image/frontend_screenshots_final/frontend_screenshot_002.png') }}" target="_blank">
                    <img class ="img-responsive" src ="{{ asset('image/frontend_screenshots_final/frontend_screenshot_002.png') }}"/>
                </a>
                <br/>

                <a class="info_css screenshot" href="{{ asset('image/dashboard_screenshots_final/dashboard_002.png') }}" target="_blank">
                    <img class ="img-responsive" src ="{{ asset('image/dashboard_screenshots_final/dashboard_002.png') }}"/>
                </a>
            </div>
        </div>
    </div>
</div>