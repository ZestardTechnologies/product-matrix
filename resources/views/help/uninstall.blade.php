<div class="col-md-12 col-sm-12 col-xs-12">
    <h2 class="dd-help">How to uninstall?</h2> 
    <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
        <div class="panel-group" id="accordion3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <p data-toggle="collapse" data-parent="#accordion3" href="#collapse11"> 
                            <strong><span class="">Uninstall instructions</span>
                                <span class="fa fa-chevron-down pull-right"></span></strong>  
                        </p>
                    </h4>
                </div>
                <div id="collapse11" class="panel-collapse collapse">       
                    <div class = "panel-body">
                      @include('help.uninstall_procedure')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>