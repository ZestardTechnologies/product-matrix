<?php
if(Session::has('shop')) 
{
    $url_product_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} 
else 
{
    $url_product_template = "#";
    $url_product_page = "#";
    $url_cart_template = "#";
    $url_cart_page = "#";
}
?>
<div class = "product_matrix_screenshot_box">
	<div class = "container">
		<div class ="row">
			<div class ="col-sm-6">
				<ul class="ul-help">
					<li>To Remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
					<li>Click on the delete icon of Zestard Product Matrix App.</li>
					<strong>If Possible then Remove Shortcode where you have Pasted as shown in below steps.</strong>
					<li>						
						Remove shortcodes from <a href="<?php echo $url_product_template; ?>" target="_blank"><b>product template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_remove.png') }}" href="javascript:;"><b>Example</b></a>						
					</li>
					<li>
						If your theme is section theme, or product template file is not available in your theme, then remove shortcodes from <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_liquid_remove.png') }}" href="javascript:;"><b>Example</b></a>
					</li>
					<li>
						Then from <a href="<?php echo $url_product_template; ?>" target="_blank"><b>product template page</b></a>, remove class "zestard-productmatrix" in the form, see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_remove_class.png') }}" href="javascript:;"><b>Example</b></a>. 						
					</li>                
					<li>
						If your theme is section theme, or product template file is not available in your theme, then remove <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_liquid_remove_class.png') }}" href="javascript:;"><b>Example</b></a>
					</li>
					<li>                
						If your form tag is having format like "{% form 'product', product, class:form_classes %}", Then remove class as shown in this <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/form_class_remove.png') }}" href="javascript:;"><b>Example</b></a>.
					</li>                					
					<li>
						Remove shortcode from <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_remove.png') }}" href="javascript:;"><b>Example</b></a> and for removing error message shortcode see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_remove_error.png') }}" href="javascript:;"><b>Example</b></a>.
					</li>
					<li>
						Remove shortcode from <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_liquid_remove.png') }}" href="javascript:;"><b>Example</b></a> and for removing error message shortcode see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_liquid_remove_error.png') }}" href="javascript:;"><b>Example</b></a>.
					</li>
				</ul>
			</div>
			<div class ="col-sm-6">
				Check this screenshot for uninstalling the app.
				<br/>
					<a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-title="Uninstall Zestard Product Matrix" data-src="{{ asset('image/uninstall.png') }}"  href="javascript:;">
				<img class ="img-responsive" src ="{{ asset('image/uninstall.png') }}"/>                                
			</a>
			</div>
		</div>
	</div>
</div>