@extends('header')
@section('content')
<script type="text/javascript">
    ShopifyApp.ready(function(e){
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
                 secondary:[
					{
						label: 'General Settings',
						href : '{{ url("/dashboard") }}',
						loading: true
					},
					{
						label: 'Product Settings',
						href : '{{ url('products') }}',
						loading: true
					}
				]
            }
        });
    });
</script>
<?php
	$store_name = session('shop');
?>
<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="help_page">
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">General Instruction</h2> 
                <div class="success-copied"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">Notes for adding JQuery into Theme</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">                                   
                                    <p>Copy and paste the following code into head part of your <a href="<?php echo 'https://' . session('shop') . '/admin/themes/current/?key=layout/theme.liquid' ?>" target="_blank"><b>Theme.liquid</b></a> file.</p>                                    
                                    <div class="copystyle_wrapper col-md-9">
                                        <textarea rows="1" class="form-control script_code" id="jquery_code" disabled><?php echo "{{ '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js' | script_tag }}" ?></textarea>
                                        <!--btn value="Copy Shortcode" class="btn btn-info copycss_button tooltipped tooltipped-s copyMe" style="display: block;" onclick="copyToClipboard()"><i class="fa fa-check"></i> Copy</btn-->
										<btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copyToClipboard()"><i class="fa fa-check"></i> Copy</btn>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                        <strong><span class="">What is allow out of stock products to Order?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>It is the option for whether to allow out of stock products to order or not.</p>
                                </div>
                            </div>
                        </div>  
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                        <strong><span class="">What is show stock quantity?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                   <p>It is the option for whether to display the available stock quantity of product or not.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                        <strong><span class="">What is show price?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                   <p>It is the option for whether to display the price of product variant or not.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
                                        <strong><span class="">What is minimum quantity error message?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>This message will be displayed when customer clicks on add to cart & total quantity of product 
									entered by customer is less than the required quantity of product.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse6"> 
                                        <strong><span class="">What is minimum quantity error message on cart?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>							
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>This message will be displayed when customer clicks on add to cart & total quantity of product 
									entered by customer is less than the required quantity of product.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse11"> 
                                        <strong><span class="">What is product matrix border color?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>It is used to give some color to boundary of product matrix. <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_matrix_border.png') }}" href="javascript:;"><b>Example</b></a></p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse7"> 
                                        <strong><span class="">What is the meaning of column background color, column font color, column font size & row background color, row font color, row font size?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>It is used to set background color for column option header, font size and font color of column options(i.e size, Color, etc) and same for row. See <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/column_row_option.png') }}" href="javascript:;"><b>Example</b></a></p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse8"> 
                                        <strong><span class="">What is price label, price font size, price color, quantity label, quantity font size & quantity color?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse8" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>This message will be displayed when customer clicks on add to cart & total quantity of product 
									entered by customer is less than the required quantity of product.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse9"> 
                                        <strong><span class="">What is additional CSS?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse9" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>Additional CSS is for adding custom design and layout for product matrix.</p>
                                </div>
                            </div>
                        </div>
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse10"> 
                                        <strong><span class="">What is minimum quantity error message on cart?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  										
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse">
                                <div class="panel-body">
									<p>This message will be displayed when customer clicks on add to cart & total quantity of product 
									entered by customer is less than the required quantity of product.</p>
                                </div>
                            </div>
                        </div>						
                    </div>
                </div>
            </div>
        </div>

        <!--Version updates-->

        <div class="version_update_section">

            <div class="col-md-6" style="padding-right: 0;">

                <div class="feature_box">

                    <h3 class="dd-help">Version Updates <span class="verison_no">1.0</span></h3>

                    <div class="version_block">
                        <!--div class="col-md-12">

                            <div class="col-md-3 version_date">

                                <p><i class="glyphicon glyphicon-heart"></i></p>

                                <strong>22 Jan, 2018</strong>

                                <a href="#"><b>Update</b></a>

                            </div>

                            <div class="col-md-8 version_details">

                                <strong>Version 2.0</strong>

                                <ul>

                                    <li>Add Delivery Date & Time information under customer order Email</li>

                                    <li>Auto Select for Next Available Delivery Date</li>

                                    <li>Auto Tag Delivery Details to all the Orders</li>

                                    <li>Manage Cut Off Time for Each Individual Weekday</li>

                                    <li>Limit Number of Order Delivery during the given time slot for any day </li>

                                </ul>

                            </div>

                        </div-->
                        <div class="col-md-12">

                            <div class="col-md-3 version_date">
                                <p><i class="glyphicon glyphicon-globe"></i></p>
                                <strong>10 April, 2018</strong>
                                <a href="#"><b>Release</b></a>
                            </div>

                            <div class="col-md-8 version_details version_details_2">

                                <strong>Version 1.0</strong>

                                <ul>

                                    <li>Delivery Date & Time Selection</li>

                                    <li>Same Day & Next Day Delivery</li>

                                    <li>Blocking Specific Days and Dates</li>

                                    <li>Admin Order Manage & Export Based on Delivery Date</li>

                                    <li>Option for Cut Off Time & Delivery Time</li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-6">

                <div class="feature_box">

                    <h3 class="dd-help">Upcoming Features</h3>

                    <div class="feature_block">

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">

                                <label for="checkbox0"></label>

                            </span> 

                            <strong>Multiple Cutoff Time Option</strong>  

                        </div>

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">

                                <label for="checkbox4"></label>

                            </span> 

                            <strong>Multiple Delivery Time Option</strong>  

                        </div>

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">

                                <label for="checkbox5"></label>

                            </span> 

                            <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  

                        </div>

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">

                                <label for="checkbox1"></label>

                            </span> 

                            <strong>Auto Select for Next Available Delivery Date</strong>  

                        </div>

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">

                                <label for="checkbox2"></label>

                            </span> 

                            <strong>Order Export in Excel</strong>  

                        </div>

                        <div>   

                            <span class="checkboxFive">

                                <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">

                                <label for="checkbox3"></label>

                            </span> 

                            <strong>Filtering Orders by Delivery Date</strong>  

                        </div>                                        

                    </div>

                    <div>

                        <p class="feature_text">

                            New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 

                        </p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>
<script>
    $(document).ready(function () {
		$(".screenshot").click(function(){		
			$(".modal-content img").attr("src", $(this).attr("data-src"));
		});
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
		
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
@endsection