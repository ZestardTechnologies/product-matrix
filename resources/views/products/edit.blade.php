@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Product Settings',
            buttons: {
                primary: {
                    label: 'Save',
                    message: 'form_submit',
                    callback: function (event) {                        						
						if ($("#row_option").val() == "")
						{
							alert('Please Select Row Option');
							//$("#column_option").click();                    
							return false;
						}
						else if ($("#column_option").val() == "")
						{
							alert('Please Select Column Option');
							$("#column_option").click();                    
							return false;
						}
						else 
						{
							$("#product-settings-form").attr("data-shopify-app-submit","form_submit");
						}						
                    }
                },
                secondary: [                    
					{
                        label: 'General Settings',
                        href: '{{ url("/dashboard") }}',
                        loading: true
                    },
                    {
                        label: 'Product Settings',
                        href: '{{ url("/products") }}',
                        loading: true
                    },
					{
						label: 'Shortcodes',
						href : '{{ url("/shortcodes") }}',
						loading: true
					},
					{
                        label: 'Help',
                        href: '{{ url("/help") }}',
                        loading: true
                    }					
                ]
            }
        });
    });
	$(document).ready(function(){
		$("#min_qty_input").keypress(function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
			if(keycode == '13') 
			{				
				if ($("#row_option").val() == "")
				{
					alert('Please Select Row Option');
					$("#row_option").click();                    
					return false;
				}
				else if ($("#column_option").val() == "")
				{
					alert('Please Select Column Option');
					$("#column_option").click();                    
					return false;
				}	
			}
		});		
		function validations()
		{			
			if ($("#row_option").val() == "")
			{
				alert('Please Select Row Option');
				//$("#column_option").click();                    
				return false;
			}
			else if ($("#column_option").val() == "")
			{
				alert('Please Select Column Option');
				$("#column_option").click();                    
				return false;
			}
			else 
			{
				$("#product-settings-form").attr("data-shopify-app-submit","form_submit");
			}
		}
	});
</script>
<form name="product-settings" id="product-settings-form" data-shopify-app-submit="" action="{{ action('ProductController@update',$id) }}">
    <div class="formcolor"> 
        <ul class="nav nav-tabs dashboard_tabs">
            <li class="active">
                <a href = '{{ "https://".$shop."/admin/products/".$product_info->product->id }}' target = '_blank'>
                    {{ $product_info->product->title }}
                </a>
            </li>
        </ul>
        <input type="hidden" name="product_id" value="{{ $id }}">
        <input type ="hidden" name = "store_id" value = "{{ $store_id }}"/>
        @if(count($product_settings) > 0)
        <input type ="hidden" name = "update_product" value = "{{ $product_settings->id }}"/>
        @endif
 
        
        @if($variants_message != "")
        <div class="row formcolor_row container">
            <div class ="col-sm-9 alert alert-danger">
                {{ $variants_message }}
            </div>
        </div>
        @else
           <div class="row formcolor_row container">
            <div class ="col-sm-9 alert alert-danger">
                {{ "If Row option and column option are not selected, than they will be taken as follows, by default." }}
                <ul>
                    <li>Option position 1 : Row option</li>
                    <li>Option position 2 : Column Option</li>
                    <li>Option position 3 : Dropdown Option</li>
                </ul>
                To change the default options, use the "Select Row" and "Select Column" options below. 
            </div>
               <div class ="col-sm-3">
                   <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/edit_product_settings.png') }}" href="javascript:;">
                                    <img class="img-responsive" src="{{ asset('image/edit_product_settings.png') }}">
                                </a>
               </div>
        </div>
        <br/>
        <br/>
        @endif

        <div class="row formcolor_row container">
            @if($options_array_count >= 2)
            <div class="col-sm-3">
                <label for="row_option">Select Row</label>
                <select class="form-control" id="row_option" name="row_option">
                    <option value="" selected>-- Please Select --</option>           
                    @foreach($options_array as $id => $option)
                    <option value = "{{ $id }}" @if(old('row_option') == $id) selected @endif  @if(isset($product_settings->row_option))
                        @if($product_settings->row_option == $id)  selected @endif @endif>{{ $option['name'] }}</option>
                    @endforeach
                </select>
            </div>            
            <div class="col-sm-3">
                <label for="column_option">Select Column</label>
                <select class="form-control" id="column_option" name="column_option" @if(isset($product_settings->column_option))
                        @if($product_settings->column_option == $id)  selected @endif @else disabled @endif>                        
                        <option value="" selected>-- Please Select Column --</option>
                    @foreach($options_array as $id => $option)
                    <option value = "{{ $id }}" @if(old('column_option') == $id) selected @endif   @if(isset($product_settings->column_option))
                            @if($product_settings->column_option == $id)  selected @endif @endif>{{ $option['name'] }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="col-sm-3">
                <label for="min_qty_required">Minimum quantity required</label>
                <span class="onoff"><input type="checkbox" id="min_qty_required" name = "min_qty_required" @if(count($product_settings) > 0) @if($product_settings->min_qty_required == 1)  {{ "checked" }} @endif @endif><label for="min_qty_required"></label></span>
                <div id ="minimum_quantity" @if(!isset($product_settings->min_qty_required)) style ="display:none;" @endif >
                     <input type ="number" min="1" name="min_qty_value" id="min_qty_input" class="form-control" placeholder = "Enter Quantity"
                       @if(isset($product_settings->min_qty_value)) @if($product_settings->min_qty_value != '1') value = "{{ $product_settings->min_qty_value }}" @endif @endif/>
                </div> 
            </div>
            
               <div class="col-sm-3">
             <label for="disable_app_for_product">Disable App for this product</label>
              <span class="onoff"><input type="checkbox" id="disable_app_for_product" name = "disable_app_for_product" @if(count($product_settings) > 0) @if($product_settings->disable_app_for_product == 1)  {{ "checked" }} @endif @endif><label for="disable_app_for_product"></label></span>
             </div>
        </div>
        
    </div>
</form>

<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        
          $(".screenshot").click(function () {
        $(".modal-content img").attr("src", $(this).attr("data-src"));
    });
    
        /* 
         $("#column_option > option").each(function() {			
         $("#column_option option[value='" + this.value + "']").attr('disabled', false);
         }); 
         */
        /* 
         $("#row_option > option").each(function() {			
         $("#row_option option[value='" + this.value + "']").attr('disabled', false);
         }); 
         */
//    $('#row_option').change(function (e) {
//    $('#column_option option').not(':selected').removeAttr('disabled');
//    $('#row_option option').not(':selected').removeAttr('disabled');
//    $('#column_option').removeAttr('disabled');
//     $('#column_option').removeAttr('selected');
//    $("#column_option option[value='" + $(this).val() + "']").attr('disabled', true);
//    });
//    $('#column_option').change(function (e) {
//    $('#row_option option').not(':selected').removeAttr('disabled');
//    $('#column_option option').not(':selected').removeAttr('disabled');
//    $('#row_option').removeAttr('disabled');
//     $('#row_option').removeAttr('selected');
//    $("#row_option option[value='" + $(this).val() + "']").attr('disabled', true);
//    });

        $('#row_option').change(function (e) {
            /* $('#column_option option').not(':selected').prop('disabled', false);
             $('#row_option option').not(':selected').prop('disabled', false);
             $('#column_option').attr('disabled', false); */

            selected_column = $("#column_option option:selected").val();
            if ($(this).val() == selected_column)
            {
                /* $("#column_option").val("");	
                 $("#column_option option:selected").prop("selected", false);	 */
                $('select#column_option option').removeAttr("selected");
            }

            $("#column_option > option").each(function () {
                $("#column_option option[value='" + this.value + "']").attr('disabled', false);
            });
            $('#column_option').attr('disabled', false);

            $("#column_option option[value='" + $(this).val() + "']").prop('disabled', true);
        });

        $('#column_option').change(function (e) {
            /* $('#row_option option').not(':selected').prop('disabled', false);
             $('#column_option option').not(':selected').prop('disabled', false);
             $('#row_option').attr('disabled', false); */
            selected_row = $("#row_option option:selected").val();
            if ($(this).val() == selected_row)
            {
                /* 				alert();	
                 $("#row_option").val("");	 */
                $("#row_option option:selected").prop("selected", false);
            }
            $("#row_option > option").each(function () {
                $("#row_option option[value='" + this.value + "']").attr('disabled', false);
            });
            $('#row_option').attr('disabled', false);
            $("#row_option option[value='" + $(this).val() + "']").prop('disabled', true);
        });


        if ($("#min_qty_required").prop('checked') == true)
        {
            $("#minimum_quantity").show();
        } else
        {
            $("#minimum_quantity").hide();
        }
        $('#min_qty_required').click(function () {
            if (this.checked) {
                $("#minimum_quantity").show();
            } else {
                $("#minimum_quantity").hide();
            }
        });
    });

</script>
@endsection


