@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'Products',
            buttons: {
            secondary: [
            {
            label: 'General Settings',
                    href : "{{ url('/dashboard') }}",
                    loading: true
            },
            {
            label: 'Shortcodes',
                    href : "{{ url('/shortcodes') }}",
                    loading: true
            },
            {
            label: 'Help',
                    href : "{{ url('/help') }}",
                    loading: true
            }
            ]
            }
    });
    });
</script>

<div class = "formcolor">
    <ul class="nav nav-tabs dashboard_tabs">
        <li>
            <a href="shortcodes">Configuration</a>
        </li>
        <li>
            <a href="dashboard">General Settings</a>
        </li>
        <li class="active">
            <a href="products">Product List</a>
        </li>
    </ul>

    <br>

    <div class = "row">
        <div class="">          
            <div class="col-sm-12">    
                <table id="products_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Vendor</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>    
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
    var table = $('#products_list').DataTable({
    "pageLength" : 20,
            "lengthChange" : false,
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax": '{{url('getproducts')}}',
            "columns": [
            {
            "render": function (data, type, JsonResultRow, meta) {
            return '<img src="' + data + '" style="width:50px;">';
            }
            },
            {
            "render": function (data, type, JsonResultRow, meta) {
            return data;
            }
            },
            {
            "data": "2", "orderable": false
            },
            {
            "data": "3", "orderable": false
            },
            {
            "render": function (data, type, JsonResultRow, meta) {
            return data;
            }
            }],
            "order": [[1, 'asc']]
    });
//        // Apply the search
//        table.columns().every(function () {
//            var that = this;
//            $('input', this.header()).on('keyup change', function () {
//                if (that.search() !== this.value) {
//                    that.search(this.value).draw();
//                }
//            });
//        });
    });
</script>

@endsection