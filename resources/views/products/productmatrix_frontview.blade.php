<?php
$settings_array = json_decode($general_settings_json, true);
if ($settings_array['min_qty_msg_on_product_page'] == "") {
    $quantity_message = "Please enter minimum quantity";
} else {
    $quantity_message = $settings_array['min_qty_msg_on_product_page'];
}
if ($settings_array['quantity_position'] == null) {
    $price_relative_to_quantity = null;
    if ($settings_array['price_position'] == "show_above_input_box_before_quantity") {
        $price_position = "show_above_input_box";
    }
    if ($settings_array['price_position'] == "show_above_input_box_after_quantity") {
        $price_position = "show_above_input_box";
    }
    if ($settings_array['price_position'] == "show_below_input_box_before_quantity") {
        $price_position = "show_below_input_box";
    }
    if ($settings_array['price_position'] == "show_below_input_box_after_quantity") {
        $price_position = "show_below_input_box";
    }
}
if ($settings_array['price_position'] == null) {
    $price_relative_to_quantity = null;
    $price_position = null;
} else {
    if ($settings_array['quantity_position'] == "show_above_input_box" && $settings_array['price_position'] == "show_above_input_box_before_quantity") {
        $price_position = "show_above_input_box";
        $price_relative_to_quantity = "before_quantity";
    }
    if ($settings_array['quantity_position'] == "show_above_input_box" && $settings_array['price_position'] == "show_above_input_box_after_quantity") {
        $price_position = "show_above_input_box";
        $price_relative_to_quantity = "after_quantity";
    }
    if ($settings_array['quantity_position'] == "show_above_input_box" && $settings_array['price_position'] == "show_below_input_box_before_quantity") {
        $price_position = "show_below_input_box";
        $price_relative_to_quantity = null;
    }
    if ($settings_array['quantity_position'] == "show_above_input_box" && $settings_array['price_position'] == "show_below_input_box_after_quantity") {
        $price_position = "show_below_input_box";
        $price_relative_to_quantity = null;
    }
    if ($settings_array['quantity_position'] == "show_below_input_box" && $settings_array['price_position'] == "show_above_input_box_before_quantity") {
        $price_position = "show_above_input_box";
        $price_relative_to_quantity = null;
    }
    if ($settings_array['quantity_position'] == "show_below_input_box" && $settings_array['price_position'] == "show_above_input_box_after_quantity") {
        $price_position = "show_above_input_box";
        $price_relative_to_quantity = null;
    }
    if ($settings_array['quantity_position'] == "show_below_input_box" && $settings_array['price_position'] == "show_below_input_box_before_quantity") {
        $price_position = "show_below_input_box";
        $price_relative_to_quantity = "before_quantity";
    }
    if ($settings_array['quantity_position'] == "show_below_input_box" && $settings_array['price_position'] == "show_below_input_box_after_quantity") {
        $price_position = "show_below_input_box";
        $price_relative_to_quantity = "after_quantity";
    }
}
?>
<style>
    .productmatrix-main ul {
        margin : 0px !important;
        padding : 0px !important;
		display: flex;
    }
    .productmatrix-main ul li {
        display: block;
        float: left;
        width: <?php echo ($total_width > 0 ? $total_width . '%' : '25%'); ?>;
        height : auto;<?php
        /* if (!isset($settings_array['show_price']) && !isset($settings_array['show_stock'])) {
            echo '35px';
        } else if (isset($settings_array['show_price']) && !isset($settings_array['show_stock'])) {
            echo '60px';
        } else if (isset($settings_array['show_stock']) && !isset($settings_array['show_price'])) {
            echo '60px';
        } else if (isset($settings_array['show_price']) && isset($settings_array['show_stock'])) {
            echo '80px';
        } */
        ?>;
        text-align:center;
        <?php if ($settings_array['quantity_position'] == "show_above_input_box" || $settings_array['price_position'] == "show_above_input_box_before_quantity" || $settings_array['price_position'] == "show_above_input_box_after_quantity") echo "position:relative;"; ?>
        border : <?php
        if ($settings_array['cell_border_status'] == '1') {
            if ($settings_array['cell_color']) {
                echo "1px solid " . $settings_array['cell_color'];
            } else {
                echo "1px solid black";
            }
        }
        ?>;
        padding-top : 5px;
        position : relative;
        margin-bottom : 0px;
        display : flex;
        flex-direction : column;
        justify-content : center;
    }
    #product_matrix_form .row_background li {
        background:<?php
        if ($settings_array['top_header_color'] != "") {
            echo $settings_array['top_header_color'];
        } else {
            echo "white";
        }
        ?>;		
        color:<?php
        if ($settings_array['top_text_color'] != "") {
            echo $settings_array['top_text_color'];
        } else {
            echo "black";
        }
        ?>;	
        height: auto !important;
		display: block !important;
        flex-direction: column;
        justify-content: center;
        border:1px solid <?php
        if ($settings_array['top_header_color'] != "") {
            echo $settings_array['top_header_color'];
        } else {
            echo "white";
        }
        ?>;               
        font-size:<?php echo $settings_array['row_font_size']; ?>;
        /* padding-left: 5px;
        padding-right: 5px; */
        text-align: center !important;
        font-weight: 600;     
        border:none;
        margin-bottom : 0px;
		width: <?php echo ($total_width > 0 ? $total_width . '%' : '25%'); ?>;          
    }
    #product_matrix_form ul.column_background > li:first-child,#product_matrix_form ul.last_row_background > li:first-child {
        background:<?php
        if ($settings_array['left_header_color'] != "") {
            echo $settings_array['left_header_color'];
        } else {
            echo "white";
        }
        ?>;
        vertical-align:middle;
        color:<?php
        if ($settings_array['left_text_color'] != "") {
            echo $settings_array['left_text_color'];
        } else {
            echo "black";
        }
        ?>;
        display: flex;
        flex-direction: column;
        justify-content: center;
        font-size:<?php echo $settings_array['column_font_size']; ?>;	
        border:1px solid <?php
        if ($settings_array['left_header_color'] != "") {
            echo $settings_array['left_header_color'];
        } else {
            echo "white";
        }
        ?>;
        justify-content : inherit;
        font-weight: 600;
        /* padding-left: 8px !important;
        padding-right: 8px !important; */
        text-align: center;
		width: <?php echo ($total_width > 0 ? $total_width . '%' : '25%'); ?>;          
    }	
    /*.productmatrix-matrix:last-child > li:first-child
        {
            margin-bottom:10px;
            border:1px solid red;
        }	
	*/
    ul.last_row_background li {
        display: block;
        float: left;        
		/* 
		width: <?php echo ($total_width_options > 0 ? $total_width_options . '%' : '25%'); ?>;            
		*/		
		width: <?php echo ($total_width > 0 ? $total_width . '%' : '25%'); ?> !important;          
        text-align:center;	        
        word-wrap:break-word;	
        font-size:12px;
        height : 35px;
        margin-bottom : 0px;
		color:<?php if ($settings_array['price_color'] != "") { echo $settings_array['price_color']; } else { echo "black"; } ?>;	
    }
    ul.last_row_background:first-child {
        border:1px solid <?php
        if ($settings_array['cell_border_status'] == 1) {
            if ($settings_array['cell_color'] != "") {
                echo $settings_array['cell_color'];
            } else {
                echo "white";
            }
        }
        ?>;		
    }
    .productmatrix-main .container #productmatrix-matrix .column_background li {
        padding: 5px;
        max-height: 100%;
        margin-bottom: 0;
        position : relative;
    }
    .productmatrix-main .container ul.column_background li {		
        max-height: 60px;
        margin-bottom: 20px;
    }
	#product_matrix_form {
        border:  <?php
        if ($settings_array['border_status'] == 1) {

            if ($settings_array['border_color'] != "") {
                echo "1px solid " . $settings_array['border_color'];
            } else {
                echo "1px solid black";
            }
        }
        ?>;
    }
    .minimum_quantity_required_message {
        color : #FF0000;
        float:right;
    }
    .min_qty {
        color : black;
    }
    .out_of_stock {
        font-size : 10px;
    }
    .show_price {
        text-align:center;
        font-weight:600;
        left:0;
        right:0;
        color:<?php echo $settings_array['price_color'] ?>;	
        font-size:<?php echo $settings_array['price_font_size']; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "after_quantity") echo "position:absolute"; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "after_quantity") echo "top:15px"; ?>;

        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "before_quantity") echo "position:absolute"; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "before_quantity") echo "top:0px"; ?>;

        <?php if ($price_position == "show_below_input_box" && $price_relative_to_quantity == "after_quantity") echo "position:absolute"; ?>;
        <?php if ($price_position == "show_below_input_box" && $price_relative_to_quantity == "after_quantity") echo "margin-top:25px"; ?>;

        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == null) echo "position: absolute"; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == null) echo "top: 0px"; ?>;
    }
    .inventory_quantity {
        text-align:center;        
        color:<?php echo $settings_array['quantity_color'] ?>;	
        font-size:<?php echo $settings_array['quantity_font_size'] ?>;	
        <?php if ($settings_array['quantity_position'] == "show_above_input_box") echo "position: absolute"; ?>;
        <?php if ($settings_array['quantity_position'] == "show_above_input_box") echo "top: 0px"; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "before_quantity") echo "position: absolute"; ?>;
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "before_quantity") echo "top: 15px"; ?>;
        font-weight:600;
        left:0;
        right:0;
    }    
    input.quantity {
        <?php if ($settings_array['quantity_position'] == "show_above_input_box" || $settings_array['price_position'] == "show_above_input_box_before_quantity" || $settings_array['price_position'] == "show_above_input_box_after_quantity") echo "margin-top: 10px"; ?>;  
        <?php if ($price_position == "show_above_input_box" && $price_relative_to_quantity == "before_quantity") echo "margin-top: 40px"; ?>;
        padding : 6px;
        width:60px;
        text-align: center;
        float : none !important;
        font-size: 13px;
        border-radius: 0px;
        border: 1px solid #ccc;
        font-weight : 700;
        margin : 0 auto;
    }
    #productmatrix-matrix ul.productmatrix_row > li {
        color:<?php echo $settings_array['top_text_color'] ?>;	
        font-size:<?php echo $settings_array['row_font_size']; ?>;
    }
    li.productmatrix_row {
        word-wrap: break-word;
    }
    .productmatrix_column {
        color:<?php echo $settings_array['left_text_color'] ?>;	
        font-size:<?php echo $settings_array['column_font_size']; ?>;
    }
    .product_matrix_container {
        width: 100% !important;
    }
    .add_to_cart {
        margin-left: 15px;
        width: 70%;
        margin : 0;
        margin-top : 12px;
    }
    .one_dimension_heading {
        text-align : center;
        background:<?php
        if ($settings_array['top_header_color'] != "") {
            echo $settings_array['top_header_color'];
        } else {
            echo "white";
        }
        ?>;		
        color:<?php
        if ($settings_array['top_text_color'] != "") {
            echo $settings_array['top_text_color'];
        } else {
            echo "black";
        }
        ?>;	
    }
    #dropdown_option {
        width : 100%;
    }
    .inventoty_quantity_span {
        font-weight : 500;
    }
    .show_price_span {
        font-weight : 500;
    }
    .add_to_cart_div {
        background: #f5f4f4;
        clear:both;
        text-align: center;
        padding-bottom: 20px;
        padding-left: 20px;
        padding-right:20px;
        border-top : 1px solid #e6e5e5;
        overflow : hidden;
    }
    ul.column_background > li:last-child {
        padding	: 0 !important;
        word-wrap:break-word;
        font-size:12px;
        display : flex;
        flex-direction : column;
        justify-content : center;
		color:<?php if ($settings_array['top_text_color'] != "") { echo $settings_array['top_text_color']; } else { echo "black"; } ?>;	
    }
    .productmatrix-dropdown {
        padding: 15px 15px 0px 15px;
        margin-bottom : 20px;
    }
    .productmatrix-dropdown  #dropdown_option
	{  
		width: 76%; 
	}
    .last_row
	{
		 width: 60%;
		 display:flex;
	}
	.last_row li
	{
		width:100%;
		text-align:center;
	}
	.first_row li.productmatrix_row
	{		
		width: <?php echo ($total_width_options > 0 ? $total_width_options . '%' : '25%'); ?> !important;          
	}
	.first_row
	{
		width: 100%;
	}
	.final_total_price
	{
		width: 20% !important;
	}
	<?php
    if ($settings_array['additional_css']) {
        echo $settings_array['additional_css'];
    }
    ?>
	.productmatrix-main ul li input.quantity {
    width: 100%;
    	text-align: center;
	}
	.productmatrix-main .last_row_background .last_row li {
    	width: 100% !important;
	}
	#product_matrix_form .row_background li:first-child {
    	/* border-right:1px solid  <?php
        if ($settings_array['top_text_color'] != "") {
            echo $settings_array['top_text_color'];
        } else {
            echo "black";
        }
        ?>;	;
    	border-bottom:1px solid <?php
        if ($settings_array['top_text_color'] != "") {
            echo $settings_array['top_text_color'];
        } else {
            echo "black";
        }
        ?>;	; */
	}
</style>
<form type = "POST" id="product_matrix_form">    
	{{ csrf_field() }}
    <input id = "variants_array_json" type ="hidden" value ="{{ $variants_array_json}}"/>
	<input id ="general_settings_json" type ="hidden" value = "{{ $general_settings_json }}" />
	<input id ="dimension" type ="hidden" value ="{{ $dimension }}"/>
	<input id ="min_qty" type="hidden" value ="{{ $min_qty }}"/>
	<input id ="min_qty_error_msg" type="hidden" value ="{{ $min_qty_error_msg }}"/>
	<input id ="disable_app_for_product" type="hidden" value ="{{ $disable_app_for_product }}"/>
    <input id ="money_format" type="hidden" value ="{{ $money_format }}"/>
	@if(count($options_array['dropdown_option']['values']) > 0)
		<div class ="row">                
			<div class ="col-md-12 productmatrix-dropdown">
				<b>{{ $options_array['dropdown_option']['name'] }}</b>
				<br/>
				<select id = "dropdown_option" class = "form-control">
					<option value="" selected disabled>Please select</option>
					@foreach($options_array['dropdown_option']['values'] as $dropdown_option)
					<option>{{ $dropdown_option  }}</option>
					@endforeach
				</select>
			</div>        
		</div>
	@endif
    <div class = "productmatrix-main">
        <div class = "product_matrix_container">
            @if(!count($options_array['column_option']['values']) > 0)
            <div class ="one_dimension_heading">
                <b>
                    {{ $options_array['row_option']['name'] }}
                </b>
            </div>
            @endif
            @if(count($options_array['row_option']['values']) > 0 && !count($options_array['column_option']['values']) > 0)            
			<ul class="row_background">
                @if(!count($options_array['column_option']['values']) > 0)
                <li class = "productmatrix_row">  </li>
                @endif
                <!--div class ="first_row">
                </div-->
                <li>{{ $column_total_label }}</li>
            </ul>
            @endif
            @if(count($options_array['row_option']['values']) > 0  && count($options_array['column_option']['values']) > 0)            
			<ul class="row_background">
                @if(count($options_array['column_option']['values']) > 0)
                <li class = "productmatrix_row"> {{ $options_array['column_option']['name'].'/ '.$options_array['row_option']['name'] }} </li>
                @endif
                <!--div class ="first_row">
                </div-->
                <li>{{ $column_total_label }}</li>
            </ul>
            @endif
            <div id = "productmatrix-matrix">
                @if(!count($options_array['column_option']['values']) >  0 AND !count($options_array['dropdown_option']['values']) > 0)
                <ul class = "productmatrix_row">
                </ul>
                @endif
            </div>
            @if(count($options_array['row_option']['values']) > 0)
            <ul class="last_row_background">
                <li>{{ $row_total_label }}</li>
                <!--div class ="last_row">
                </div-->
                <li class = "final_total_price"></li>
            </ul>
            @endif
        </div>
    </div>
    <br/>
    <div class ="add_to_cart_div">
        <input type = "submit" name ="add_to_cart" value ="<?php
        if ($settings_array['add_to_cart_text'] != "") {
            echo $settings_array['add_to_cart_text'];
        } else {
            echo "Add to Cart";
        }
        ?>" class ="add_to_cart btn btn-primary"/>
        <div class ="minimum_quantity_required_message"></div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){     
	var intermediate_price = 0;
	var quantity_value = 0;
	var variants_array = $('#variants_array_json').val();
	var row_options_array;
	var general_settings_array = $('#general_settings_json').val();
	var disable_app_for_product = $('#disable_app_for_product').val();
	var money_format = $('#money_format').val();      
	if (disable_app_for_product == '1')
	{
		$('#product_matrix_form').hide();
		$('.zestard-product-matrix').show();
		return false;
	}
	var general_settings = $.parseJSON(general_settings_array);
	var show_stock = general_settings.show_stock;
	var show_price = general_settings.show_price;	
	var allow_out_of_stock = general_settings.allow_out_of_stock;
	var show_total = general_settings.show_total;
	var price_label = general_settings.price_label;
	var quantity_label = general_settings.quantity_label;
	var one_dimensional_array = false;
	function render_product_matrix(selected_variants_array_json, product_matrix_string)
	{		
		$.each(selected_variants_array_json, function (column_option, row_options) {
			row_options_array = [];
			product_matrix_string = product_matrix_string + '<ul  class="column_background">';
			product_matrix_string = product_matrix_string + '<li class ="productmatrix_column">' + column_option + '</li>';
			$.each(row_options, function (key, values) {
				
				row_option_key = key;
				row_options_array.push(row_option_key);
				if (values) {
					if (typeof (values) == 'object')
					{
						formatted_money = Shopify.formatMoney(values.price,money_format);                               						
						product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
						class = "quantity zestard_productmatrix_quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + row_option_key + '"  />';
						if (show_price == 1)
						{
							product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + '<span class ="show_price_span">' + formatted_money + '</span></div>';
						}
						if (show_stock == 1)
						{
							product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + '<span class = "inventoty_quantity_span">' + values.inventory_quantity + '</span></div>';
						}
						product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
					} else {
						if (values == "out of stock")
						{
							product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
							return;
						}
					}
				} else {
					product_matrix_string = product_matrix_string + '<li>-</li>';
					return;
				}
			});
			product_matrix_string = product_matrix_string + '<li data-column = "total_price" data-final_total_column="0" class ="total_intermediate_price"></li>';
			product_matrix_string = product_matrix_string + '</ul>';
			//Total row wise
			row_options_string = "";

			$.each(row_options_array, function (index, row_option) {
				row_options_string = row_options_string + '<li class = "productmatrix_row">' + row_option + '</li>';
			});
			// code added for last row
			last_row_string = "";
			$.each(row_options_array, function (index, row_option) {
				last_row_string = last_row_string + '<li class = "total_' + row_option.replace(/ /g,"_") + '" data-total = "row_intermediate_total" data-final_total_row="0"></li>';
			});
			// $('.first_row').html(row_options_string);
			$('#productmatrix-matrix').html(product_matrix_string);
			// $('.last_row').html(last_row_string);
		});		
		if($("#dropdown_option").length){			
			$(".productmatrix_row").not(':first').remove();			
			$(".last_row_background li").not(':first').not(':last').remove();			
		}				
		$('.row_background li:eq(0)').after(row_options_string);
		$('.last_row_background li:eq(0)').after(last_row_string);
	}
	if($("#dropdown_option").length){
		$('.productmatrix-main').hide();
		$('#dropdown_option').change(function () {
			$('.productmatrix-main').show();
			var product_matrix_string = "";
			dropdown_option_selected = this.value;
			variants_array_json = $.parseJSON(variants_array);
			$.each(variants_array_json, function (key, value) {
				if(key == dropdown_option_selected)
				{
					selected_variants_array_json = value;
				}
			});			
			render_product_matrix(selected_variants_array_json, product_matrix_string);
		});
	} 
	else 
	{
		var product_matrix_string = "";
		variants_array_json = $.parseJSON(variants_array);

		$.each(variants_array_json, function (column_option, row_options) {
			if (row_options.hasOwnProperty("price") || row_options == "out of stock") {
				one_dimensional_array = true;
			}
		});
		if (one_dimensional_array) {
			//console.log("one");
			// console.log(variants_array_json);
			row_options_array = [];
			product_matrix_string = product_matrix_string + '<li></li>';			
			$.each(variants_array_json, function(key, values){
				row_option_key = key;
				row_options_array.push(row_option_key);                    
				formatted_money = Shopify.formatMoney(values.price,money_format);
				if(values) 
				{
					if (typeof (values) == 'object')
					{
						product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
						class = "quantity zestard_productmatrix_quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + key + '"  />';
						if (show_price)
						{
							product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' <span class ="show_price_span">' + formatted_money + '</span></div>';
						}
						if (show_stock)
						{
							product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' <span class = "inventoty_quantity_span">' + values.inventory_quantity + '</span></div>';
						}
						product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
					} 
					else 
					{
						if (values == "out of stock")
						{
							product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
							return;
						}
					}
				} 
				else 
				{						
					product_matrix_string = product_matrix_string + '<li>-</li>';
					return;
				}
			});
			//Total row wise
			row_options_string = "";
			$.each(row_options_array, function (index, row_option) {
				row_options_string = row_options_string + '<li class = "productmatrix_row">' + row_option + '</li>';
			});
			// code added for last row
			last_row_string = "";
			$.each(row_options_array, function (index, row_option) {
				last_row_string = last_row_string + '<li class = "total_' + row_option.replace(/ /g,"_") + '" data-total = "row_intermediate_total" data-final_total_row="0"></li>';
			});
			product_matrix_string = product_matrix_string + '<li data-column = "total_price" data-final_total_column="0" class ="total_intermediate_price"></li>';
			// $('.first_row').html(row_options_string);
			// $('#productmatrix-matrix ul').html(product_matrix_string);
			// $('.last_row').html(last_row_string);
			$('.row_background li:eq(0)').after(row_options_string);
			$('#productmatrix-matrix ul').html(product_matrix_string);			
			$('.last_row_background li:eq(0)').after(last_row_string);
		} 
		else 
		{
			render_product_matrix(variants_array_json, product_matrix_string);
		}
	}
	$('#productmatrix-matrix').on('keyup mouseup', '.quantity', function () {
		quantity_value = this.value;
		var flag = 0;
		if (allow_out_of_stock != '1')
		{
			if (show_stock == '1')
			{
				max_stock_allowed = $(this).siblings('.inventory_quantity').data('inventory_qty');
				if (parseInt(quantity_value) > parseInt(max_stock_allowed))
				{
					flag = 1;
					this.value = 0;
					alert('Please enter maximum quantity ' + max_stock_allowed);
				}
			}
		}
		//Show Total Row
		if (show_total == "row_total" || show_total == "both")
		{
			/* console.log('here 1'); */
			var quantity_array_column_wise = [];
			var total_qty = 0;
			column_value = $(this).data('column');
			//Show Quantity
			$('input[data-column="' + column_value + '"]').map(function () {
				row_price = $(this).data('price');
				row_qty = this.value;
				row_intermediate_total = parseFloat(row_price * row_qty).toFixed(2);				
				quantity_array_column_wise.push(row_intermediate_total);                
				total_qty += parseFloat(row_intermediate_total);
			});
			if(flag == 1) 
			{
				formatted_money_for_total = Shopify.formatMoney("0.00",money_format);
				$('.total_' + column_value).data('final_total_row' , "0.00");
				$('.total_' + column_value).html(formatted_money_for_total);
			} 
			else  
			{
				final_price_row = total_qty.toFixed(2);
				$('.total_' + column_value.replace(/ /g,"_")).data('final_total_row' , final_price_row);
				formatted_money_for_total = Shopify.formatMoney(final_price_row,money_format);				
				$('.total_' + column_value.replace(/ /g,"_")).html(formatted_money_for_total);
			}
		}           
		// Show Total Column 
		if (show_total == "column_total" || show_total == "both")
		{
			/* console.log('here 2'); */
			price_value = $(this).data('price');
			intermediate_price = parseFloat(quantity_value * price_value).toFixed(2);
			$(this).parent().next().val(intermediate_price);
			var sum = 0;
			var total_values_array = [];
			$(this).parent().parent().children('input.set_price').each(function () {
				total_values_array.push(this.value);
			});
			$.each(total_values_array, function (index, value) {
				sum += parseFloat(value);
			});
			sum = parseFloat(sum).toFixed(2);
			if (flag == 1)
			{
				formatted_sum = Shopify.formatMoney("0.00",money_format);
				formatted_money_for_total = Shopify.formatMoney(final_price_row,money_format);
				
				$(this).parent().parent().children('li.total_intermediate_price').data('final_total_column',"0.00");
				$(this).parent().parent().children('li.total_intermediate_price').html(formatted_sum);
			} 
			else  
			{
				formatted_sum = Shopify.formatMoney(sum,money_format);
				$(this).parent().parent().children('li.total_intermediate_price').data('final_total_column',sum);
				$(this).parent().parent().children('li.total_intermediate_price').html(formatted_sum);
			}
		}            
		if (show_total == "column_total" || show_total == "both")
		{
			/* console.log('here 3'); */
			var total_final_price = 0;
			$('li[data-column="total_price"]').map(function () {
				if ($(this).text() == "")
				{
					$(this).text(0);
				}
				total_final_price += parseFloat($(this).data('final_total_column'));
			});               
			total_final_price = parseFloat(total_final_price).toFixed(2);                
			if (flag == 1)
			{
				formatted_final = Shopify.formatMoney("0.00",money_format);								
				$('.final_total_price').html(formatted_final);
			} 
			else
			{
				formatted_final = Shopify.formatMoney(total_final_price,money_format);								
				$('.final_total_price').html(formatted_final);
			}
		}
		if (show_total == "row_total" || show_total == "both")
		{
			var total_final_price = 0;
			$('li[data-total="row_intermediate_total"]').map(function(){                    
				if ($(this).text() == "")
				{
					$(this).text(0);
				}
				//total_final_price += parseFloat($(this).text());                    
				total_final_price += parseFloat($(this).data('final_total_row'));
			});
			total_final_price = parseFloat(total_final_price).toFixed(2);			
			if(total_final_price != 0.00)
			{
				if (flag == 1)
				{
					formatted_final = Shopify.formatMoney("0.00",money_format);
					$('.final_total_price').html(formatted_final);
				} 
				else
				{
					formatted_final = Shopify.formatMoney(total_final_price,money_format);
					$('.final_total_price').html(formatted_final);
				}
			}
		}
	});
	min_qty_value = parseInt($('#min_qty').val()) - current_quantity;
	//if(min_qty_value != "" min_qty_value > 0)
	if (min_qty_value > 0)
	{
		// $('.minimum_quantity_required_message').html('Minimum quantity required is: <span class ="min_qty">' + min_qty_value + "</span>");
		$('.minimum_quantity_required_message').html('<?php echo $quantity_message . " "; ?>' + min_qty_value);
	}
	//for add products in cart
	$("#product_matrix_form").submit(function (event) {
		if($("#dropdown_option").length) {

			//Stop Submiting Form when Type is not Selected
			if ($("#dropdown_option").val() == "" || typeof $("#dropdown_option").val() === 'undefined')
			{
				alert('Please Select an option from dropdown first!');
				$("#dropdown_option").focus();
				return false;
			}
		}
		min_qty_required = $('#min_qty').val();
		min_qty_error_msg = $('#min_qty_error_msg').val();
		//total_qty = 0;
		total_qty = current_quantity;
		$(".quantity").each(function () {
			if ($(this).attr("data-variant_id") != '' && this.value > 0) {
				total_qty = total_qty + parseInt(this.value);
			}
		});
		if (min_qty_required != "")
		{
			if (total_qty < min_qty_required)
			{
				event.preventDefault();
				alert(min_qty_error_msg);
				return false;
			}
		}
		$(".quantity").each(function () {
			event.preventDefault();
			if ($(this).attr("data-variant_id") != '' && this.value > 0) {
				var quantity = this.value;
				var varient_id = $(this).data("variant_id");
				$.ajax({
					url: '/cart/add.js',
					data: {quantity: quantity, id: varient_id},
					async: false
				});
			}
			window.location.href = '/cart';
		});
	});
});
</script>