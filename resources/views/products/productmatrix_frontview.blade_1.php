<?php
$settings_array = json_decode($general_settings_json, true);
?>
<style>
    #product_matrix_form
    {
        padding: 10px;
        <?php if ($settings_array['border_status'] == 1) echo "border:1px solid " . $settings_array['border_color']; ?> !important;	
        width:650px;
    }    	
    .productmatrix-main 
    {
        margin-top: 20px;
		
    }    
    .productmatrix-main ul li, .last_row li 	
    {
        display: block;
        float: left;
        width: <?php echo ($total_width > 0 ? $total_width . '%' : '25%'); ?>;
        //margin-bottom: 10px;        
		height: 85px;
        //padding-right: 5px;	
		
		text-align:center;
        <?php if ($settings_array['quantity_position'] == "show_above_input_box" || $settings_array['price_position'] == "show_above_input_box") echo "position:relative" ; ?>
    }
    .minimum_quantity_required_message
    {
        color : #FF0000;
        float:right;
    }
    .min_qty
    {
        color : black;
    }
    .out_of_stock
    {
        color: #F00;
    }
    .show_price
    {
		text-align:center;
        font-weight:700;
		left:0;
		right:0;
		color:<?php echo $settings_array['price_color'] ?>;	
        font-size:<?php echo $settings_array['price_font_size']; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == "after_quantity") echo "position:absolute" ; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == "after_quantity") echo "top:10px" ; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == "before_quantity") echo "position:absolute" ; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == "before_quantity") echo "top:-15px" ; ?>;
        <?php if ($settings_array['price_position'] == "show_below_input_box" && $settings_array['price_relative_to_quantity'] == "after_quantity") echo "position:absolute" ; ?>;
        <?php if ($settings_array['price_position'] == "show_below_input_box" && $settings_array['price_relative_to_quantity'] == "after_quantity") echo "margin-top:15px" ; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == null) echo "position: absolute" ; ?>;
        <?php if ($settings_array['price_position'] == "show_above_input_box" && $settings_array['price_relative_to_quantity'] == null) echo "top: 0px" ; ?>
    }
    .inventory_quantity
    {
		text-align:center;        
        color:<?php echo $settings_array['quantity_color'] ?>;	
        font-size:<?php echo $settings_array['quantity_font_size'] ?>;	
        <?php if ($settings_array['quantity_position'] == "show_above_input_box") echo "position: absolute" ; ?>;
        <?php if ($settings_array['quantity_position'] == "show_above_input_box") echo "top: 0px" ; ?>;
		font-weight:700;
		left:0;
		right:0;
	}    
    input.quantity 
	{
        <?php if ($settings_array['quantity_position'] == "show_above_input_box" || $settings_array['price_position'] == "show_above_input_box") echo "margin-top: 15px" ; ?>    
	}
	.productmatrix_row 
	{
        color:<?php echo $settings_array['top_text_color'] ?>;	
        font-size:<?php echo $settings_array['row_font_size']; ?>;
	}
	.productmatrix_column 
	{
        color:<?php echo $settings_array['left_text_color'] ?>;	
        font-size:<?php echo $settings_array['column_font_size']; ?>;
	}
	.row_background li
	{			
		background:<?php if($settings_array['top_header_color'] != "") { echo $settings_array['top_header_color']; } else { echo "white"; } ?>;		
		color:<?php if($settings_array['top_text_color'] != "") { echo $settings_array['top_text_color']; } else { echo "black"; } ?>;	
		height:85px !important;			
		display: flex !important;
		font-weight:900;
		flex-direction: column;
		justify-content: center;
		border:1px solid <?php if($settings_array['top_header_color'] != "") { echo $settings_array['top_header_color']; } else { echo "white"; } ?>;		
	}
	ul.column_background > li:first-child
	{			
		background:<?php if($settings_array['left_header_color'] != "") { echo $settings_array['left_header_color']; } else { echo "white"; } ?>;
		vertical-align:middle;
		color:<?php if($settings_array['left_text_color'] != "") { echo $settings_array['left_text_color']; } else { echo "black"; } ?>;
		font-weight:900;
		display: flex;
		flex-direction: column;
		justify-content: center;	
		border:1px solid <?php if($settings_array['left_header_color'] != "") { echo $settings_array['left_header_color']; } else { echo "white"; } ?>;
	}
	.productmatrix-matrix:last-child > li:first-child
	{
		margin-bottom:10px;
		border:1px solid red;
	}
	ul.column_background > li
	{
		border:1px solid <?php if($settings_array['top_header_color'] != "") { echo $settings_array['top_header_color']; } else { echo "white"; } ?>;		
		padding:5px;	
	}
	.productmatrix-main .container #productmatrix-matrix .column_background li 
	{
		padding: 5px;
		max-height: 100%;
		margin-bottom: 0;	
	}
	.productmatrix-main .container ul.column_background li 
	{		
		max-height: 60px;
		margin-bottom: 20px;
	}
</style>

<form type = "POST" id="product_matrix_form">
    {{ csrf_field() }}
    <div class ="row">
        <input id = "variants_array_json" type ="hidden" value ="{{ $variants_array_json}}"/>
        <input  id ="general_settings_json" type ="hidden" value = "{{ $general_settings_json }}" />
        <input id ="dimension" type ="hidden" value ="{{ $dimension }}"/>
        <input id ="min_qty" type="hidden" value ="{{ $min_qty }}"/>
        <input id ="min_qty_error_msg" type="hidden" value ="{{ $min_qty_error_msg }}"/>

        <div class ="col-md-12">
            @if(count($options_array['dropdown_option']['values']) > 0)

            {{ $options_array['dropdown_option']['name'].":" }}
            <select id = "dropdown_option">
                <option value="" selected disabled>Please select</option>
                @foreach($options_array['dropdown_option']['values'] as $dropdown_option)
                <option>{{ $dropdown_option  }}</option>
                @endforeach
            </select>
            @endif
        </div>
    </div>
	<div class = "productmatrix-main">
        <div class = "container">
            @if(count($options_array['row_option']['values']) > 0 )
            <ul class="row_background">
                @if(count($options_array['column_option']['values']) > 0)
                <li class = "productmatrix_row"> {{ $options_array['column_option']['name'].'/'.$options_array['row_option']['name'] }} </li>
                @else 
                <li class = "productmatrix_row"> {{ $options_array['row_option']['name'] }} </li>
                @endif
                <div class ="first_row">

                </div>
                <li>{{ $column_total_label }}</li>
            </ul>
            @endif
            <div id = "productmatrix-matrix">
                @if((!count($options_array['column_option']['values']) >  0) AND (!count($options_array['dropdown_option']['values']) > 0))
                <ul class = "productmatrix_row">
                </ul>
                @endif
            </div>
            @if(count($options_array['row_option']['values']) > 0)
            <ul class="row_background">
                <li>{{ $row_total_label }}</li>
                <div  class ="last_row">
                </div>
                <li class = "final_total_price"></li>
            </ul>
            @endif

        </div>
        <div class ="minimum_quantity_required_message"></div>
    </div>
    <!--div class = "productmatrix-main">
        <div class = "container">
            @if(count($options_array['row_option']['values']) > 0 )
            <ul class="row_background">
                @if(count($options_array['column_option']['values']) > 0)
                <li class = "productmatrix_row"> {{ $options_array['column_option']['name'].' /'.$options_array['row_option']['name'] }} </li>
                @else 
                <li class = "productmatrix_row"> {{ $options_array['row_option']['name'] }} </li>
                @endif
                @foreach($options_array['row_option']['values'] as $row_option_value)
                <li class = "productmatrix_row">{{ $row_option_value }} </li>
                @endforeach
                <li>{{ $column_total_label }}</li>
            </ul>
            @endif

            <div id = "productmatrix-matrix">
                @if((!count($options_array['column_option']['values']) >  0) AND (!count($options_array['dropdown_option']['values']) > 0))
                <ul class = "productmatrix_row">
                </ul>
                @endif
            </div>

            @if(count($options_array['row_option']['values']) > 0)
            <ul class="column_background">
                <li>{{ $row_total_label }}</li>
                @foreach($options_array['row_option']['values'] as $row_option_value)
                <li class = "total_<?php echo $row_option_value; ?>" data-total = "row_intermediate_total"></li>
                @endforeach
                <li class = "final_total_price"></li>
            </ul>
            @endif

        </div>
        <div class ="minimum_quantity_required_message"></div>
    </div-->
    <br/>
    <input type = "submit" name ="add_to_cart" value ="Add to cart" class ="btn btn-primary form-control"/>
</form>

<script type ="text/javascript">
    $(document).ready(function () {
        var intermediate_price = 0;
        var quantity_value = 0;
        var variants_array = $('#variants_array_json').val();

        console.log(variants_array);

        var row_options_array;

        var general_settings_array = $('#general_settings_json').val();
        var general_settings = $.parseJSON(general_settings_array);

        var show_stock = general_settings.show_stock;
        var show_price = general_settings.show_price;

        var allow_out_of_stock = general_settings.allow_out_of_stock;
        var show_total = general_settings.show_total;

        var price_label = general_settings.price_label;
        var quantity_label = general_settings.quantity_label;

        var one_dimensional_array = false;

        function render_product_matrix(selected_variants_array_json, product_matrix_string)
        {
            $.each(selected_variants_array_json, function (column_option, row_options) {
                row_options_array = [];
                product_matrix_string = product_matrix_string + '<ul class="column_background">';
                product_matrix_string = product_matrix_string + '<li class ="productmatrix_column">' + column_option + '</li>';
                $.each(row_options, function (key, values) {
                    row_option_key = key;
                    row_options_array.push(row_option_key);

                    if (values) {
                        if (typeof (values) == 'object')
                        {
                            product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
                            class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + row_option_key + '"  />';

                            if (show_price == 1)
                            {
                                product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
                            }

                            if (show_stock == 1)
                            {
                                product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
                            }

                            product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
                        } else {
                            if (values == "out of stock")
                            {
                                product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
                                return;
                            }
                        }
                    } else {
                        product_matrix_string = product_matrix_string + '<li>-</li>';
                        return;
                    }
                });

                product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';

                product_matrix_string = product_matrix_string + '</ul>';

                //Total row wise
                row_options_string = "";

				
                $.each(row_options_array, function (index, row_option) {
                    row_options_string = row_options_string + '<li class = "productmatrix_row">' + row_option + '</li>';
                });

                // code added for last row
                last_row_string = "";

                $.each(row_options_array, function (index, row_option) {
                    last_row_string = last_row_string + '<li class = "total_' + row_option + '" data-total = "row_intermediate_total"></li>';
                });

                $('.first_row').html(row_options_string);
                $('#productmatrix-matrix').html(product_matrix_string);
                $('.last_row').html(last_row_string);
            });
        }

        if ($("#dropdown_option").length) {
            $('.productmatrix-main').hide();
            $('#dropdown_option').change(function () {
                $('.productmatrix-main').show();
                var product_matrix_string = "";
                dropdown_option_selected = this.value;
                variants_array_json = $.parseJSON(variants_array);
                $.each(variants_array_json, function (key, value) {
                    if (key == dropdown_option_selected)
                    {
                        selected_variants_array_json = value;
                    }
                });

                render_product_matrix(selected_variants_array_json, product_matrix_string);

            });
        } else {
            var product_matrix_string = "";
            variants_array_json = $.parseJSON(variants_array);

            $.each(variants_array_json, function (column_option, row_options) {
                if (row_options.hasOwnProperty("price") || row_options == "out of stock") {
                    one_dimensional_array = true;
                }
            });

            if (one_dimensional_array) {
                row_options_array = [];
                product_matrix_string = product_matrix_string + '<li></li>';
                $.each(variants_array_json, function (key, values) {
                    row_option_key = key;
                    row_options_array.push(row_option_key);

                    if (values) {
                        if (typeof (values) == 'object')
                        {
                            product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
                            class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + key + '"  />';

                            if (show_price)
                            {
                                product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
                            }

                            if (show_stock)
                            {
                                product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
                            }

                            product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
                        } else {
                            if (values == "out of stock")
                            {
                                product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
                                return;
                            }
                        }
                    } else {
                        product_matrix_string = product_matrix_string + '<li>-</li>';
                        return;
                    }

                });

                //Total row wise
                row_options_string = "";

                $.each(row_options_array, function (index, row_option) {
                    row_options_string = row_options_string + '<li class = "productmatrix_row">' + row_option + '</li>';
                });

                // code added for last row
                last_row_string = "";

                $.each(row_options_array, function (index, row_option) {
                    last_row_string = last_row_string + '<li class = "total_' + row_option + '" data-total = "row_intermediate_total"></li>';
                });

                product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';

                $('.first_row').html(row_options_string);
                $('#productmatrix-matrix ul').html(product_matrix_string);
                $('.last_row').html(last_row_string);
            } else {

                render_product_matrix(variants_array_json, product_matrix_string);
            }
        }

        $('#productmatrix-matrix').on('keyup', '.quantity', function () {
            quantity_value = this.value;
			var flag = 0;
            if (allow_out_of_stock != '1')
            {
                if (show_stock == '1')
                {
					flag = 1;
                    max_stock_allowed = $(this).siblings('.inventory_quantity').data('inventory_qty');
                    if (parseInt(quantity_value) > parseInt(max_stock_allowed))
                    {
                        this.value = 0;
                        alert('Please enter minimum quantity ' + max_stock_allowed);
                    }
                }
            }

            // Show Total Row
            if (show_total == "row_total" || show_total == "both")
            {
                var quantity_array_column_wise = [];
                var total_qty = 0;

                column_value = $(this).data('column');

                //Show Quantity
                $('input[data-column=' + column_value + ']').map(function () {
                    row_price = $(this).data('price');
                    row_qty = this.value;

                    row_intermediate_total = parseFloat(row_price * row_qty).toFixed(2);

                    quantity_array_column_wise.push(row_intermediate_total);
                    total_qty += parseFloat(row_intermediate_total);
                });

                $('.total_' + column_value).html(total_qty.toFixed(2));
            }


            // Show Total Column 
            if (show_total == "column_total" || show_total == "both")
            {
                price_value = $(this).data('price');
                intermediate_price = parseFloat(quantity_value * price_value).toFixed(2);

                $(this).parent().next().val(intermediate_price);

                var sum = 0;

                var total_values_array = [];
                
                $(this).parent().parent().children('input.set_price').each(function () {
                    total_values_array.push(this.value);
                });

                $.each(total_values_array, function (index, value) {
                    sum += parseFloat(value);
                });

                sum = parseFloat(sum).toFixed(2);
				if(flag == 1)
				{
					$(this).parent().parent().children('li.total_intermediate_price').html("0.00");	
				}
				else
				{
					$(this).parent().parent().children('li.total_intermediate_price').html(sum);
                }                
            }

            if (show_total == "column_total" || show_total == "both")
            {
                var total_final_price = 0;
                $('li[data-column="total_price"]').map(function () {
                    if ($(this).text() == "")
                    {
                        $(this).text(0);
                    }

                    total_final_price += parseFloat($(this).text());
                });

                total_final_price = parseFloat(total_final_price).toFixed(2);
				
                $('.final_total_price').html(total_final_price);
            }

            if (show_total == "row_total" || show_total == "both")
            {
                var total_final_price = 0;
                $('li[data-total="row_intermediate_total"]').map(function () {
                    if ($(this).text() == "")
                    {
                        $(this).text(0);
                    }

                    total_final_price += parseFloat($(this).text());
                });

                total_final_price = parseFloat(total_final_price).toFixed(2);
                $('.final_total_price').html(total_final_price);
            }
        });
    });
</script>

<!--script type ="text/javascript">
    $(document).ready(function () {
        var intermediate_price = 0;
        var quantity_value = 0;
        var variants_array = $('#variants_array_json').val();
        var row_options_array;

        var general_settings_array = $('#general_settings_json').val();
        var general_settings = $.parseJSON(general_settings_array);

        //console.log(general_settings);
        //alert(general_settings);

        var show_stock = general_settings.show_stock;
        var show_price = general_settings.show_price;

        var allow_out_of_stock = general_settings.allow_out_of_stock;
        var show_total = general_settings.show_total;

        var price_label = general_settings.price_label;
        var quantity_label = general_settings.quantity_label;

        var one_dimensional_array = false;

        if ($("#dropdown_option").length) {
            $('.productmatrix-main').hide();
            $('#dropdown_option').change(function () {					
                $('.productmatrix-main').show();
                var product_matrix_string = "";
                dropdown_option_selected = this.value;
                variants_array_json = $.parseJSON(variants_array);
                $.each(variants_array_json, function (key, value) {
					
                    if (key == dropdown_option_selected)
                    {
                        selected_variants_array_json = value;
                    }
                });

                $.each(selected_variants_array_json, function (column_option, row_options) {
				
                    row_options_array = [];
                    product_matrix_string = product_matrix_string + '<ul class="column_background">';
                    product_matrix_string = product_matrix_string + '<li class ="productmatrix_column">' + column_option + '</li>';
                    $.each(row_options, function (key, values) {
                        row_option_key = key;
                        row_options_array.push(row_option_key);

                        if (values) {
                            if (typeof (values) == 'object')
                            {
                                product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
                            class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + row_option_key + '"  />';

                                if (show_price == 1)
                                {
                                    product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
                                }

                                if (show_stock == 1)
                                {
                                    product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
                                }

                                product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
                            } else {
                                if (values == "out of stock")
                                {
                                    product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
                                    return;
                                }
                            }
                        } else {
                            product_matrix_string = product_matrix_string + '<li>-</li>';
                            return;
                        }
                    });

                    row_options_array.push("total_price");

                    product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';

                    product_matrix_string = product_matrix_string + '</ul>';
                });

                $('#productmatrix-matrix').html(product_matrix_string);
            });
        } else {
            var product_matrix_string = "";
            variants_array_json = $.parseJSON(variants_array);
            $.each(variants_array_json, function (column_option, row_options) {

                if (row_options.hasOwnProperty("price")) {
                    one_dimensional_array = true;

                    // count the length of the object row_options
//                    row_options_array = [];
//                    product_matrix_string = product_matrix_string + '<li></li>';
//
//                    row_option_key = column_option;
//                    row_options_array.push(row_option_key);
//
//                    values = row_options;
//                    
// 
//                    
//                     product_matrix_string = product_matrix_string + '<li></li>';
//                    
//                    
//                    if (values) {
//                        if (typeof (values) == 'object')
//                        {
//                            product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
//                            class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + row_option_key + '"  />';
//
//                            if (show_price)
//                            {
//                                product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
//                            }
//
//                            if (show_stock)
//                            {
//                                product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
//                            }
//
//                            product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
//                        } else {
//                            
//                            alert('here');
//                            if (values == "out of stock")
//                            {
//                                product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
//                                return;
//                            }
//                        }
//                    } else {
//                        product_matrix_string = product_matrix_string + '<li>-</li>';
//                        return;
//                    }
//                    row_options_array.push("total_price");
//
//                    product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';
//                    alert("product matrix string");
//                    alert(product_matrix_string);
//                    $('#productmatrix-matrix ul').html(product_matrix_string);

                } else {

                    // count the length of the object row_options
                    row_options_array = [];
                    product_matrix_string = product_matrix_string + '<ul class="column_background">';
                    product_matrix_string = product_matrix_string + '<li class ="productmatrix_column">' + column_option + '</li>';


                    $.each(row_options, function (key, values) {
                        row_option_key = key;
                        row_options_array.push(row_option_key);

                        if (values) {
                            if (typeof (values) == 'object')
                            {
                                product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
                                                                class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + row_option_key + '"  />';
                                product_matrix_stock = '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
                                product_matrix_price = '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
                                if (show_price)
                                {
                                    product_matrix_string = product_matrix_string + product_matrix_price;
                                }

                                if (show_stock)
                                {
                                    product_matrix_string = product_matrix_string + product_matrix_stock;
                                }

                                product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
                            } else {
                                if (values == "out of stock")
                                {
                                    product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
                                    return;
                                }
                            }
                        } else {
                            product_matrix_string = product_matrix_string + '<li>-</li>';
                            return;
                        }
                    });

                    row_options_array.push("total_price");

                    product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';

                    product_matrix_string = product_matrix_string + '</ul>';

                    $('#productmatrix-matrix').html(product_matrix_string);

                }

            });

            if (one_dimensional_array)
            {
                product_matrix_string = product_matrix_string + '<li></li>';
                $.each(variants_array_json, function (key, values) {

                    if (values) {
                        if (typeof (values) == 'object')
                        {
                            product_matrix_string = product_matrix_string + '<li><input type = "number" min = 0 value = 0 name = "quantity" \n\
                            class = "quantity" data-price = ' + values.price + ' data-variant_id= ' + values.variant_id + ' data-column = "' + key + '"  />';

                            if (show_price)
                            {
                                product_matrix_string = product_matrix_string + '<div class = "show_price">' + price_label + ' ' + values.price + '</div>';
                            }

                            if (show_stock)
                            {
                                product_matrix_string = product_matrix_string + '<div class ="inventory_quantity" data-inventory_qty =' + values.inventory_quantity + '>' + values.quantity_label + ' ' + values.inventory_quantity + '</div>';
                            }

                            product_matrix_string = product_matrix_string + '</li><input type = "hidden" class = "set_price" value = "0" />';
                        } else {
                            if (values == "out of stock")
                            {
                                product_matrix_string = product_matrix_string + '<li class = "out_of_stock">' + values + '</li>';
                                return;
                            }
                        }
                    } else {
                        product_matrix_string = product_matrix_string + '<li>-</li>';
                        return;
                    }

                });

                product_matrix_string = product_matrix_string + '<li data-column = "total_price" class ="total_intermediate_price"></li>';
                $('#productmatrix-matrix ul').html(product_matrix_string);
            }
        }

        $('#productmatrix-matrix').on('keyup', '.quantity', function(){
            quantity_value = this.value;
			var flag = 0;
            if (allow_out_of_stock != '1')
            {
                if (show_stock == '1')
                {
                    max_stock_allowed = $(this).siblings('.inventory_quantity').data('inventory_qty');
                    if (parseInt(quantity_value) > parseInt(max_stock_allowed))
                    {
						flag = 1;
                        this.value = 0;
                        alert('Please enter minimum quantity ' + max_stock_allowed);
						var element = $(this).parent().parent().find(".total_intermediate_price");
						//alert(element);
						element.text("0.00");
						//alert(element.data("column"));
						//alert($(this).data('column'));
						//$('.total_' + $(this).data('column')).html("0.00");
                    }
                }
            }

            // Show Total Column 
            if (show_total == "column_total" || show_total == "both")
            {
                price_value = $(this).data('price');
                intermediate_price = parseFloat(quantity_value * price_value).toFixed(2);

                $(this).parent().next().val(intermediate_price);

                var sum = 0;

                var total_values_array = [];

                $(this).parent().parent().children('input.set_price').each(function () {
                    total_values_array.push(this.value);
                });

                $.each(total_values_array, function (index, value) {
                    sum += parseFloat(value);
                });

                sum = parseFloat(sum).toFixed(2);
				if(flag == 0)
				{
					$(this).parent().parent().children('li.total_intermediate_price').html(sum);
				}
				else
				{
					flag = 0;
					$(this).parent().parent().children('li.total_intermediate_price').html("0.00");
				}
            }

            // Show Total Row
            if (show_total == "row_total" || show_total == "both")
            {
                var quantity_array_column_wise = [];
                var total_qty = 0;

                column_value = $(this).data('column');

                //Show Quantity
                $('input[data-column=' + column_value + ']').map(function () {
                    row_price = $(this).data('price');
                    row_qty = this.value;

                    row_intermediate_total = parseFloat(row_price * row_qty).toFixed(2);

                    quantity_array_column_wise.push(row_intermediate_total);
                    total_qty += parseFloat(row_intermediate_total);
                });

                $('.total_' + column_value).html(total_qty.toFixed(2));

            }

            if (show_total == "column_total" || show_total == "both")
            {
                var total_final_price = 0;
                $('li[data-column="total_price"]').map(function () {
                    if ($(this).text() == "")
                    {
                        $(this).text(0);
                    }

                    total_final_price += parseFloat($(this).text());
                });

                total_final_price = parseFloat(total_final_price).toFixed(2);

                $('.final_total_price').html(total_final_price);
            }

            if (show_total == "row_total" || show_total == "both")
            {
                var total_final_price = 0;
                $('li[data-total="row_intermediate_total"]').map(function () {
                    if ($(this).text() == "")
                    {
                        $(this).text(0);
                    }

                    total_final_price += parseFloat($(this).text());
                });

                total_final_price = parseFloat(total_final_price).toFixed(2);
                $('.final_total_price').html(total_final_price);
            }
        });

    });
</script-->

<script>
    $(document).ready(function () {

        min_qty_value = parseInt($('#min_qty').val()) - current_quantity;

        //if(min_qty_value != "" min_qty_value > 0)
        if (min_qty_value > 0)
        {
            $('.minimum_quantity_required_message').html('Minimum quantity required is: <span class ="min_qty">' + min_qty_value + "</span>");
        }

        //for add products in cart
        $("#product_matrix_form").submit(function (event) {
            min_qty_required = $('#min_qty').val();
            min_qty_error_msg = $('#min_qty_error_msg').val();

            //total_qty = 0;
            total_qty = current_quantity;

            $(".quantity").each(function () {
                if ($(this).attr("data-variant_id") != '' && this.value > 0) {
                    total_qty = total_qty + parseInt(this.value);
                }
            });

            if (min_qty_required != "")
            {
                if (total_qty < min_qty_required)
                {
                    event.preventDefault();
                    alert(min_qty_error_msg);
                    return false;
                }
            }

            $(".quantity").each(function () {
                event.preventDefault();
                if ($(this).attr("data-variant_id") != '' && this.value > 0) {
                    var quantity = this.value;
                    var varient_id = $(this).data("variant_id");

                    $.ajax({
                        url: '/cart/add.js',
                        data: {quantity: quantity, id: varient_id},
                        async: false
                    });
                }
                window.location.href = '/cart';
            });
        });
    });
</script>
