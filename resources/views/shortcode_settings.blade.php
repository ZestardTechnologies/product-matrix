@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
    ShopifyApp.Bar.initialize({
    title: 'Help',
            buttons: {
            secondary: [
            {
            label: 'General Settings',
                    href: '{{ url("/dashboard") }}',
                    loading: true
            },
            {
            label: 'Product Settings',
                    href: '{{ url('products') }}',
                    loading: true
            },
            {
            label: 'Shortcodes',
                    href: '{{ url('help') }}',
                    loading: true
            }
            ]
            }
    });
    });</script>

<style>
    #regForm {
        display:block;
        background-color: #ffffff;
        margin: 0 auto;
        padding: 30px;
        width: 90%;
        height: 400px;
        overflow : auto;
        overflow-x : hidden;

    }

    /* Hide all steps by default: */
    .tab {
        display: none;
    }

    .navigation-btn button {
        background-color: #52c1bc;
        color: #ffffff;
        border: none;
        padding: 8px 15px;
        font-size: 17px;
        cursor: pointer;
        margin-top:-15px;
    }

    .navigation .nav-button
    {
        background-color: #52c1bc;
        color: #ffffff;
        padding:10px;
        cursor: pointer;
        border: none;
        font-size: 14px;
        margin-top:-15px;

        /*        background-color: #52c1bc;
                color: #ffffff;
                border: none;
                padding: 8px 15px;
                font-size: 17px;
                cursor: pointer;
                margin-top:-15px;*/
    }

    .top_navigation .nav-button-top
    {
        /*        padding: 8px 15px;
                margin-top:-15px;*/
        background-color: Transparent;
        /*    background-repeat:no-repeat;*/
        border: none;
        cursor:pointer;
        /*    overflow: hidden; */
        float:right;
    }

    .navigation .nav-button:hover
    {
        opacity: 0.8;
    }

    /*    .navigation-btn button:hover {
            opacity: 0.8;
        }*/

    #prevBtn {
        background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbbbbb;
        border: none;  
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
        background-color: #52c1bc;
    }

    .navigation {
        padding: 15px;
        padding-right: 35px;
        margin: 0 auto;
        background-color: #6a7386;
        display: block;
        height: 100%;
        width: 90%;
    }

    .form-control
    {
        margin-top: 6px;
        margin-bottom: 6px;
        box-shadow: none;
        border-radius: 3px;
        border: 1px solid #c6cedf;
        background-color: #f5f8fa;
        color: #5f5f5f;
        font-size: 12px;
        padding: 6px;
    }
</style>

<?php
$store_name = session('shop');
?>

<?php
if (Session::has('shop')) {
    $url_product_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} else {
    $url_product_template = "#";
    $url_product_page = "#";
    $url_cart_template = "#";
    $url_cart_page = "#";
}
?>

<div>
    <ul class="nav nav-tabs dashboard_tabs">
        <li class="active">
            <a href="shortcodes">Configuration</a>
        </li>
        <li>
            <a href="dashboard">General Settings</a>
        </li>
        <li>
            <a href="products">Product List</a>
        </li>
    </ul>
    <br>
    <br>

    <div id="regForm">
        <div class = "top_navigation">
            <button type="button" id="nextBtn_top"  class ="nav-button-top" onclick="nextPrev(1)"><h4><span class = "glyphicon glyphicon-forward"></span></h4></button>
            <button type="button" id="prevBtn_top" class ="nav-button-top" onclick="nextPrev( - 1)"><h4><span class = "glyphicon glyphicon-backward"></span></h4></button>

        </div>

        <!-- One "tab" for each step in the form: -->
        <div class="tab">
            <b><center><h4>Step 1/3 -  For displaying product matrix on product page</h4></center></b>            
            @include('help.adding_shortcode_product_page_step1')
        </div>
        <!--div class="tab">
            <b><center><h4>Step 2/4 - For hiding default options and variants dropdown box</h4></center></b>
            @include('help.adding_shortcode_product_page_step2')
        </div-->
        <div class="tab">
            <b><center><h4>Step 2/3 - For checking minimum quantity on cart page(Optional but recommended when minimum quantity validation is set)</h4></center></b>
            @include('help.adding_shortcode_cart_page_step1')
        </div>
        <div class="tab">
            <b><center><h4>Step 3/3 - For displaying error message if minimum quantity criteria is not satisfied(Optional but recommended when minimum quantity validation is set)</h4></center></b> 
            @include('help.adding_shortcode_cart_page_step2')
        </div>
        <div class="tab">
            <b><center><h4>Uninstalling the app</h4></center></b> 
            @include('help.uninstall_procedure')
        </div>
    </div>

    <div class = "navigation">
        <div class ="row">
            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center; margin-top:15px;">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>                
                <div style="float:right;">
                    <button type="button" id="prevBtn" class ="nav-button prevBtn" onclick="nextPrev( - 1)">Previous</button>
                    <button type="button" id="nextBtn"  class ="nav-button nextBtn" onclick="nextPrev(1)">Next</button>
                </div>
            </div>        
        </div>
    </div>
    <br/>
    <br/>
    <br/>
</div>

<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>

       <div class="modal fade" id="config_confirmation_modal" >
            <div class="modal-dialog">          
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><b>Confirmation</b></h4>
                    </div>
                    <div class="modal-body">
                        
                        <p> I have configured all 4 steps correctly. (Unless you select yes, app will not be displayed on store.)</p>
                            
                    </div>        
                    <div class="modal-footer">
                         <div class="datepicker_validate" id="modal_div">
                            <div>
                                 <strong>Enable App</strong>
                    <span class="onoff">
                        <input type="checkbox" value="0" id="config_confirmation" name="config_confirmation">
                        <label for="config_confirmation"></label>
                    </span>
                            </div>      
                        </div>  
                    </div>       
                </div>
            </div>
        </div>


<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the crurrent tab

    function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("prevBtn_top").style.display = "none";
    document.getElementById("nextBtn_top").style.display = "inline";
    } else {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("prevBtn_top").style.display = "inline";
    document.getElementById("nextBtn_top").style.display = "inline";
    }
    if (n == (x.length - 1)) {


    @if (isset($config_confirmation))
    {
    var config_confirmation = "{{ $config_confirmation }}";
    if (config_confirmation == "N")
    {
    $('#config_confirmation_modal').modal('show');
    }
    }
    @endif

    document.getElementById("nextBtn").style.display = "none";
    document.getElementById("nextBtn_top").style.display = "none";
    //document.getElementById("nextBtn").hide();
    } else {
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("nextBtn_top").style.display = "inline";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n);
    }

    function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm())
            return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
    }

    function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
    // add an "invalid" class to the field:
    y[i].className += " invalid";
    // and set the current valid status to false
    valid = false;
    }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
    }

    function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
    }


    $("#config_confirmation").change(function () {
    var checked = $(this).prop("checked");
    var shop_name = "{{ session('shop') }}";
    if (checked)
    {
    $.ajax({
    url: 'update_config_confirmation',
            data: { "_token": "{{ csrf_token() }}", "shop_name": shop_name},
            async: false,
            type: 'POST',
            success: function (result)
            {

            },
            error: function(xhr, error){
            console.debug(xhr); console.debug(error);
            },
    });
    $('#config_confirmation_modal').modal('toggle');
    }
    }
    );
</script>
@endsection

@section('scripts')
<script>
    $(".screenshot").click(function () {
    $(".modal-content img").attr("src", $(this).attr("data-src"));
    $(".modal-title").html($(this).data("title"));
    });
</script>
@endsection