@extends('header')
@section('content')
<?php
if (Session::has('shop')) 
{
    $url = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} else 
{
    $url = "#";
    $url_cart_template = "#";
    $url_product_page = "#";
    $url_cart_page = "#";
}
?>
<?php
if (Session::has('shop')) 
{
    $url1 = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
} 
else 
{
    $url1 = "#";
}
?>
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'General Settings',
            buttons: {                
                secondary: [                    
                    {
                        label: 'General Settings',
                        href: 'dashboard',
                        loading: true
                    },
					{
                        label: 'Product Settings',
                        href: '{{ url("/products") }}',
                        loading: true
                    },
					{
                        label: 'Help',
                        href: '{{ url("/help") }}',
                        loading: true
                    }
                ]
            }
        });
    });
    
    $(document).ready(function () {
        $(".slide_down").click(function () {
            var display = $("#shortcode_info").css("display");
            $("#shortcode_info").slideToggle();
            if (display == "none")
            {
                $(".slide_down i").removeClass("fa fa-chevron-up");
                $(".slide_down i").addClass("fa fa-chevron-down");
            } else
            {
                $(".slide_down i").removeClass("fa fa-chevron-down");
                $(".slide_down i").addClass("fa fa-chevron-up");
            }
        });
        $(".close_box").click(function () {
            var display = $("#shortcode_info").css("display");
            $("#shortcode_info").slideToggle();
            if (display == "none")
            {
                $(".slide_down i").removeClass("fa fa-chevron-up");
                $(".slide_down i").addClass("fa fa-chevron-down");
            } else
            {
                $(".slide_down i").removeClass("fa fa-chevron-down");
                $(".slide_down i").addClass("fa fa-chevron-up");
            }
        });
    });
</script>
<div class="col-md-12 formcolor">   
   <ul class="nav nav-tabs dashboard_tabs">
		<li>
			<a href="dashboard">General Settings</a>
		</li>
		<li>
			<a href="products">Product Settings</a>
		</li>
		<li class="active">
			<a href="shortcodes">Shortcodes</a>
		</li>		
	</ul>    
	<br>
	<br>
	<h1 class="sub-heading subleft col-md-12">Shortcode & Where to paste Shortcode?</h1>	
	<div class="col-sm-12">                
		<ul class="shortcode-note">
			<li><h3>#1 - For rendering product matrix</h3></li>
			<li>
				<div>Copy and paste below shortcode in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code.png') }}" href="javascript:;"><b>Example</b></a>
				</div>
			</li>
			<li>If your theme is section theme, or product template file is not available in your theme, then paste it in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_001.png') }}" href="javascript:;"><b>Example</b></a>
			</li>	                
			<li>
				<div class="copystyle_wrapper">
					<textarea id="script_code" rows="1" class="form-control short-code"  readonly=""><?php echo "<div class='zestard-productmatrix' store_id= '" . $encrypted_store_id . "' id='{{product.id}}'></div>"; ?>
					</textarea>
					<btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_shortcode()"><i class="fa fa-check"></i> Copy</btn>
					<label class="copy_message">
					</label>
				</div>
				<div class="alert alert-success alert-dismissable show_copy_message"><a href="#" class="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>		
			</li>
			<li><h3>#2 - For updating minimum quantity</h3></li>
			<li>
				<div class="note"><strong>Note: This is step is not mandatory if you have not set the minimum quantity for product(s)</strong></div>
				<li>
					<div>Copy and paste below shortcode in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code.png') }}" href="javascript:;"><b>Example</b></a>
					</div>
				</li>
				<li>If your theme is section theme, or product template file is not available in your theme, then paste it in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_001.png') }}" href="javascript:;"><b>Example</b></a>
				</li>	                
				<div class="copystyle_wrapper">
					<textarea id="product_page_code" rows="1" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix' %}"; ?></textarea>
					<btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_product_shortcode()"><i class="fa fa-check"></i> Copy</btn>		
					<label class="copy_message">
					</label>
				</div>
				<div class="alert alert-success alert-dismissable show_product_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>							
			</li>                                			
			<li><h3>#3 - For hiding default options and variants dropdown box</h3></li>
			<li>
				Then in <a href="<?php echo $url; ?>" target="_blank"><b>product template page</b></a>, add class "zestard-productmatrix" in the form, see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/shortcode_product_2.png') }}" href="javascript:;"><b>Example</b></a>. If your form tag is having format like "{% form 'product', product, class:form_classes %}", Then add class as shown in this <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/form_class_product_matrix.png') }}" href="javascript:;"><b>Example</b></a>.
			</li>                
			<li>If your theme is section theme, or product template file is not available in your theme, then paste it in <a href="<?php echo $url_product_page; ?>" target="_blank"><b>product page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/product_template_shortcode_002.png') }}" href="javascript:;"><b>Example</b></a>
			</li>                                				
			<li>				
				<div class="copystyle_wrapper">				
					<textarea id="productmatrix_custom_class" rows="1" class="form-control short-code"  readonly=""><?php echo "zestard-product-matrix"; ?></textarea>
					<btn id="copy_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="add_productmatrix_custom_class()"><i class="fa fa-check"></i> Copy</btn>
					<label class="copy_message">
					</label>
				</div>
				<div class="alert alert-success alert-dismissable show_productmatrix_custom_class_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>	
			</li>
			<li><h3>#4 - For checking minimum quantity on cart page</h3></li>	
			<li><div class="note"><strong>Note: This is step is not mandatory if you have not set the minimum quantity for product(s)</strong></div></li>
			<li>
				Copy and paste below shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/short_code_cart.png') }}" href="javascript:;"><b>Example</b></a>
			</li>                
			<li>
				If your theme does not have cart-template.liquid file then paste the short-code in <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_shortcode_001.png') }}" href="javascript:;"><b>Example</b></a>
			</li>                
			<li>
				<div class="copystyle_wrapper">
					<textarea id="cart_page_code" rows="1" class="form-control short-code"  readonly=""><?php echo "{% include 'product-matrix-cart' %}"; ?>
					</textarea>
					<btn id="copy_cart_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_cart_shortcode()"><i class="fa fa-check"></i> Copy</btn>
					<label class="copy_message">
					</label>
				</div>		
				<div class="alert alert-success alert-dismissable show_cart_copy_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>							
			</li>    
			<li><h3>#5 - For displaying error message if minimum quantity criteria is not satisfied</h3></li>	
			<div class="note"><strong>Note: This is step is not mandatory if you have not set the minimum quantity for product(s)</strong></div>
			<li>Copy and paste below shortcode in <a href="<?php echo $url_cart_template; ?>" target="_blank"><b>cart template page </b></a> inside <?php echo "&lt;div class='cart__qty'&gt; &lt;/div&gt;." ?> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/error_code.png') }}" href="javascript:;"><b>Example</b></a>
			</li>            
			 <li>
			 If your theme does not have cart-template.liquid file then paste the short-code in <a href="<?php echo $url_cart_page; ?>" target="_blank"><b>cart page</b></a> see <a data-toggle="modal" data-target="#help_modal" class="info_css screenshot" data-src="{{ asset('image/cart_template_shortcode_002.png') }}" href="javascript:;"><b>Example</b></a>
			</li>                
			<li>
				<div class="copystyle_wrapper">
					<textarea id="error_code" rows="1" class="form-control short-code"  readonly=""><?php echo '<p style="color:red" class="{{ item.product.id }}"></p>'; ?>
					</textarea>
					<btn id="copy_error_code" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_error_shortcode()"><i class="fa fa-check"></i> Copy</btn>
					<label class="copy_message">
					</label>
				</div>
				<div class="alert alert-success alert-dismissable show_error_message"><a href="#" class="close" >×</a><strong>Success!</strong> Your shortcode has been copied.</div>		
			</li>
		</ul>
		<ul class="shortcode-note">
			<li><h3></h3></li>
			<li>
				<strong>If your cart page is not opening as url https://your-store-name/cart, Then Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right</strong>
			</li>                
		</ul>            
		<br>
		<br>
	</div>           
</div>

<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(".screenshot").click(function () {
        $(".modal-content img").attr("src", $(this).attr("data-src"));
    });
</script>
@endsection