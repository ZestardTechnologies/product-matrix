@extends('header')
@section('content')

<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
                secondary: [
                    {
                        label: 'General Settings',
                        href: '{{ url("/dashboard") }}',
                        loading: true
                    },
                    {
                        label: 'Product Settings',
                        href: '{{ url('products') }}',
                        loading: true
                    }
                ]
            }
        });
    });
</script>

<?php
$store_name = session('shop');
?>

<?php
if (Session::has('shop')) {
    $url_product_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/product-template.liquid";
    $url_product_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/product.liquid";
    $url_cart_template = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
    $url_cart_page = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
} else {
    $url_product_template = "#";
    $url_product_page = "#";
    $url_cart_template = "#";
    $url_cart_page = "#";
}
?>

<div class="col-md-12 formcolor">   
    <ul class="nav nav-tabs dashboard_tabs">
        <li>
            <a href="dashboard">General Settings</a>
        </li>
        <li>
            <a href="products">Product Settings</a>
        </li>
        <li class="active">
            <a href="shortcodes">Shortcodes</a>
        </li>	
        <li>
            <a href="shortcodes_modified">Shortcodes Modified</a>
        </li>	
    </ul>    
    <br>
    <br>
    @include('help.how_to_setup')
</div>

<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class ="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</h4>
            </div>
            <img src=""/>			
        </div>      
    </div>
</div>
@endsection


@section('scripts')
<script>
    $(".screenshot").click(function () {
        $(".modal-content img").attr("src", $(this).attr("data-src"));
    });
</script>
@endsection