<script>
    temp_array = new Array();
    <?php echo "{% for item in cart.items %}" ?>
    if (typeof temp_array[<?php echo "{{ item.product.id }}" ?>] === 'undefined')
    {
        temp_array[<?php echo "{{ item.product.id }}" ?>] = <?php echo "{{ item.quantity }}" ?>;
    } else
    {
        quantity = temp_array[<?php echo "{{ item.product.id }}" ?>];
        temp_array[<?php echo "{{ item.product.id }}" ?>] = quantity + <?php echo "{{ item.quantity }}" ?>;
    }
	<?php echo "{% endfor %}" ?>
    if (typeof temp_array[<?php echo "{{ product.id }}" ?>] === 'undefined')
    {
        current_quantity = 0;
    } else {
        current_quantity = temp_array[<?php echo "{{ product.id }}" ?>];
    }
</script>
<div class='zestard-productmatrix' store_id='<?php echo $id ?>' id='<?php echo "{{product.id}}" ?>'>
<style>
.zestard-productmatrix{
	text-align:center;
}
</style>
<h4>Loading.. Please Wait..</h4>
<img height="130" width="130" src='https://zestardshop.com/shopifyapp/product-matrix/public/image/loader_new.svg' />
</div>					
<script type="text/javascript" async="" src="https://zestardshop.com/shopifyapp/product-matrix/public/js/productmatrix-app.js?shop=product-matrix.myshopify.com"></script>