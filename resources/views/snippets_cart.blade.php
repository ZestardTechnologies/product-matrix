<div id="product_matrix_cart"></div>
<script>
	var flag = 0, response = 1;
	response_array = new Array();
	temp_array = new Array();
	var product_array, shop_name=Shopify.shop, settings;	
	var $zestard_product_matrix = "";
	var base_path_product_matrix = "https://zestardshop.com/shopifyapp/product-matrix/public/";
	if (typeof jQuery == 'undefined')
    {
		var jscript = document.createElement("script");
		jscript.src = base_path_product_matrix + "js/zestard_jquery_3.3.1.js";
		jscript.type = 'text/javascript';
		jscript.async = false;
		document.getElementById('product_matrix_cart').append(jscript);            		
		jscript.onload = function() {
			$zestard_product_matrix = window.jQuery;   
			product_matrix_cart();
		};
	}
	else
	{
		$zestard_product_matrix = window.jQuery;   
		product_matrix_cart();
	}
	function product_matrix_cart()
	{
		$.ajax({
			url:'https://zestardshop.com/shopifyapp/product-matrix/public/get-settings',       	
			data:{shop_name:shop_name},                         
			async:false,	
			type:"POST",		
			success:function(result){
				settings = $.parseJSON(result, true);				                	
			}          	
		});
		error_message_on_cart = settings['min_qty_error_msg_on_cart'];
		if(settings['app_status'] == 1)
		{
			<?php echo "{% for item in cart.items %}" ?>
			if(typeof temp_array[<?php echo "{{ item.product.id }}" ?>] === 'undefined') 
			{
				temp_array[<?php echo "{{ item.product.id }}" ?>] = <?php echo "{{ item.quantity }}"?>+"," + '<?php echo "{{ item.product.title }}" ?>'; 	
			}
			else
			{
				response_array = temp_array[<?php echo "{{ item.product.id }}" ?>].split(",");
				quantity = parseInt(response_array[0]);       	 
				temp_array[<?php echo "{{ item.product.id }}" ?>] = (quantity + <?php echo "{{ item.quantity }}"?>) + "," + '<?php echo "{{ item.product.title }}" ?>';                     	
			}        
			<?php echo "{% endfor %}" ?>
			$.ajax({
				url:'https://zestardshop.com/shopifyapp/product-matrix/public/check-quantity',       	
				data:{shop_name:shop_name},                         
				async:false,	
				type:"POST",		
				success:function(result){
					product_array = $.parseJSON(result, true);				                	
				}          	
			});
			if(product_array)
			{
				$.map(product_array, function( val, i ) {					 
					if(typeof temp_array[i] !== 'undefined')
					{
						response_array = temp_array[i].split(",");     
						console.log(response_array);
						if(product_array[i] > parseInt(response_array[0]))
						{
							if(!error_message_on_cart || typeof error_message_on_cart === 'undefined' || error_message_on_cart == null)
							{
								$("."+i).html('Minimum Total Quantity should be ' + product_array[i] + ' for '  + response_array[1]);  
							}
							else
							{
								$("."+i).html(error_message_on_cart);
							}						
							//$("."+i).html('Minimum Total Quantity should be ' + product_array[i] + ' for '  + response_array[1]);  
							$("input[name=checkout]").attr("disabled", "disabled");
						}
					}
				});
			}		
		}		
		$("input[name=checkout]").click(function(e){
			$.ajax({
				url:'https://zestardshop.com/shopifyapp/product-matrix/public/product-config',       	
				data:{shop_name:shop_name},                         
				async:false,	
				type:"POST",		
				success:function(result){
					settings = $.parseJSON(result, true);				                	
				}          	
			});
			error_message_on_cart = settings['min_qty_error_msg_on_cart'];
			if(settings['app_status'] == 1)
			{		
				response_array = new Array();
				temp_array = new Array();
				checkout_flag = 1;	
				<?php echo "{% for item in cart.items %}"?>  	
				if(typeof temp_array[<?php echo "{{ item.product.id }}" ?>] === 'undefined') 
				{
					temp_array[<?php echo "{{ item.product.id }}" ?>] = <?php echo "{{ item.quantity }}"?>+"," + '<?php echo "{{ item.product.title }}" ?>'; 	
				}
				else
				{
					response_array = temp_array[<?php echo "{{ item.product.id }}" ?>].split(",");
					quantity = parseInt(response_array[0]);       	 
					temp_array[<?php echo "{{ item.product.id }}" ?>] = (quantity + <?php echo "{{ item.quantity }}"?>) + "," + '<?php echo "{{ item.product.title }}" ?>';                     	
				}        
				<?php echo "{% endfor %}" ?> 
				$.ajax({
					url:'https://zestardshop.com/shopifyapp/product-matrix/public/check-quantity',       	
					data:{shop_name:shop_name},                         
					async:false,
					type:"POST",	
					success:function(result){
						product_array = $.parseJSON(result, true);				                	
					}          	
				});
				if(product_array)
				{
					$.map(product_array,function(val,i) {                         
						if(typeof temp_array[i] !== 'undefined')
						{
							response_array = temp_array[i].split(",");     
							console.log(response_array);
							if(product_array[i] > parseInt(response_array[0]))
							{
								checkout_flag = 0;
								if(!error_message_on_cart || typeof error_message_on_cart === 'undefined' || error_message_on_cart == null)
								{
									$("."+i).html('Minimum Total Quantity should be ' + product_array[i] + ' for '  + response_array[1]); 
								}
								else
								{
									$("."+i).html(error_message_on_cart + " " + product_array[i]);
								}
								$("input[name=checkout]").attr("disabled", "disabled");
							}
						}
					});	  		    
				}
				if(checkout_flag == 0)
				{
					return false;
				}
			}
		});
	}
</script>