<script>
    temp_array = new Array();
    <?php echo "{% for item in cart.items %}" ?>
    if (typeof temp_array[<?php echo "{{ item.product.id }}" ?>] === 'undefined')
    {
        temp_array[<?php echo "{{ item.product.id }}" ?>] = <?php echo "{{ item.quantity }}" ?>;
    } else
    {
        quantity = temp_array[<?php echo "{{ item.product.id }}" ?>];
        temp_array[<?php echo "{{ item.product.id }}" ?>] = quantity + <?php echo "{{ item.quantity }}" ?>;
    }
<?php echo "{% endfor %}" ?>;
    if (typeof temp_array[<?php echo "{{ product.id }}" ?>] === 'undefined')
    {
        current_quantity = 0;
    } else {
        current_quantity = temp_array[<?php echo "{{ product.id }}" ?>];
    }
</script>