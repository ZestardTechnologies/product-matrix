<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * CallbackController for installation
*/

Route::group(['middleware' => 'checkconfig'], function () {
Route::any('change_currency', 'callbackController@Currency')->name('change_currency');

Route::post('search', function () {
    return view('search');
})->middleware('cors')->name('search');


Route::get('help', 'ProductController@help')->name('help');

Route::any('dashboard', 'ProductController@dashboard')->name('dashboard');

Route::get('getproducts' , 'ProductController@get_products');

Route::post('update/general/settings', 'ProductController@updateGeneralSettings');

Route::any('update-modal-status', 'ProductController@update_modal_status')->middleware('cors')->name('update-modal-status');

Route::get('products' , 'ProductController@index');

Route::get('edit/{id}' , 'ProductController@edit');

Route::get('update/{id}' , 'ProductController@update');

});

Route::post('update_config_confirmation' ,'ProductController@configConfirmation')->name('config_confirmation');

Route::any('shortcodes' , 'ProductController@shortcodes')->name('shortcodes');

Route::any('shortcodes_modified' , 'ProductController@shortcodes_modified')->name('shortcodes_modified');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('declined', 'callbackController@declined')->name('declined');

Route::any('test', 'callbackController@test')->name('test');

Route::get('decline', function () {
    return view('decline');
})->name('decline');

Route::any('testing', 'Testing@index')->name('testing');

/*
 * ProductController methods
 * 
*/

Route::any('frontend', 'FrontendController@frontend_view')->middleware('cors')->name('frontend');

Route::any('check-quantity', 'FrontendController@get_product_settings')->middleware('cors')->name('check-quantity');

Route::any('product-config', 'FrontendController@get_settings')->middleware('cors')->name('product-config');

Route::any('check-app', 'FrontendController@check_app')->middleware('cors')->name('check-app');

Route::any('get-settings', 'FrontendController@get_settings')->middleware('cors')->name('get-settings');



//Route::post('product/get/matrix' , 'ProductController@getMatrix')->middleware('cors');

Route::any('check/html' , function() {
    return view('check');
});

Route::get('charge-declined', function () {
    return view('charge_declined')->with('store_name', session('shop'));
})->name('charge-declined');

Route::any('productmatrix/cart/add' , 'ProductController@productMatrixAddCart');
